# 作业：
    # usernames = ['xiaohei','xiaobai','xiaoming']
    # passwords = ['123456','11111','1']
    # 需要通过代码把上面的两个list转成下面的字典
    # user_info = {'xiaohei':'123456','xiaobai':'11111','xiaoming':'1','lcl':'123'}
    # 1、注册
    #     1、输入账号和密码、确认密码
    #     2、要校验账号是否存在（从字典里面判断），存在的话，不能注册
    #     3、校验两次输入的密码是否一致，如果一致，加入到字典中，提示注册成功
    #     4、最多3次
    #     5、要校验输入是否为空，为空也算一次错误，全是空格也算空
    # 2、登录
    #     1、输入账号和密码
    #     2、要校验账号是否存在（从字典里面判断），不存在的话，不能登录
    #     3、账号和密码一致就可以登录
    #     4、最多3次
    #     5、要校验输入是否为空，为空也算一次错误，全是空格也算空

usernames = ['xiaohei','xiaobai','xiaoming']
passwords = ['123456','11111','1']
#1、两个list转成字典，需要循环，username是key，password是value
#2、循环3次，输入账号和密码、确认密码
#3、判读是否输入为空，不为空继续
#4、判断账号是不是在字典里面，如果不在的话，可以注册
#4、再判断两次输入密码是否一致，如果一致的话，可以注册
#5、账号和密码加入到字典里面

#1、两个list转成字典，需要循环，username是key，password是value
#2、循环3次，输入账号和密码
#3、判读是否输入为空，不为空继续
#4、判断账号是不是在字典里面，如果在的话，可以登录
#5、根据账号从字典里面获取到密码，和用户输入的密码做比较，如果一致，登录成功


info = {}
for i in range(len(usernames)): # for i in range(3): 0 1 2
    u = usernames[i]
    p  = passwords[i]
    info[u] = p

#1、注册
# for i in range(3):
#     username = input('username:').strip()
#     if  username=='':
#         print('账号已存在')
#     elif username in info:
#         print('输入不能为空')
#     else:
#         password = input('password:').strip()
#         cpassword = input('cpassword:').strip()
#         if password=='' or cpassword=='':
#             print('密码不为空')
#         elif password != cpassword:
#             print('两次输入密码不一致')
#         else:
#             info[username] = password
#             print('注册成功')
#             break
# else:
#     print('错误次数过多')

#2、登录

for i in range(3):
    username = input('username:').strip()
    if  username == '': #非空即真，username''
        print('输入不能为空')
    elif username not in info:
        print('用户名不存在')
    else:
        password = input('password:').strip()
        if password:
            print('密码不为空')
        elif password != info.get(username):
            print('密码输入错误！')
        else:
            print('欢迎%s登录'%username)
            break
else:
    print('错误次数过多')

print(info)