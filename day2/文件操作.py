#1、打开文件
#2、读/写文件
#3、关闭文件
#r 只读模式
#   w写模式，他会把以前的内容清空掉
f = open(r'a1.txt','w',encoding='utf-8') #打开的模式 r w a
f.write('xxxxxxx')
l=['1234\n','56789\n','abcdef']
f.writelines(l)

# for i in l:
#     f.write(i)
#
# f.close()

# result2 = f.readline() #读文件，一次只读取一行
# result = f.read() #读文件,获取文件里面的全部内容
# result3 = f.readlines()#读取文件里面所有的内容，返回的是一个list，
# # 每行的内容是list的一个元素
# print('result',result)
# print('result2',result2)
# print('result3',result3)
# f.close()

#文件指针的

#20G 8G

#处理大文件的时候
# count = 0
# for line in f:
#     print(line)
#     count+=1
#     print(count)
