#

#set
#1、集合天生可以去重
#2、集合是无序的
s = {1,2,3,4,5,6,7,7,7,6,6,1,1} #集合
s2 = set() #空集合
l = [1,2,3,4,5,6,7,7,7,6,6,1,1]
l = list(set(l))
s.add(8) #添加元素
s.update({4,5,6}) #把另外一个集合加入这个集合里面
s.remove(4) #删除指定的元素

#关系测试，交集、并集、差集、对称差集

l1 = range(0,11) #学了自动化的学生
l2 = [2,4,5,6] #学性能的学生

l1 = set(l1)
l2 = set(l2)

# print(l1 & l2) #取交集，就是两个集合里面相同的元素
# print(l1.intersection(l2))

# print(l1.union(l2)) #并集,就是把两个集合合并到一起
# print(l1 | l2)

# print(l1 - l2 )#差集,在a集合里面存在，在b集合里面不存的
# print(l1.difference(l2))
#
# print(l1 ^ l2) #对称差集，把a集合和b集合里面相同的元素去掉，剩下的
# print(l1.symmetric_difference(l2))

print(l1.issuperset(l2)) #判断a集合是不是b集合的父集
print(l2.issubset(l1)) #判断a集合是不是b集合的子集
# 12345678
# 345