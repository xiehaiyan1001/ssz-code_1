a = 1
b = 3
d = {}

#1、非空即真，非0即真,帮你节省代码的
True
False

s='' #false
l=[] #false
d={} #false
t=() #false
se=set()  #false
num = 1 #true
num2 = 0 #false
l1 = ['','']#true

for i in range(3):
    choice = input('1,登录2、退出3、取钱:').strip() #1 choice = ''
    if not choice: # 非空即真 #取反 choice not true  not false
        print('为空')
    else:
        print('已经输入')


#not 的意思是取反，本来是true加上not变成false，本来是false加上not 变成true
