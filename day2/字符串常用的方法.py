s = " 1861253  1231DDDDF "
# print(s.count('a'))
# print(s.index('a'))
# print(s.find('a'))
print(s)
# print(s.strip('.'))#默认去掉字符串两边的空格和换行符
# print(s.lstrip())
# print(s.rstrip())
# print(s.startswith('135'))
# print(s.endswith('com'))

# print(s.lower())
# print(s.upper())
# print(s.islower())
# print(s.isupper())

# s=[1,2,3,4,5,6,7.....]
#0001 0002  0003 0004
# s='marry'

# print(s.zfill(5))#用来补零的

# print(s.capitalize())#首字母大写

# print(s.replace('1','',2))
# s='2'
# print(s.isdigit())#判断是否为纯数字
s='           '
# print(s.center(80,'*'))
# print('买东西')
# print('欢迎再来'.center(50,'*'))
# print(s.isspace())#判断是不是空格
s = '编号是nameage123'
# print(s.format(name=1,age=2))
# print(s.format_map({'name':1,'age':35}))
# print(s.isalnum()) ##如果是大小写字母、汉字、数字返回true，其他的字符串都返回false
# print(s.isalpha()) #如果是大小写字母、汉字返回true，其他的字符串都返回false


# print(s.find('a'))
# print(s.count('a'))
# print(s.strip('.'))#默认去掉字符串两边的空格和换行符
# print(s.startswith('135'))
# print(s.endswith('com'))
# print(s.lower())
# print(s.upper())
# print(s.replace('1','',2))
# print(s.isdigit())#判断是否为纯数字
# s.join() #连接字符串的
# s.split() #分割字符串

# s='zyb,cmc,wy,lj,lzh,lxy'  #['zyb','cmc','wy']
# s1='zyb cmc wy     lj  lzh  lxy'  #['zyb','cmc','wy']
# s2='zyb.cmc.sy'
# print(s.split(','))
# print(s1.split())
# print(s2.split(','))

# d = {'xiaohei':'123456','xiaobai':'123456'}
l = ['zyb', 'cmc', 'wy', 'lj', 'lzh', 'lxy']
l3=[1,2,3,4,5,6]
l4='abcdedfe'
print('='.join(l))
# s='zyb,cmc,wy,lj,lzh,lxy'