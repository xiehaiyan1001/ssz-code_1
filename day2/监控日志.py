#1、监控，一直在运行，死循环，每分钟读一次文件
#2、第一次运行，读取文件所有的内容，从文件内容里面获取到ip地址，以及每个ip地址出现的次数
#4、记住当前指针的位置，下次从这个位置开始读
#5、判断每个ip地址出现的次数，如果大于50次，那么发短信报警
#6、等待60秒，继续重复上面的操作，读文件，获取内容
import time
point = 0 #最前面的位置

while True:
    ip_list = {}  # {ip:2 }
    f = open('access.log',encoding='utf-8')
    f.seek(point)#移动文件指针到哪个位置
    for line in f:
        ip = line.split()[0]
        if ip not in ip_list:
            ip_list[ip] = 1
        else:
            ip_list[ip] = ip_list[ip]+1
    point = f.tell() #当前文件指针的位置
    f.close()
    for ip in ip_list:
        count = ip_list.get(ip)
        if count>=50:
            print('ip地址【%s】有问题，请注意！'%ip)
    time.sleep(60)
