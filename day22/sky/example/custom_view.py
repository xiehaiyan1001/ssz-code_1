import datetime
from itertools import chain

from django.core.paginator import Paginator
from django.db.models import Model, Q
from django.forms import BaseForm
from django.http import JsonResponse
from django.views import View
from example.custom_response import NbResponse


def model_to_dict(instance, fields=None, exclude=None):
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if isinstance(value, datetime.datetime):
            value = value.strftime('%Y-%m-%d %H:%M:%S')
        if isinstance(value, datetime.date):
            value = value.strftime('%Y-%m-%d')
        data[f.name] = value
    return data


class BaseView(View):
    filter_fields = []  # 按照什么字段进行过滤
    search_fields = []  # 按照什么字段进行匹配  key search
    model_class = None  # model
    form_class = None
    fields = []  # 返回时指定返回什么字段
    exclude_fields = []  # 指定返回时过滤掉那个字段

    @property
    def model(self):
        # issubclass 参数1 是不是 参数2的子类
        if self.model_class and issubclass(self.model_class, Model):
            return self.model_class
        raise Exception("未定义model_class")

    @property
    def form(self):
        # issubclass 参数1 是不是 参数2的子类
        if self.form_class and issubclass(self.form_class, BaseForm):
            return self.form_class
        raise Exception("未定义form_class")

    def get_filter_dict(self):
        # 现在 self.filter_fields 就是 ['name']
        filter_dict = {}
        for filed in self.filter_fields:
            # 通过request 取到 name值
            value = self.request.GET.get(filed)
            if value:
                filter_dict[filed] = value
        return filter_dict

    # 简陋版本 只实现了 在一个字段中模糊匹配
    def get_search_dict(self):
        search_dict = {}
        value = self.request.GET.get('search')
        if value:
            search_dict = {"%s__contains" % self.search_fields[0]: value}
        return search_dict

    def get_search_dict_or(self):
        # ['name','desc']  中模糊匹配出所有符合要求的
        # or --》 Q 通过 |
        # 空Q 什么条件都没有
        q = Q()
        value = self.request.GET.get('search')
        if value:
            for filed in self.search_fields:
                d = {"%s__contains" % filed: value}
                q = Q(**d) | q
        return q


class GetView(BaseView):
    def get(self, request):
        # get请求时 获取传递过来的第几页数据
        page = request.GET.get('page')
        limit = request.GET.get('limit')
        filter_dict = self.get_filter_dict()
        # search_dict = self.get_search_dict()
        search_q = self.get_search_dict_or()
        qs = self.model.objects.filter(is_delete=0).filter(**filter_dict).filter(search_q)
        page_obj = Paginator(qs, limit)  # page_obj Paginator实例对象
        # 获取第几页的数据
        page_data = page_obj.get_page(page)
        # 用于存储返回的dict类型数据
        data_list = []
        for data in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(data, fields=self.fields, exclude=self.exclude_fields)
            data_list.append(tmp_data)
        # return JsonResponse({"code": 0, "data": data_list})
        return NbResponse(data=data_list,count=page_obj.count)


class PostView(BaseView):
    def post(self, requests):
        # 通过from进行数据验证
        form_obj = self.form(requests.POST)
        # 数据验证是否通过
        if form_obj.is_valid():
            # 创建数据
            # form_obj.cleaned_data 是所有验证通过的数据
            self.model.objects.create(**form_obj.cleaned_data)
            return NbResponse()
        else:
            # 返回错误信息
            # return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
            return NbResponse(code=500, msg=form_obj.errors.get_json_data())


class PutView(BaseView):
    def put(self, request):
        # django 并没有处理PUT的数据 requests.PUT
        # 实际put传过来的数据在requests.body
        # 需要导入 from django.http import QueryDict  来处处理数据 QueryDict(requests.body)
        # print(requests.PUT)
        # print('requests.body',QueryDict(requests.body))
        # PUT 传过来的数据
        # 更新数据，需要告知，要更新那条数据
        # 获取要更新那条数据的主键id
        p_id = request.PUT.get('id')
        print("p_id",p_id)
        # 通过id 从数据库中取这条数据  obj
        data_obj = self.model.objects.get(id=p_id)
        # 参数1 是前端传过来的数据， 参数2 是数据库中获取的数据
        form_obj = self.form(request.PUT, instance=data_obj)
        if form_obj.is_valid():
            # 通过form的save方法进行数据更新
            form_obj.save()
            return NbResponse()
        else:
            return NbResponse(code=500, msg=form_obj.errors.get_json_data())


class DeleteView(BaseView):
    def delete(self, requests):
        #  删除时，要知道 删除的是那条数据 获取主键id
        p_id = requests.GET.get('id')
        # 删除分为2种，逻辑删除、物理删除
        # 逻辑删除： 只是通过改变 某个字段的状态，来控制是否删除
        # 比如：is_delete 数据很重要未来有通过状态在改变回来的需要
        # 物理删除： 直接将数据删掉 delete掉 数据不是很重要。
        # 逻辑删除例子
        # models.Parameter.objects.filter(id=p_id).update(is_delete=1) # 这种更新方式不会触发 updatetime
        # obj = models.Parameter.objects.filter(id=p_id).first()
        # obj.is_delete = 1
        # obj.save()

        # 物理删除
        self.model.objects.filter(id=p_id).delete()
        return NbResponse()


class NbView(GetView, PostView, PutView, DeleteView):
    pass
