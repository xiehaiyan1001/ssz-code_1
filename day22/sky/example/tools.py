import hashlib

def md5(s):
    '''md5'''
    m = hashlib.md5(s.encode())
    return m.hexdigest()


# 优化 errors返回的信息
class ErrorFormat:
    @property
    def error_format(self):
        errors_data = self.errors.get_json_data()
        # {'username': [{'message': '用户不存在', 'code': ''}]}
        print("errors_data", errors_data)
        error_list = []
        # 错误列表 用于存储所有的错误数据
        for k, v in errors_data.items():
            error_list.append(k + ':' + v[0].get("message"))
        return ','.join(error_list)