from django import forms
from django.core.exceptions import ValidationError  # 验证有问题，抛异常用的
from user import models


# validators 里面的先进行验证
# 第一个运行的验证方式
def test_phone(value):
    print("test_phone", 3)
    if len(value) != 11:
        raise ValidationError('手机格式不正确')
    else:
        return value


class ArticleFrom(forms.Form):
    phone = forms.CharField(required=True,  # 是否必填
                            validators=[test_phone],  # 公共验证器 验证手机号码的格式 邮箱的格式
                            error_messages={  # 错误信息提示
                                'required': '必填参数未填',
                            })

    # title = forms.CharField(required=True,  # 是否必填
    #                         max_length=10,  # 最大长度5
    #                         min_length=5,  # 最小长度
    #                         strip=False,  # 是否去掉用户输入的空格 默认是True
    #                         error_messages={  # 错误信息提示
    #                             'required': '必填参数未填',
    #                             'max_length': '最大10个字符',
    #                             'min_length': '最小5个字符'
    #                         })

    # forms 中 存在钩子函数 clean
    # 第三步进行全部数据验证
    def clean(self):
        #
        # 通过 self.cleaned_data 可以获取到所有数据
        print("clean", 1)
        return self.cleaned_data

    # 针对个别字段进行验证，
    #  第二步 针对 个别字段进行验证
    # 验证这个手机号 是不是我们系统的
    def clean_phone(self):
        print("clean_phone", 2)
        return self.cleaned_data.get('phone')


#  model + form
def test_name(value):
    if value == 'dsx':
        raise ValidationError('姓名不存在。')
    else:
        return value


class StudentForm(forms.ModelForm):
    # 当你觉得model写的验证规则不符合你的要求，可以像forms.Form 一样进行验证
    name = forms.CharField(validators=[test_name])

    class Meta:
        model = models.Student  # 创建类变量和model建立映射关系
        # fields = '__all__'  # 验证全部字段
        # fields = ['name']  # 显示指定列验证 只有指定的验证的字段才会出现在cleaned_data中
        exclude = ['age']  # 排除了就不会出现在cleaned_data中

    def clean(self):
        if self.cleaned_data.get('name') == 'ssz':
            raise ValidationError('姓名是射手座')
        else:
            return self.cleaned_data

    # 单个字段验证，必须要争在form中有些这个 字段
    def clean_name(self):
        if self.cleaned_data.get('name') == 'nhy':
            raise ValidationError('姓名是nhy')
        else:
            return self.cleaned_data.get('name')
