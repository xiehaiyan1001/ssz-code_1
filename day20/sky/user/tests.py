from django.test import TestCase
import os, django
from django.db.models import Q

# Create your tests here.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')
django.setup()

from user import models

# models.Category.objects.get(id=5).delete()


# c = models.Category.objects.get(name='python')
# a = models.Article(title='django项目配置',content='django项目配置django项目配置django项目配置',category=c)
# a.save()
#
# a2 = models.Article(title='python面向对象',content='python面向对象python面向对象',category_id=3)
# a2.save()
# models.Article.objects.create(title='python111面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python113面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python114面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python115面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python116面向对象',content='python面向对象python面向对象',category_id=7)
#

# models.Article.objects.get(id=1).delete()

# 增
# category  = models.Category(name='Python')
# category.save()

# models.Category.objects.create(name='Java')
# models.Category.objects.create(name='Python')
# models.Category.objects.create(name='MySQL')
# models.Category.objects.create(name='心情日记')
# c_obj = models.Category.objects.get(name='Java')

# models.Article.objects.filter(category=c_obj)#手动通过分类查询文章

# print(c_obj.article_set.count())
# 一对多的时候，通过1，反查多
# 1_obj.多_set.all() #取全部 1_obj.多_set.count()#取数量  1_obj.多_set.filter(is_delete=False)#根据条件差

# 删

# c1 = models.Category.objects.get(id=3)
# c1.delete() #单个数据修改

# q = models.Category.objects.filter(name='python1')
# q.delete()

# 改
# c1 = models.Category.objects.get(id=3)
# c1.name = 'Java'
# c1.save() #单个数据修改

# q = models.Category.objects.filter(name='Python')
# q.update(name='python1')
# #查

# c1 = models.Category.objects.get(name='Python',id=2)
# 只能返回一条数据，如果返回多条会报错
# 如果指定的条件查询不到数据，也会报错
# query_set = models.Category.objects.filter()
# query_set = models.Category.objects.all()
# print(query_set)
# c = query_set[0]
# print(c.id)
# print(c.name)

# a = models.Article.objects.get(id=1)
#
# print(a.title)
# print(a.category.name)
# print(a.category.id)

#  多对多关系
teacher_obj = models.Teacher.objects.get(id=2)  # id=1 牛牛  id=2 大师兄
student_obj = models.Student.objects.get(id=4)
# 谁和谁建立多对多关系
# 创建多对多关系 方法1
teacher_obj.student.add(student_obj)
'''
# 创建多对多 方法2  add方法可以接收主键id
# teacher_obj.student.add(4)

# 删除多对多关系
# teacher_obj.student.clear() # 清除老师对应的所有学生 批量操作 比如：老师离职了
# teacher_obj.student.remove(2) # 指定某个学生进行删除 比如：学生劝退了
# teacher_obj.student.remove(student_obj)  # 可以接收对象和 主键id

# teacher_obj.student.set([4,5]) # [1,2,3,4] 学生的主键id ，比如：老师去别的班当班主任了

# 查询多对多关系
# 正向查询
# 获取这个老师，有那些学生
# students = teacher_obj.student.all()
# print("students", students)

# 反向查询
# 反向查询是 基于某个学生，去查这个学生有几个老师
# teachers = student_obj.teacher_set.all()
# print("teachers", teachers)

# orm的基础操作
# orm 返回的 有两种数据类型  QuerySet   object
# QuerySet
# 支持链式编程，可以在all()后继续.方法
teachers = models.Teacher.objects.all()
# print("teachers",teachers)

name = teachers.values('name')  # 过滤字段，获取我们希望获取到字段
# print("name",name)
count = teachers.count()  # 返回qs的个数
# print("count",count)
firstData = teachers.first()  # 获取qs中第一个数据  list[0]
# print("firstData",firstData)
value_name = teachers.values_list('name')  # 只返回 要求过滤的字段的 value值不返回key
# print("value_name", value_name)

# object
teacher = models.Teacher.objects.get(id=1)
# print("teacher.name", teacher.name)
# print("teacher",teacher)

# 过滤 orm filter  sql的 where条件


# teacher = models.Teacher.objects.filter(name__contains='师')  # 过滤 模糊查询  == sql中 %
# teacher = models.Teacher.objects.filter(name__endswith='牛')  # 过滤 以什么为结尾
# teacher = models.Teacher.objects.filter(name__startswith='师')  # 过滤 以什么开头
# teacher = models.Teacher.objects.filter(name__in=['大师兄','牛牛'])  # 过滤 在什么范围内 == sql  in条件
# teacher = models.Teacher.objects.filter(id__gt=1)  # 过滤 大于
# teacher = models.Teacher.objects.filter(id__gte=1)  # 过滤 大于等于
# teacher = models.Teacher.objects.filter(id__lt=2)  # 过滤 小于
# teacher = models.Teacher.objects.filter(id__lte=2)  # 过滤 小于等于
# student = models.Student.objects.filter(id__range=[1,3]) # 过滤 在1到4之间 between and

# 排除
# 除了name=大师兄 其他都查出来
# teacher = models.Teacher.objects.exclude(name='大师兄')
# print("teacher",teacher)


# 多条件查询
# and
# teacher = models.Teacher.objects.filter(id=1, name='牛牛')  # where id=1 and name=牛牛
# print("teacher", teacher)

# or咋办？
# 我想查 id = 1  或者  name=大师兄
# where id=1 or name=大师兄
# teacher = models.Teacher.objects.filter(Q(id=1) | Q(name='大师兄'))  # or | 只要条件成立 就把数据都查出来
# where id=1 and name=大师兄
teacher = models.Teacher.objects.filter(Q(id=1) & Q(name='大师兄'))  # and & 只要条件成立 就把数据都查出来

# 取反的功能
# where id=1 and name!=大师兄
teacher = models.Teacher.objects.filter(Q(id=1) & ~Q(name='大师兄'))  # and & 只要条件成立 就把数据都查出来
print("teacher", teacher)
'''