"""sky URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from user import views
# 导入 example 中url
from example import urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index', views.user_info),
    path('welcome', views.welcome),
    path('articles', views.index),
    path('category/<int:id>', views.category),
    path('post', views.article),
    path('student', views.student),
    # 通过include 将 其他app的路由导入进来
    # api 就是 base目录  每个 example 当中的路由前都要加上api/
    path('api/', include(urls)),

]
