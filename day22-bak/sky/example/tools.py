import hashlib


def md5(s):
    '''md5'''
    m = hashlib.md5(s.encode())
    return m.hexdigest()