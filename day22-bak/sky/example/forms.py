from django import forms
from django.db.models import Q

from example import models


class ExtendForm:
    # 格式化form返回的错误,这是自己扩展的类，用的form的时候，继承
    ignore_error_keys = []  # 这个是存一些出错时忽略的参数名，给修改数据的时候用的，因为修改的时候，不是每个字段都要传的，这里只存传了并且校验没有通过的参数名

    @property
    def error_format(self):
        '''默认form返回的错误信息是一个字典套list，这个函数的作用是把它返回的错误信息，都拼成一个字符串'''
        error_message = ''
        for field, error_list in self.errors.get_json_data().items():
            if field not in self.ignore_error_keys:
                error_message += field + error_list[0].get('message')
        return error_message


class ParameterForm(forms.ModelForm):
    class Meta:
        model = models.Parameter  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time']


class LoginForm(forms.Form, ExtendForm):
    username = forms.CharField(min_length=5, max_length=20, required=True)
    password = forms.CharField(min_length=6, max_length=20, required=True)

    def clean(self):
        if not self.errors:
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            user = models.User.objects.filter(phone=username)
            if user:
                user = user.first()
                if user.check_password(password):
                    self.cleaned_data['user'] = user
                    return self.cleaned_data
                else:
                    self.add_error('password', '密码错误')
            else:
                self.add_error('username', '用户不存在')


class RegisterForm(forms.ModelForm, ExtendForm):
    class Meta:
        model = models.User
        exclude = ['is_delete', 'id']


class PermissionForm(forms.ModelForm, ExtendForm):
    class Meta:
        model = models.User
        exclude = ['is_delete', 'id']