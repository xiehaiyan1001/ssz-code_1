import pickle

import django_redis
from django.middleware.common import MiddlewareMixin
from django.http import QueryDict
from sky.settings import DEBUG, NO_LOGIN_LIST
from example.custom_response import NbResponse
from .const import redis_session


class PutMethodMiddleware(MiddlewareMixin):
    def process_request(self, request):
        '''请求过来之后先走到的这里'''
        if request.method == 'PUT':
            request.PUT = QueryDict(request.body)


class ExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        # 拦截异常的
        if not DEBUG:
            return NbResponse(code=500, msg='系统开小差了，请联系管理员 %s' % exception)


class SessionMiddleware(MiddlewareMixin):
    def check_uri(self, path):
        for uri in NO_LOGIN_LIST:  # 判断是uri否在白名单，在白名单的话，循环结束返回True
            if uri in path:
                return True
        return False

    def process_request(self, request):
        print('request.path_info', request.path_info)
        if not self.check_uri(request.path_info):  # 不在白名单，校验token
            print("request.META", request.META)
            token = request.META.get('HTTP_TOKEN')
            print("token", token)
            if token:
                redis = django_redis.get_redis_connection()
                user_b = redis.get(redis_session + token)
                if user_b:
                    user = pickle.loads(user_b)  # 从redis里面获取到用户信息
                    request.user = user  # 加到request里面
                    request.token = token  # request里面添加token
                else:
                    return NbResponse(-2, '请登录！')
            else:
                return NbResponse(-2, '请登录！')
