import django_redis, pickle
from django.test import TestCase
import django, os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')  # 设置django的配置文件
django.setup()

# redis 练习
# Create your tests here.
# redis = django_redis.get_redis_connection('redis2')
# data = {"name": "dsx", "age": 18}
# # 超时时间是秒
# # redis.set('test1', pickle.dumps(data), 5)
# redis.set('test1', pickle.dumps(data), 5)
#
# res = redis.get('test1')
# print("res", res)
# print("pickle.loads(res)", pickle.loads(res))


# http://127.0.0.1:8001/api/info?token=35fcbd2f83f55d6fed7a75495860968e
# {"code": 0, "msg": "操作成功", "data": {"roles": ["qa"], "avatar": "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif", "name": "12320202020", "introduction": "123@qq.com", "user": 1}}

from example import models

# models.Roles.objects.create(desc='测试', name='qa')

u = models.User.objects.first()
r = models.Roles.objects.first()
models.Permission.objects.create(roles=r,user=u)