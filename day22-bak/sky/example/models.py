from django.db import models
from sky import settings
from . import tools


class BaseModel(models.Model):
    '''公共字段'''
    is_delete_choice = (
        (1, '删除'),
        (0, '正常')
    )
    is_delete = models.SmallIntegerField(choices=is_delete_choice, default=0, verbose_name='是否被删除')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)  # auto_now_add的意思，插入数据的时候，自动取当前时间
    update_time = models.DateTimeField(verbose_name='修改时间', auto_now=True)  # 修改数据的时候，时间会自动变

    class Meta:
        abstract = True  # 只是用来继承的,不会创建这个表


class Parameter(BaseModel):
    name = models.CharField(max_length=200, verbose_name='参数名', unique=True)
    desc = models.CharField(max_length=200, verbose_name='描述')
    value = models.CharField(max_length=200, verbose_name='参数值')

    def __str__(self):
        return self.name

    class Meta:
        # django admin
        verbose_name = '全局参数表'
        verbose_name_plural = verbose_name
        db_table = 'parameter'
        # 最后创建的在最上面
        ordering = ['-id']


class User(BaseModel):
    '''用户表'''
    phone = models.CharField(verbose_name='手机号', max_length=11, unique=True)
    password = models.CharField(verbose_name='密码', max_length=32)
    name = models.CharField(verbose_name='名字', max_length=20)

    @staticmethod
    def make_password(raw_password):
        '''生成密码'''
        before_password = '%s%s' % (raw_password, settings.SECRET_KEY)  # 生成密码的算法，可以自己改
        after_password = tools.md5(before_password)
        return after_password

    def set_password(self, raw_password):
        '''设置密码'''
        self.password = self.make_password(raw_password)

    def check_password(self, raw_password):
        '''校验登录密码'''
        return self.make_password(raw_password) == self.password

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '用户表'
        verbose_name_plural = verbose_name
        db_table = 'user'


class Roles(BaseModel):
    desc = models.CharField(verbose_name='角色描述', max_length=20, unique=True)
    name = models.CharField(verbose_name='角色标识', max_length=20, unique=True)

    class Meta:
        verbose_name = '角色表'
        verbose_name_plural = verbose_name
        db_table = 'roles'


class Permission(BaseModel):
    roles = models.ForeignKey(Roles, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='角色')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='用户')

    class Meta:
        verbose_name = '权限表'
        verbose_name_plural = verbose_name
        db_table = 'permission'
