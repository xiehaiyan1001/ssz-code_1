from django.db.models import Q
from django.test import TestCase
import os, django

# Create your tests here.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')
django.setup()

from user import models

# models.Category.objects.get(id=5).delete()


# c = models.Category.objects.get(name='python')
# a = models.Article(title='django项目配置',content='django项目配置django项目配置django项目配置',category=c)
# a.save()
#
# a2 = models.Article(title='python面向对象',content='python面向对象python面向对象',category_id=3)
# a2.save()
# models.Article.objects.create(title='python111面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python113面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python114面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python115面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python116面向对象',content='python面向对象python面向对象',category_id=7)
#

# models.Article.objects.get(id=1).delete()

# 增
# category  = models.Category(name='Python')
# category.save()

# models.Category.objects.create(name='Java')
# models.Category.objects.create(name='Python')
# models.Category.objects.create(name='MySQL')
# models.Category.objects.create(name='心情日记')
# c_obj = models.Category.objects.get(name='Java')

# models.Article.objects.filter(category=c_obj)#手动通过分类查询文章

# print(c_obj.article_set.count())
# 一对多的时候，通过1，反查多
# 1_obj.多_set.all() #取全部 1_obj.多_set.count()#取数量  1_obj.多_set.filter(is_delete=False)#根据条件差

# 删

# c1 = models.Category.objects.get(id=3)
# c1.delete() #单个数据修改

# q = models.Category.objects.filter(name='python1')
# q.delete()

# 改
# c1 = models.Category.objects.get(id=3)
# c1.name = 'Java'
# c1.save() #单个数据修改

# q = models.Category.objects.filter(name='Python')
# q.update(name='python1')
# #查

# c1 = models.Category.objects.get(name='Python',id=2)
# 只能返回一条数据，如果返回多条会报错
# 如果指定的条件查询不到数据，也会报错
# query_set = models.Category.objects.filter()
# query_set = models.Category.objects.all()
# print(query_set)
# c = query_set[0]
# print(c.id)
# print(c.name)

# a = models.Article.objects.get(id=1)
#
# print(a.title)
# print(a.category.name)
# print(a.category.id)


# # 获取到某一个老师
# teacher_obj = models.Teacher.objects.get(id=2)
# # 获取某一个学生
# student_obj = models.Student.objects.get(id=2)
#
# # 创建多对多关系 方法一 接收对象
# teacher_obj.student.add(student_obj)
# # 创建多对多关系 方法二 接收主键ID
# teacher_obj.student.add(1)
#
# # 删除多对多关系
# teacher_obj.student.clear()  # 清除老师对应的所有学生 批量操作 比如：老师离职了
# teacher_obj.student.remove(2) # 清除id为2的学生，当操作，比如：学生劝退了
# teacher_obj.student.remove(student_obj)  # 也可以传入学生对象
# # 重置多对多关系
# teacher_obj.student.set([1, 2])  # 删除已有关系 重新创建新的多对多关系，接收学生ID，比如：老师去别的班当班主任了
#
# # 查询多对多
# # 正向查询
# students = teacher_obj.student.all()  # 获取老师所有的学生
#
# # 反向查询
# teachers = student_obj.teacher_set.all()  # 获取这个学生拥有的老师


# ORM操作
qs = models.Teacher.objects.all()  # 获取全部Teacher数据 qs
name = qs.values('name')  # 通过values进行字段过滤
count = qs.count()  # 获取qs的个数
firstData = qs.first()  # 获取qs的第一个数据
valueList = qs.values_list('name')  # 只获取数据中的Value

teacher = models.Teacher.objects.first()  # 获取第一个数据 obj
name = teacher.name  # 获取其中一个字段

teacher = models.Teacher.objects.raw('select * from teacher;')  # 执行原生sql qs

teacher = models.Teacher.objects.filter(name__contains='大')  # 过滤 模糊查询
teacher = models.Teacher.objects.filter(name__endswith='牛')  # 过滤 以什么为结尾
teacher = models.Teacher.objects.filter(name__startswith='牛')  # 过滤 以什么为开始
teacher = models.Teacher.objects.filter(name__in=['大师兄', '牛牛'])  # 过滤 在什么范围内
teacher = models.Teacher.objects.filter(name__isnull=True)  # 过滤 为空
teacher = models.Teacher.objects.filter(id__gt=1)  # 过滤 大于
teacher = models.Teacher.objects.filter(id__gte=1)  # 过滤 大于等于
teacher = models.Teacher.objects.filter(id__lt=1)  # 过滤 小于
teacher = models.Teacher.objects.filter(id__lte=1)  # 过滤 小于等于
teacher = models.Teacher.objects.filter(id__range=[1, 4])  # 过滤 在1到4之间 between and

teacher = models.Teacher.objects.exclude(id__gt=2)  # 排除 id 大于2的 其他用法和filter相同

# 多条件查询
# ADN 操作
teacher = models.Teacher.objects.filter(name='大师兄', id=2)  # 过滤 filter中都是and操作。
# OR 操作
# 通过django中提供的Q来实现负责的过滤
# 导入 from django.db.models import Q

teacher = models.Teacher.objects.filter(Q(name='大师兄') & Q(id=2))  # & 相当于sql中的and
teacher = models.Teacher.objects.filter(Q(name='大师兄') | Q(id=2))  # | 相当于sql中的 or
# 在Q前面可以通过~取反，代表name!=大师兄 and id=1
teacher = models.Teacher.objects.filter(~Q(name='大师兄') & Q(id=1))
# 使用Q之外 还可以使用关键字参数，再次进行过滤，Q必须写在关键字参数的前面
teacher = models.Teacher.objects.filter(~Q(name='大师兄') | Q(id=1), name__contains='牛')
