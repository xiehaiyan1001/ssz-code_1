from django import forms
from django.core.exceptions import ValidationError

# 公共验证器
from user import models


def test_title(value):
    if value == '111':
        raise ValidationError('标题是111')
    else:
        return value


class ArticleForm(forms.Form):
    title = forms.CharField(required=True,  # 是否必填
                            max_length=10,  # 最大长度5
                            min_length=1,  # 最小长度
                            strip=False,  # 是否去掉用户输入的空格 默认是True
                            validators=[test_title],  # 自定义验证方式 例如：公用验证方法 最先执行
                            empty_value='TitleInit',  # 默认值 当前端不传递时接收到的信息
                            error_messages={  # 错误信息提示
                                'required': '必填',
                                'max_length': '最大10个字符',
                            })

    # 最后执行
    def clean(self):
        # 所有数据都在self.cleaned_data中
        if self.cleaned_data.get('title') == '333':
            raise ValidationError('标题是333')
        else:
            # 如果验证通过则返回self.cleaned_data
            return self.cleaned_data

    # # 第二个被执行
    def clean_title(self):
        # 所有数据都在self.cleaned_data中
        if self.cleaned_data.get('title') == '222':
            raise ValidationError('标题是222')
        else:
            # 返回验证的字段的数据即可
            return self.cleaned_data.get('title')


# Form 和 Model结合，能够省略定Form定义字段。共用Model
class StudentForm(forms.ModelForm):
    # 使用forms.ModelForm后，一样也可以像forms.Form一样自己定义字段定义
    # 自己在form中定义的字段，在存数据库时会出问题，因为数据库没这个字段，所以不建议这样使用。
    # 或者自己要处理下返回的cleaned_data
    # 这种常用于，认为model写的验证不够完美，可以重新定义这个字段的验证规则
    user = forms.CharField(required=True,  # 是否必填
                           max_length=10,  # 最大长度5
                           min_length=1,  # 最小长度
                           strip=False,  # 是否去掉用户输入的空格 默认是True
                           validators=[test_title],  # 自定义验证方式 例如：公用验证方法 最先执行
                           empty_value='TitleInit',  # 默认值 当前端不传递时接收到的信息
                           error_messages={  # 错误信息提示
                               'required': '必填',
                               'max_length': '最大10个字符',
                           })

    class Meta:
        model = models.Student  # 创建类变量和model建立映射关系
        fields = '__all__'  # 验证全部字段
        # fields = ['age']  # 显示指定列验证 只有指定的验证的字段才会出现在cleaned_data中
        # exclude = ['age', 'name']  # 排除了就不会出现在cleaned_data中

    # clean的原则和forms.Form相同
    def clean(self):
        print('clean')
        # 由于model中没有user字段，则在这里删除掉
        self.cleaned_data.pop('user')
        return self.cleaned_data

    # clean_xxx的原则和forms.Form相同
    def clean_user(self):
        print('clean_user')
        return self.cleaned_data.get('user')

    # clean_xxx的原则和forms.Form相同
    def clean_name(self):
        print('clean_name')
        return self.cleaned_data.get('name')
