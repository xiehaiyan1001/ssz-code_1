from . import models

def process_category(request):

    categories = models.Category.objects.all()
    return {'categories':categories}

def process_title(request):
    web_site = models.WebSite.objects.all().first()
    return {'web_site':web_site}