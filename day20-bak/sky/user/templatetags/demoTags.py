from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def simple_tag_demo(x, y,z):
    return x + y + z


@register.filter
def filter_demo(x,y):
    return x+y
