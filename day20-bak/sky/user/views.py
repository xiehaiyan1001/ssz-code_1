import datetime
import json
import time
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from . import models


# Create your views here.

def user_info(request):
    s = '你好'
    return HttpResponse(s)  # 返回的字符串的话用它


def welcome(request):
    username = 'changcaixia'  # request
    username_zh = '常彩霞-使用的reaplace'  # sql查

    return render(request, 'welcome.html', {'username_zh': username_zh})


def index(request):
    print("index,....")
    limit = request.GET.get('limit', 5)
    page = request.GET.get('page')
    print('limit', limit)
    print('page', page)
    all_articles = models.Article.objects.all().order_by('id')  # 获取全部文章数据
    page_obj = Paginator(all_articles, limit)  # 通过django自带分页器进行分页 每页5条数据
    page_data = page_obj.get_page(page)  # 获取第几页的数据集
    return render(request, 'index.html', {'articles': page_data, 'page': page_obj, 'page_limit': 5})


def category(request, id):
    category_obj = models.Category.objects.get(id=id)
    articles = models.Article.objects.filter(category=category_obj)
    return render(request, 'category.html', {'articles': articles, 'category_obj': category_obj})


def article(request):
    if request.method == 'GET':
        return render(request, 'post.html')
    else:
        from user.forms import ArticleForm
        form = ArticleForm(request.POST)
        # 是否form验证通过
        f = form.is_valid()
        if f:
            # 验证通过后返回的数据信息
            print('form.cleaned_data', form.cleaned_data)
            return HttpResponseRedirect('/post')
        else:
            # 获取错误信息，默认errors 是个标签错误类型
            # get_json_data 返回字典类型的数据
            print('form.error', form.errors.get_json_data())
            return HttpResponse()


def login(request):
    username = request.POST.get('userbane')

    return HttpResponse('{"code":0,"msg":"ok"}')


def archive(request):
    categories = models.Category.objects.all()


def tagTest(request):
    if request.method == 'GET':
        return render(request, 'tag_test.html',
                      {'nav': '<div>测试django自带tag</div>', "title": "Django自带filter测试", "navs": ['test', 'django'],
                       'cur_time': datetime.datetime.now()})


def student(requests):
    if requests.method == 'GET':
        return render(requests, 'student.html')
    else:
        # 导入ModelForm
        from user.forms import StudentForm
        # 使用写好的Form
        form = StudentForm(requests.POST)
        # 查看form验证后结果是否通过
        f = form.is_valid()
        if f:
            models.Student.objects.create(**form.cleaned_data)
            print('验证结果', f)
            # 验证通过的数据
            print('form.cleaned_data', form.cleaned_data)
        else:
            # 获取错误信息，默认errors 是个标签错误类型
            # get_json_data 返回字典类型的数据
            print('form.error', form.errors.get_json_data())
        return HttpResponse()
