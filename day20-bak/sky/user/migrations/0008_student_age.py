# Generated by Django 2.1 on 2020-08-27 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_student_teacher'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='age',
            field=models.IntegerField(default=1, verbose_name='学生年龄'),
            preserve_default=False,
        ),
    ]
