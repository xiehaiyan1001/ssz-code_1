# from urllib import request
# from urllib.parse import urlencode
# url = 'http://api.nnzhp.cn/api/user/stu_info'
# data = {'stu_name':'小黑','age':18}
# str_data = urlencode(data)
#
# result = request.urlopen(url+'?'+str_data).read()#get请求
# # result = request.urlopen(url,data=str_data.encode() ).read() #post请求
# print(result.decode())
import datetime

import requests
url = 'http://api.nnzhp.cn/api/user/stu_info'


#get请求
# req = requests.get(url,params={'stu_name':'abc'})
# print(req.json()) #返回的json直接帮你转成了字典
# print(req.text) #返回的就是字符串,如果返回的不是json的话，就要用它了
# print(req.status_code) #返回的http状态码
# print(req.content)  #返回的bytes类型的，下载文件的时候用它

#post请求
# url="http://api.nnzhp.cn/api/user/login"
# req = requests.post(url,data={"username":"niuhanyang","passwd":"aA123456"})
# print(req.json())

#发送header、cookie
# url="https://qun.qq.com/cgi-bin/qun_mgr/get_group_list"
# data ={"bkn": 208992859}

# d = {'pgv_pvi': '6636933120', 'RK': 'gRZhhBpNbS', 'ptcz': '14bab564718e3e1048a09cc0e18a23f7c51f20d5b93da610cc1427f51f63a2f8', 'pgv_pvid': '4990195883', 'ts_uid': '5190463916', 'uin': 'o0511402865', 'pgv_si': 's7505852416', 'skey': '@2ttDS8Ljw', 'p_uin': 'o0511402865', 'pt4_token': 'AgqIsYBhO1b82zx1N4SxoGpCxGV0d38ss7jCI1nYfIg_', 'p_skey': '9nlMjw4Uy44*Hu5iL3DOFonmAtZtExiniLykrsIRKmM_', 'traceid': '14035c8a79'}
# req = requests.post(url,data,cookies=d)

# header = {'cookie':"pgv_pvi=6636933120; RK=gRZhhBpNbS; ptcz=14bab564718e3e1048a09cc0e18a23f7c51f20d5b93da610cc1427f51f63a2f8; pgv_pvid=4990195883; ts_uid=5190463916; uin=o0511402865; pgv_si=s7505852416; skey=@2ttDS8Ljw; p_uin=o0511402865; pt4_token=AgqIsYBhO1b82zx1N4SxoGpCxGV0d38ss7jCI1nYfIg_; p_skey=9nlMjw4Uy44*Hu5iL3DOFonmAtZtExiniLykrsIRKmM_; traceid=14035c8a79"}
# req = requests.post(url,data,headers=header)
# print(req.json())

url="https://oapi.dingtalk.com/robot/send?access_token=44402c9408df8cf3f429c02a20399fc34604f98cf572fcaeaa3f9592426176a7"
today = datetime.datetime.now()
d = {"msgtype": "text","text": {"content": "现在是%s，大家不要忘记写作业哦！暗号：besttest"%today} }
req = requests.post(url,json=d)
print(req.json())
print(req.cookies) #获取cookies

#下载文件
# url = "https://q4.qlogo.cn/g?b=qq&nk=516481730&s=140"
# req = requests.get(url)
#
# f = open('wjl.jpg','wb')
# f.write(req.content)
# f.close()


#上传文件
url = "http://api.nnzhp.cn/api/file/file_upload"
f = open('sxy.mp4','rb')
data = {'file':f}
r = requests.post(url,files=data)
print(r.json())
f.close()