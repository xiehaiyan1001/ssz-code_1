import xlwt

book = xlwt.Workbook()
sheet = book.add_sheet('sheet1')


title = ['编号','姓名','语文成绩','数学成绩','英语成绩','总分','平均分']
#处理表头
row = 0
for t in title:
    sheet.write(0,row,t)
    row+=1

data = [
	["1","小花",99,100,98.5],#1
	["2","小王",90,30.5,95],#
	["3","小明",67.5,49.6,88]#
]

for row,v in enumerate(data,1): #行
    sum_score = sum(v[2:])#算总分
    avg_score = round(sum_score / 3,2) #算平均分
    v.append(sum_score)
    v.append(avg_score)
    for col,value in enumerate(v):
        sheet.write(row,col,value)

book.save("students.xls")  #如果你用的是wps的话

#[1,2,3,4]    0 ->1   1->