#1、写一个删除日志的程序，删除5天前或为空的日志，不包括当天的

#1、获取logs目录下所有的日志文件 os.walk()
#2、获取日志文件的时间 # [apache, 2020-04-24 log][1].split(.)[0]
#3、判断时期是否是5天前，当天的日期 - 5*24*60*60 #5天前的时间
#4、再判断是否为空文件，如果是空文件，文件日期不是今天的，也删掉
import os,time

def str_to_timezone(str=None,format="%Y-%m-%d %H:%M:%S"):
    #这个函数是格式化好的时间转时间戳的，如果不传参数默认返回当前时间戳
    if str:
        time_tuple = time.strptime(str,format)
        result = time.mktime(time_tuple)
    else:
        result = time.time()
    return int(result)

def clean_log(path,day=5):
    if not os.path.exists(path):
        print('路径不存在')
        return
    for cur_path,dirs,files in os.walk(path):
        for file in files:
            if file.endswith('.log'):
                file_time = file.split('_')[-1].split('.')[0] #2020-05-23
                timezone = str_to_timezone(file_time,'%Y-%m-%d')
                abs_path = os.path.join(cur_path,file)
                if timezone < (time.time() - day * 24 * 60 *60): #判断时间
                    os.remove(abs_path)
                elif os.path.getsize(abs_path) == 0 and time.strftime('%Y-%m-%d')!=file_time:
                    os.remove(abs_path)
clean_log('logs')
