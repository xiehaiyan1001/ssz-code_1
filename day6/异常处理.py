# l = [ 1,2,3]
# d = {}
#
# try:
#     # print(l[4])
#     print(d['name'])
# except Exception as e:
#     print("出异常了",e)
# else:
#     print("不出异常的时候走到这里")
# finally:
#     print("什么时候走这里呢")
# except IndexError as e:
#     print("下标不存在")
#     print(e)
# except KeyError as e:
#     print("key不存在")
#     print(e)


import pymysql
import nnlog
import traceback
log = nnlog.Logger('python.log',level='debug',backCount=5,when='D')
mysql_info = {
    'host':'118.24.3.40',
    'port':3306,
    'db':'jxz',
    'password':'123456',
    'charset':'utf8',
    'autocommit':True,
    'user':'jxz'
}
def op_db(sql,one_tag=True):
    try:
        log.debug("开始连接数据库，数据库连接信息是%s"%mysql_info)
        connect = pymysql.connect(**mysql_info)
    except Exception as e:
        log.debug("连接数据失败")
        log.error("连接数据失败,请检查")
        log.error(traceback.format_exc())
        # traceback.format_exc()拿到出错的那一大坨字符串
        #traceback.print_exc()#只print错误信息
        return "001"
    else:
        log.debug("开始建立游标")
        cur = connect.cursor(pymysql.cursors.DictCursor)
        try:
            log.debug("开始执行sql")
            log.info("sql语句是%s"%sql)
            cur.execute(sql)
        except Exception as e:
            log.warning("sql错误！")
            return "002"
        else:
            if one_tag:
                result = cur.fetchone()#{}
            else:
                result = cur.fetchall()#[]
        finally:
            cur.close()
            connect.close()
    return result

