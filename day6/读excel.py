import xlrd

book = xlrd.open_workbook('students.xls')
sheet = book.sheet_by_index(0)
# sheet = book.sheet_by_name('sheet1')
print(sheet.cell(0,0).value) #指定单元格的内容
print(sheet.row_values(1))#取整行的数据
print(sheet.col_values(0))#取整列的数据
print(sheet.nrows) #多少行
print(sheet.ncols) #多少列