from xlutils import copy
import xlrd
import os

book = xlrd.open_workbook('students.xls')

new_book = copy.copy(book)

sheet = new_book.get_sheet(0)
sheet.write(0,0,"id")
sheet.write(0,1,"name")

os.rename('students.xls','students_bak.xls')

new_book.save('students.xls')
