# a = 1
# b = 2
# a,b = b,a
#
# print()
# #b = 2
# #a = 1
#
# c = None
#
# c = a
# a = b
# b = c
# print(a,b)


#三元表达式
a = 1
sex = '男' if a==1 else '女'

# if a==1:
#     sex='男'
# else:
#     sex = '女'

#列表生成式

# l = [1,5,7,6,32,352,623,5626,2632,626]
# l2 = [str(i)+'km' for i in l]
# l3 = [str(i)+'km' for i in l if i>=30]
#
# print(l3)

for i in range(3):
    for j in range(5):
        print(j)