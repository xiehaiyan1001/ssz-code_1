import json
#python的数据类型和json互相转换的
#json就是一串字符串
d = {"key1":"v1",'k2':'v2','k3':'v3','name':'刘婕'}
for i in range(20):
    d['key%s'%i] = i

with open('u2.txt','w',encoding='utf-8') as f:
    # json_str = json.dumps(d,indent=4,ensure_ascii=False) #转成字符串，转成json了
    # f.write(json_str)
    json.dump(d,f,ensure_ascii=False,indent=4)


# dic = json.loads(json_str) #json转字典
# print(dic)

# with open('u.txt',encoding='utf-8') as f:
#     # content = f.read()
#     # print(json.loads(content))
#     result = json.load(f)
#     print(result)
