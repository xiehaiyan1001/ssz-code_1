#r   r+  rb   rb+   r+读是没有问题的，写的话，会覆盖原来的内容，文件不存在会报错
#w   w+  wb   wb+
#a   a+  ab   ab+
f = open('users.txt','a+',encoding='utf-8')
f.seek(0)
f.truncate()#删除文件内容的意思
# f.write('~~~~~~~')
# print(f.read())
f.close()
