
#1、简单粗暴直接的
    #1、读取文件所有内容
    #2、replace
    #3、清空文件
    #4、把新的内容写进去
    #修改小文件的话可以用这种方式
# with open('数据.txt','r+',encoding='utf-8') as f:
#     content = f.read()
#     new_content = content.replace('100','1000000000000')
#     f.seek(0)
#     f.truncate()
#     f.write(new_content)
#     f.flush()#立即刷新缓冲区的内容，写到磁盘上
#2、两个文件操作
    #1、r模式打开a文件,w模式打开b文件
    #2、逐行读取a文件的内容，读完之后替换内容
    #3、把替换完的内容写到b文件里面
    #4、把a文件删掉，把b文件的名字改成a文件
import os
with open('数据.txt',encoding='utf-8') as f1, open('数据2.txt','w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('4','444444')
        f2.write(new_line)

os.remove('数据.txt')
os.rename('数据2.txt','数据.txt')