def a():
    pass
def b(name,age): #必填参数，位置参数
    pass
def op_file(file,content=None): #默认值参数
    with open(file,'a+',encoding='utf-8') as f:
        if content:
            f.write(str(content))
        else:
            f.seek(0)
            result = f.read()
            return result

def send_sms(*args):#可变参数
    print(args)
#send_sms(1,2,3,3,4,24)
def t(**kwargs):#关键字参数,传入的必须得指定关键字
    print(kwargs)
# t()
def test(name,content=None,*args,**kwargs):
    print(name)
    print(content)
    print(args)
    print(kwargs)

#必填参数、默认值、可变参数、关键字参数 必须得按照这个顺序来
#必填参数必须写到默认值参数前面
#默认参数必须在可变参数前面
#可变参数必须在关键字参数前面


def test2(name,phone,qq,addr,sex):
    print(name)
    print(phone)
    print(qq)
    print(addr)
    print(sex)


# test('183','mingzi','1111','beijing','女')
# test(age=36,addr='beijing',qq='1111',sex='xxx',name='xx')
# test('xiaoming','1823423',qq='xxx',sex='xxx',addr='xxxx')
# test('xiaoming','1823423',qq='xxx',sex='xxx') #错误的，不能这么写
#open('file','w',encoding='utf-8')