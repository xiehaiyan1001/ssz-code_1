from django.http import HttpResponse
from django.middleware.common import MiddlewareMixin
from . import models
class TestMiddleWare(MiddlewareMixin):

    def process_request(self,request):
        '''请求过来之后先走到的这里'''
        path = request.path_info
        interface = models.Interface.objects.filter(path = path).first()
        if interface:
            return HttpResponse(interface.response)


    def process_response(self,request,response):
        #拦截返回的
        print('response....')
        return response

    # def process_exception(self,request,exception):
    #     #拦截异常的
    #     print('出异常了。。')
    #     return HttpResponse('hhhh')

