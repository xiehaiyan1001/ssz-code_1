from django.middleware.common import MiddlewareMixin
from django.http import QueryDict
from sky.settings import DEBUG
from example.custom_response import NbResponse


class PutMethodMiddleware(MiddlewareMixin):
    def process_request(self, request):
        '''请求过来之后先走到的这里'''
        if request.method == 'PUT':
            request.PUT = QueryDict(request.body)


class ExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        # 拦截异常的
        if not DEBUG:
            return NbResponse(code=500, msg='系统开小差了，请联系管理员 %s' % exception)
