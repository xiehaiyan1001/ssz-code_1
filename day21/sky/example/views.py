import datetime
from itertools import chain

from django.core.paginator import Paginator
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views import View

from .forms import ParameterForm
from . import models
from example.custom_view import NbView


# from django.forms.models import model_to_dict

def model_to_dict(instance, fields=None, exclude=None):
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if isinstance(value, datetime.datetime):
            value = value.strftime('%Y-%m-%d %H:%M:%S')
        if isinstance(value, datetime.date):
            value = value.strftime('%Y-%m-%d')
        data[f.name] = value
    return data


# FBV func base view
# 用函数实现的 view 叫做FBV
def f_parameter(requests):
    if requests.method == 'GET':
        # get请求时 获取传递过来的第几页数据
        page = requests.GET.get('page')
        qs = models.Parameter.objects.filter(is_delete=0)
        page_obj = Paginator(qs, 5)  # page_obj Paginator实例对象
        # 获取第几页的数据
        page_data = page_obj.get_page(page)
        # 用于存储返回的dict类型数据
        data_list = []
        for data in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(data, exclude=['is_delete'])
            data_list.append(tmp_data)
        return JsonResponse({"code": 0, "data": data_list})
    elif requests.method == 'POST':
        # 通过from进行数据验证
        form_obj = ParameterForm(requests.POST)
        # 数据验证是否通过
        if form_obj.is_valid():
            # 创建数据
            # form_obj.cleaned_data 是所有验证通过的数据
            models.Parameter.objects.create(**form_obj.cleaned_data)
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            # 返回错误信息
            return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
    elif requests.method == 'PUT':
        # django 并没有处理PUT的数据 requests.PUT
        # 实际put传过来的数据在requests.body
        # 需要导入 from django.http import QueryDict  来处处理数据 QueryDict(requests.body)
        # print(requests.PUT)
        # print('requests.body',QueryDict(requests.body))
        # PUT 传过来的数据
        # 更新数据，需要告知，要更新那条数据
        put_data = QueryDict(requests.body)
        print('requests.body', QueryDict(requests.body))
        # 获取要更新那条数据的主键id
        p_id = put_data.get('id')
        # 通过id 从数据库中取这条数据  obj
        data_obj = models.Parameter.objects.get(id=p_id)
        # 参数1 是前端传过来的数据， 参数2 是数据库中获取的数据
        form_obj = ParameterForm(put_data, instance=data_obj)
        if form_obj.is_valid():
            # 通过form的save方法进行数据更新
            form_obj.save()
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
    elif requests.method == 'DELETE':
        #  删除时，要知道 删除的是那条数据 获取主键id
        p_id = requests.GET.get('id')
        # 删除分为2种，逻辑删除、物理删除
        # 逻辑删除： 只是通过改变 某个字段的状态，来控制是否删除
        # 比如：is_delete 数据很重要未来有通过状态在改变回来的需要
        # 物理删除： 直接将数据删掉 delete掉 数据不是很重要。
        # 逻辑删除例子
        # models.Parameter.objects.filter(id=p_id).update(is_delete=1) # 这种更新方式不会触发 updatetime
        # obj = models.Parameter.objects.filter(id=p_id).first()
        # obj.is_delete = 1
        # obj.save()

        # 物理删除
        models.Parameter.objects.filter(id=p_id).delete()
        return JsonResponse({"msg": "delete"})
    else:
        return HttpResponse("errors")


# CBV class Base view
# 面向对象的语言  通过class  可以 用到继承 多继承 面向对像
# cbv 的视图
# class Parameter(View):
#     def get(self, requests):
#         # get请求时 获取传递过来的第几页数据
#         page = requests.GET.get('page')
#         qs = models.Parameter.objects.filter(is_delete=0)
#         page_obj = Paginator(qs, 5)  # page_obj Paginator实例对象
#         # 获取第几页的数据
#         page_data = page_obj.get_page(page)
#         # 用于存储返回的dict类型数据
#         data_list = []
#         for data in page_data:
#             # 通过model_to_dict 转换成dict
#             # fields  指定返回那些字段
#             # exclude 过滤那些字段
#             # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
#             tmp_data = model_to_dict(data, exclude=['is_delete'])
#             data_list.append(tmp_data)
#         return JsonResponse({"code": 0, "data": data_list})
#
#     def post(self, requests):
#         # 通过from进行数据验证
#         form_obj = ParameterForm(requests.POST)
#         # 数据验证是否通过
#         if form_obj.is_valid():
#             # 创建数据
#             # form_obj.cleaned_data 是所有验证通过的数据
#             models.Parameter.objects.create(**form_obj.cleaned_data)
#             return JsonResponse({"code": 200, "msg": "成功"})
#         else:
#             # 返回错误信息
#             return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
#
#     def put(self, requests):
#         # django 并没有处理PUT的数据 requests.PUT
#         # 实际put传过来的数据在requests.body
#         # 需要导入 from django.http import QueryDict  来处处理数据 QueryDict(requests.body)
#         # print(requests.PUT)
#         # print('requests.body',QueryDict(requests.body))
#         # PUT 传过来的数据
#         # 更新数据，需要告知，要更新那条数据
#         put_data = QueryDict(requests.body)
#         print('requests.body', QueryDict(requests.body))
#         # 获取要更新那条数据的主键id
#         p_id = put_data.get('id')
#         # 通过id 从数据库中取这条数据  obj
#         data_obj = models.Parameter.objects.get(id=p_id)
#         # 参数1 是前端传过来的数据， 参数2 是数据库中获取的数据
#         form_obj = ParameterForm(put_data, instance=data_obj)
#         if form_obj.is_valid():
#             # 通过form的save方法进行数据更新
#             form_obj.save()
#             return JsonResponse({"code": 200, "msg": "成功"})
#         else:
#             return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
#
#     def delete(self, requests):
#         #  删除时，要知道 删除的是那条数据 获取主键id
#         p_id = requests.GET.get('id')
#         # 删除分为2种，逻辑删除、物理删除
#         # 逻辑删除： 只是通过改变 某个字段的状态，来控制是否删除
#         # 比如：is_delete 数据很重要未来有通过状态在改变回来的需要
#         # 物理删除： 直接将数据删掉 delete掉 数据不是很重要。
#         # 逻辑删除例子
#         # models.Parameter.objects.filter(id=p_id).update(is_delete=1) # 这种更新方式不会触发 updatetime
#         # obj = models.Parameter.objects.filter(id=p_id).first()
#         # obj.is_delete = 1
#         # obj.save()
#
#         # 物理删除
#         models.Parameter.objects.filter(id=p_id).delete()
#         return JsonResponse({"msg": "delete"})


# 简化代码
# 大多数接口 都可以继承getview 来实现get的方法
# 继承 NbView 既有get post put  delete
class SParameter(NbView):
    # 重写了父类的变量
    # 抽象出不同的地方  让相同的代码进行复用
    model_class = models.Parameter
    form_class = ParameterForm
    # fields = ['name','desc','value']
    # exclude_fields = ['name']
    filter_fields = ['value']
    search_fields = ['name','desc']


