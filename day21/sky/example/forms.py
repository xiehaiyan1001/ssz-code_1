from django import forms
from example import models


class ParameterForm(forms.ModelForm):
    class Meta:
        model = models.Parameter  # 创建类变量和model建立映射关系
        exclude = ['id','is_delete','update_time','create_time']

