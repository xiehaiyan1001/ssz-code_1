from django.http import JsonResponse


class NbResponse(JsonResponse):
    def __init__(self, code=200, msg='操作成功', **kwargs):
        data = {"code": code, "msg": msg}
        data.update(kwargs)
        super().__init__(data=data, json_dumps_params={"ensure_ascii": False})
