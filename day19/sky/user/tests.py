from django.test import TestCase
import os,django
# Create your tests here.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')
django.setup()

from user import models

# models.Category.objects.get(id=5).delete()


# c = models.Category.objects.get(name='python')
# a = models.Article(title='django项目配置',content='django项目配置django项目配置django项目配置',category=c)
# a.save()
#
# a2 = models.Article(title='python面向对象',content='python面向对象python面向对象',category_id=3)
# a2.save()
# models.Article.objects.create(title='python111面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python113面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python114面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python115面向对象',content='python面向对象python面向对象',category_id=7)
# models.Article.objects.create(title='python116面向对象',content='python面向对象python面向对象',category_id=7)
#

# models.Article.objects.get(id=1).delete()

#增
# category  = models.Category(name='Python')
# category.save()

# models.Category.objects.create(name='Java')
# models.Category.objects.create(name='Python')
# models.Category.objects.create(name='MySQL')
# models.Category.objects.create(name='心情日记')
c_obj = models.Category.objects.get(name='Java')

# models.Article.objects.filter(category=c_obj)#手动通过分类查询文章

print(c_obj.article_set.count())
#一对多的时候，通过1，反查多
# 1_obj.多_set.all() #取全部 1_obj.多_set.count()#取数量  1_obj.多_set.filter(is_delete=False)#根据条件差

#删

# c1 = models.Category.objects.get(id=3)
# c1.delete() #单个数据修改

# q = models.Category.objects.filter(name='python1')
# q.delete()

#改
# c1 = models.Category.objects.get(id=3)
# c1.name = 'Java'
# c1.save() #单个数据修改

# q = models.Category.objects.filter(name='Python')
# q.update(name='python1')
# #查

# c1 = models.Category.objects.get(name='Python',id=2)
#只能返回一条数据，如果返回多条会报错
#如果指定的条件查询不到数据，也会报错
# query_set = models.Category.objects.filter()
# query_set = models.Category.objects.all()
# print(query_set)
# c = query_set[0]
# print(c.id)
# print(c.name)

# a = models.Article.objects.get(id=1)
#
# print(a.title)
# print(a.category.name)
# print(a.category.id)

