from django.contrib import admin

# Register your models here.
from . import models

class ArticleAdmin(admin.ModelAdmin):
    list_per_page = 10 #每页最多展示多少条数据

    list_display = ['id','title','create_time'] #展示哪些字段

    list_filter = ['category'] #按照哪些字段来筛选

    search_fields = ['title','content']

class CategoryAdmin(admin.ModelAdmin):
    list_per_page = 10 #每页最多展示多少条数据

    list_display = ['id','name','create_time'] #展示哪些字段

    search_fields = ['name']

class InterfaceAdmin(admin.ModelAdmin):
    list_per_page = 10 #每页最多展示多少条数据

    list_display = ['id','name'] #展示哪些字段

    search_fields = ['name']

admin.site.register(models.Interface,InterfaceAdmin)

admin.site.register(models.Article,ArticleAdmin)


admin.site.register(models.Category,CategoryAdmin)

admin.site.register(models.WebSite)