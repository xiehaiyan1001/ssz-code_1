#守护线程，一但主线程死掉，那么守护线程不管有没有执行完成，全都结束
import threading
import time
def talk(name):
    print('正在和%s聊天'%name)
    time.sleep(200)

def shipin(name):
    print('正在和%s视频'%name)
    time.sleep(300)

print("qq主窗口")
t1 = threading.Thread(target=talk,args=['刘小燕'])
# t1.setDaemon(True) #设置线程为守护线程
t1.start()

t2 = threading.Thread(target=shipin,args=['蔡明昌'])
# t2.setDaemon(True) #设置线程为守护线程
t2.start()

time.sleep(5)

print('结束。。。')