#线程锁：
 #多个线程同时操作同一个数据的时候，会有问题
import threading
count = 0
lock = threading.Lock()
def test():
    global count
    print(threading.current_thread())
    lock.acquire()#加锁
    count+=1
    lock.release()#解锁
    #线程死锁


for i in  range(3):
    t = threading.Thread(target=test)
    t.start()

