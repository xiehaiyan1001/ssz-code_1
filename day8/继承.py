class Lz:
    country = 'Beijing'
    house = 8
    money = 99999
    def driver(self):
        print('开车')
    def cook(self):
        print('做饭')
    def make_money(self):
        print('挣钱')
class Lw:
    def majiang(self):
        print('majiang')
    def maicai(self):
        print('maicai')

class Car:
    __key = '车轱辘下头' #私有的变量和私有的方法不能继承
    def run(self):
        print(self.__key)
        print('run')

class CcxCar(Car): #--
    def run(self):
        print('拐弯跑。。')

class Cmc(CcxCar):
    def fly(self):
        print('fly')


class Xz(Lz,Lw):
    pass

xz = Xz()
xz.cook()
xz.make_money()
xz.driver()
xz.majiang()