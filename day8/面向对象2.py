#1、化零为整
#2、类 --》模型 class Test:pass
#3、类需要实例化之后才可以用  wy = Test()  wy.xxx wy.xxx

#类方法、类变量
class Staff:
    '''员工类'''
    nationality = 'China' #类变量
    @classmethod #装饰器，其实就是一个函数
    def help(cls): #cls就是Staff，类方法
        '''
        一些公共的方法，就用类方法
        类方法里面不能调用实例方法、不能调用实例变量
        '''
        print('这里的员工都是来自%s'% cls.nationality)
        print('我们每个人每个月的补贴',cls.fabudie())

    @classmethod
    def fabudie(cls):
        print('大家都发500的补贴')

    @classmethod
    def haha(cls):
        boss = cls("BOSS",1,'老板办公室',10000000)
        boss.fagongzi()

    def __init__(self,name,id_no,position,salary):
        self.name = name #实例变量、成员变量
        self.id_no = id_no
        self.position = position
        self.salary = salary

        #this.xxx = xxx
        #this.fun()

    def fagongzi(self):#实例方法、成员方法
        print('这个月发了%s'%self.salary)
        print(self.fabudie())

    def my_self(self):
        print("my name is %s,i'm from %s" %(self.name,self.nationality) )
        self.help()


Staff.haha()

# Staff.nationality = 'Japan'
cmc = Staff('蔡明昌',12352,'c-1-3',99999)
cmc.nationality = 'USA'
cmc.salary
cmc.name
cmc.position
# wy = Staff('王月',3235,'c-2-3',8888)
# cmc.my_self()