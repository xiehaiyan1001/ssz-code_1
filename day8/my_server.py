import fastapi
import uvicorn
from pydantic import BaseModel

server = fastapi.FastAPI() #实例化一个服务

class User(BaseModel):
    username:str
    password:str
    phone:str

@server.post('/user')
def user(u:User):#入参是json的
    print(u.username)
    print(u.password)
    print(u.phone)
    return {'code':0}



@server.get('/')
def index(id:int,sex:str):
    if sex == 'nan':
        data = {'id':id,'sex':'男'}
    else:
        data = {'id':id,'sex':'女'}
    return data

@server.post('/')
def order(type:str):
    if type == 'success':
        data = {'id':1,'order_amount':10000}
    else:
        data = {'id':id,'order_amount':0}
    return data


#ip:port/docs
if __name__ == '__main__':
    uvicorn.run('my_server:server', port=9000, debug=True)

