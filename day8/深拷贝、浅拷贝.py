import copy
a = [1,1,2,3,4,5,1,4,5,6,8,9]
#a2 = [1,1,2,3,4,5,1,4,5,6,8,9] 手动方式写的
a2 = copy.deepcopy(a)  #深拷贝
a3 = a.copy()
a4 = copy.copy(a)
print('a',id(a))
print('a2',id(a2))
print('a3',id(a3))
print('a4',id(a4))

#


#浅拷贝、深拷贝
for i in a2:
    if i % 2 !=0:
        a.remove(i)
print(a)  #循环删list会导致下标错乱


import random
class User:
    def __init__(self,id,status):
        self.id = id
        self.status = status
class Order:
    def __init__(self,id,status):
        self.id = id
        self.status = status

users = []
orders = []
u_fail_count = 0
for i in range(20):
    u = User(i,random.choice(['fail','succes']))
    o = Order(i,random.choice(['fail','succes']))
    orders.append(o)
    users.append(u)
print('失败的个数',u_fail_count)
print('成功的个数',20 - u_fail_count )


def delete_item(items):
    items2 = copy.deepcopy(items)
    for item in items2:
        if item.status == 'fail':
            items.remove(item)
    return items
# result = delete_item(users)
# print(len(result))