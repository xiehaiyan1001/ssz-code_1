class Car:
    def run(self):
        print('run')

class CcxCar(Car): #覆盖式的
    def run(self):
        print('拐弯跑')

class CmcCar(Car): #在父类方法的基础上做修改
    def run(self):
        super().run()
        print('充电')


