import threading
import requests
import hashlib
import time
def down_load(url):
    name = hashlib.md5(url.encode()).hexdigtest()
    r = requests.get(url)
    with open('%s.jpg'%name,'wb') as fw:
        fw.write(r.content)

l = [
    'http://www.nnzhp.cn/wp-content/themes/QQ/images/logo.jpg',
    'http://www.nnzhp.cn/wp-content/uploads/2016/12/2016aj5kn45fjk5-150x150.jpg',
    'http://www.nnzhp.cn/wp-content/themes/QQ/images/thumbnail.png'
]
import threading
import time
def insert_db():
    time.sleep(3)
    print('insert_db over')

# start_time = time.time()
# for i in range(3): #串行的方式
#     insert_db()
# end_time = time.time()
# print('串行的执行的时间',end_time - start_time )

threads = []
start_time2 = time.time()


#1、第一种方式，麻烦一点
# for i in range(3):
#     t = threading.Thread(target=insert_db)
#     t.start()
#     threads.append(t)
#
# for t in threads:
#     t.join()

#2、第二种方式，思路是判断当前存活的线程个数
for i in range(3):
    t = threading.Thread(target=insert_db)
    t.start()
#1!=1
while threading.activeCount()!=1:
    pass

end_time2 = time.time()
print('多线程执行的时间',end_time2 - start_time2)
print('锁门...')

#只是主线程执行完它自己工作的时间，并不包括子线程执行的时间

#主线程，也就是程序一开始运行的时候，最初的那个线程
#子线程，通过thread类实例化的线程，都是子线程
#主线程等待子线程，执行结束后，主线程再去做别的操作
#