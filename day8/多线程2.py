import threading
import requests
import hashlib

def down_load(url):
    name = hashlib.md5(url.encode()).hexdigest()
    r = requests.get(url)
    with open('%s.jpg'%name,'wb') as fw:
        fw.write(r.content)

case_result = []

def run_case(case_name):
    print('run case..',case_name)
    case_result.append(  {case_name:'success'})
#多线程运行函数时，函数的返回值是拿不到的，所以定义一个list
#把函数运行的结果都存进去就ok了

l = [
    'http://www.nnzhp.cn/wp-content/themes/QQ/images/logo.jpg',
    'http://www.nnzhp.cn/wp-content/uploads/2016/12/2016aj5kn45fjk5-150x150.jpg',
    'http://www.nnzhp.cn/wp-content/themes/QQ/images/thumbnail.png'
]

for i in l:
    t = threading.Thread(target=down_load,args=[i])
    t.start()

while threading.activeCount()!=1:
    pass

# print('down load over...')