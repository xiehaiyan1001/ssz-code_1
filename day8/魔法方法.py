#一个类里面有一些特殊作用的方法，不需要你调用，在一些情况下会自动运行
# 类里面 __funname__ 都是魔法方法，有一些特殊作用
class Lxy:
    def __init__(self): #实例化的时候自动执行
        self.name = 'lxy'
        self.sex = 'nv'

    def __getattr__(self, item):
        return self.item

# l = Lxy()
# print(l.name)
# print(l.sex)

# print(l['name'])
# print(l.get('name'))


class DictToObject(dict):
    '''字典转对象'''
    def __getattr__(self, item): #xx.xx的会自动调用它
        value = self[item]
        if type(value) == dict:
            value = DictToObject(value)
        elif type(value) == list or isinstance(value,tuple):
            for index,item in enumerate(value):
                if type(item) == dict:
                    dto = DictToObject(item)
                    value[index] = dto
        return value

d1 = {'a':1,'name':'xiaohei','money':{'sarlay':500},
      'car':[ {'name':'benz'} ,{'name':'bmw'},12,3,4  ]
      }
d = DictToObject(d1)
print(d.a)
print(d.name)
print(type(d.money))
print(d.money.sarlay)
print(d.car[0].name)