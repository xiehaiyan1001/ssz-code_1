大师兄博客：http://www.imdsx.cn

目标：自动化平台的开发

后台—》牛牛 django

前台—》html css js  vue element-ui

### 前端

前端的三把利器

把前端比作一个赤裸的人。

#### 一、HTML（将这个人完成呈现出来）

Python解释器：编写完一个.py结尾的文件，python xxx.py

Html解释器：浏览器

~~~html
一对标签
<h1></h1>
~~~

HTML学什么？

学的是浏览器能够识别的标签规则。

#### 1、html基础

##### 1.1、单个html文件只允许出现一对html标签。

##### 1.2、自闭和标签和主动闭合标签

~~~html
<!--    1、自闭和标签  <meta charset="UTF-8">-->
<!--    2、主动闭合标签 <title>射手座</title>-->
~~~

##### 1.3、块级标签和行内标签

块级标签特点：无论自己内容有多大 都占一整行。

行内标签特点：自己内容有多大就占多大。（无法应用宽、高、外边距、内边距)

##### 1.4、白板标签

div和span没有被css修饰，所以称作白板标签

#### 二、CSS（修饰，让这个人更加完美、好看）

有几种方式去写css样式

##### 2.1、通过link标签导入css样式表

~~~html
<link rel="stylesheet" href="cssDemo.css">
~~~

##### 2.2、在head标签中，增加style标签，在style标签中可以添加css样式

~~~html
    <style>
        #i1{
            width: 100px;
            height: 100px;
            border: 1px blue solid;
        }
    </style>
~~~

##### 2.3、在标签上，增加style属性。

~~~html
<div style="width: 100px;height: 100px;border: 1px red solid"></div>
~~~

##### 2.4、选择器

###### id选择器

~~~html
代码约束 不允许一对html标签中存在多个相同的id属性
<div id="i1"></div>
~~~

###### Class选择器

允许多次出现，也就是允许复用

~~~html
<div class="c1"></div>
~~~

应用多个样式

~~~html
<div class="c1 c2" ></div>
~~~

###### 标签选择器

~~~html
标签选择器，全局设置，所有标签都应用这个样式。
<div></div>
~~~

###### 属性选择器

~~~html
div[name="ssz"]{
  width: 100px;
  height: 100px;
  background-color: green;
}
<div name="ssz"></div>
~~~

##### 2.5、css优先级

style属性是最高优先级

以标签为基准，由近到远，由下到上。

##### 2.6、基础css样式

###### 居中

~~~html
/*水平居中*/
text-align: center;
/*垂直居中*/
line-height: 100px;
~~~



#### 三、JS（让这个人动起来。）

DOM

innerText 获取标签间的字符串。无论innerText赋值是普通字符串，还是具有标签的字符串，都会变成字符串放在div中间。

innerHtml 获取标签间的所有的内容。如果是特殊含义的字符串，如：<input> 那么就会变成一个元素。

##### 3.1、创建标签

字符串创建

~~~javascript
var demo = document.getElementById('create1')
demo.insertAdjacentHTML('afterEnd','<span>xxx</span>')
~~~

标签对象创建

~~~javascript
var input = document.createElement('input')
var create = document.getElementById('create')
create.appendChild(input)
~~~

##### 3.2、indexof实现和python的 in操作

indexOf 在获取到字符时，返回其角标，没有获取到则返回-1

通过判断-1可以判断 某个字符是否在字符串内。

##### 3.3

console.log()  ==  print()

alert() js的弹框

#### 四、Vue（vue的核心思想是只要改变数据，页面就发生变化）

##### 4.0、vue实例

vue实例关注：

el： vue接管的div元素  注：只能接管一个div，所有需要vue处理，必须写在这个div内。

data：数据

methods：方法

。。。



##### 4.1、v-show 和 v-if区别（判断）

例：接口测试平台，成功 失败，根据后台返回的结果状态。显示对应的文案

v-show 如果条件为False 则对标签增加 display=none

v-if 只有符合条件的元素 才加载

##### 4.2、v-on:click  （绑定事件）

全写 v-on:click

简写 @click

click对应的方法是，methods的方法。

##### 4.3、v-bind（标签属性绑定）

全写 v-bind:href

简写 :href

可以使用data中的数据

class的样式绑定

~~~vue
<head>
  <style>
    .bg{}
    .box{}
  </style>
</head>	

<div :class="['bg','box']"></div>
<div :class="{bg:true}"></div>   // bg 代表样式表    true则使用样式表，false 不使用
<div :class="{bg:is_bg}"></div> // 引用了vue.data.is_bg

new Vue({
	data:{
	is_bg:true
}
})

~~~

##### 4.4、v-model（双向数据绑定）

a)、input

b)、radio

c)、checkbox  —>   value 配置成  []  用于存储选择的多个数据

d)、select —> value 配置成 str 

通过修改标签 例：切换radio、checkbox...都会对data中绑定的数据有影响。

通过事件触发方法，修改data中数据，反向作用域radio、checkbox.....

##### 4.5、v-for（循环）

v-for 支持 list、map

要循环那个标签，就将v-for 写在那个标签上

~~~vue
list
<div v-for="game in games">
  {{game}}
</div>


<div v-for="(game,index) in games">
  {{game}}
</div>

map
<div v-for="value in games">
  {{value}}
</div>


<div v-for="(value,key) in games">
  {{value}}
</div>


<div>{{movie}}</div>
<select v-model="movie">
  <option disabled value="">请选择</option>
  <option v-for="option in options" :value="option.id">{{option.name}}</option>
</select>

options:[{"name":"变形金刚","id":1}, {"name":"复仇者联盟","id":2}, {"name":"飞驰人生","id":3}]

~~~

##### 4.5、过滤器

filter：

定义filter

~~~app
new Vue({
	el:"#app",
	filters:{
		func:function(){
		
		}
	}
})
~~~



~~~vue
默认将|左侧count传递给右侧方法
{{count|filterFunc}}

可以传多个参数
{{count|filterFunc("test")}}

func:function(value,test){

}
~~~



##### 4.6、vue的生命周期

```
mounted-->常用钩子-->完成初始化数据
```

##### 4.7、$refs

a)、标签

~~~vue
<div ref="test"></div>

this.$refs.test // 获取到标签  可以理解为 document.getElementbyID 获取的是标签对象
~~~

b)、组件

~~~vue
<test ref="a"></test>

this.$refs.a.xxxx // 可以获取组件中的data声明的数据
~~~

c)、组件化时，如果一个元素是由多个标签组成,内部标签，想组件化不可以直接使用组件，需要用is属性指定组件。

~~~vue
    <table border="1">
        <thead>
            <th>id</th>
            <th>method</th>
            <th>状态</th>
        </thead>
        <tbody>
<!--            <tr-demo></tr-demo>-->
            <tr is="tr-demo"></tr>
        </tbody>
    </table>


    Vue.component('tr-demo',{
        template:'<tr><td>1</td><td>post</td><td>失败</td></tr>'
    })
    new Vue({
        el:'#app'
    })
~~~

~~~
<selelct>
	<option></option>
</select>

<table></table>
~~~

##### 4.8、组件（复用代码，抽象共用代码）

全局组件

~~~vue
Vue.component('组件名',{
	template:'',
	// 组件可以被多个地方调用，所以通过方法进行数据隔离
	data:fucn(){
		return {
		
		}
	},
	methods:{}
})
~~~

局部组件

~~~vue
        // 局部组件
        components:{
            "demo-key":{
                template: "<h1 @click='change'>{{msg}}</h1>",
                data:function () {
                    return{
                        msg:0
                    }
                },
                methods:{
                    change:function () {
                        this.msg ++
                    }
                }
            },
            "h2-demo":{
                
            }
        }
~~~

##### 4.9、父子组件的交互







##### 5.0、element-ui  开发自己的接口自动化平台（基于vue）

前端的开发（element ui+vue）



##### 5.1、axios发送请求

默认response返回带有：

~~~
{data: {…}, status: 200, statusText: "OK", headers: {…}, config: {…}, …}
~~~

data代表真实接口返回的数据

获取接口发回的数据要以response.data











#### 五、node环境安装

##### 1、安装node：http://nodejs.cn/download/

验证node是否安装成功

~~~linux
(base) houyafandeMacBook-Pro:~ houyafan$ node -v
v8.9.1
(base) houyafandeMacBook-Pro:~ houyafan$
~~~

##### 2、安装vue

~~~
npm install vue
~~~

##### 3、安装vue-cli

~~~
npm install -g @vue/cli
~~~

验证vue-cli（vue --version）

~~~
(base) houyafandeMacBook-Pro:~ houyafan$ vue --version
@vue/cli 4.3.1
(base) houyafandeMacBook-Pro:~ houyafan$
~~~

##### 4、下载百度网盘中vue项目代码

链接:https://pan.baidu.com/s/1-IKCp7LobPR42opxfnV8Og  密码:zv83

##### 5、解压运行文件夹中mock.py文件

报错自己解决python报错

~~~python
 * Running on http://127.0.0.1:8000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 334-839-330
~~~

##### 6、在项目根目录运行

~~~vue
npm run dev

App running at:
- Local:   http://localhost:9527/ 
- Network: http://172.17.14.2:9527/

Note that the development build is not optimized.
To create a production build, run npm run build.
~~~

##### 7、访问http://localhost:9527

用浏览器访问地址，点击登录，登录成功。并跳转主页

##### 8、接口文档地址

https://www.showdoc.cc/xinghan?page_id=2778284852374602

#### 六、用例集合

1、嵌套的 Dialog

2、Transfer 穿梭框

allCase： 代表穿梭框左侧待选用例   [{},{}]

joinCaseValue: 代表穿梭框右侧已选择用例  []

~~~
<el-transfer v-model="joinCaseValue" :data="allCase" :props="{key:'id',label:'title'}"></el-transfer>

:props="{key:'id',label:'title'}"  绑定 props 进行key label的映射
~~~

##### 选择用例保存

集合中用例在保存时，需要传递 当前所属的集合id。

##### 集合运行

向后台传递集合id，[1,2,3]

七、VUE框架

vue-admin-element https://panjiachen.github.io/vue-element-admin-site/zh/

基于vue的生态做的很好，提供的很多的文档，中文。并且有视频。

vue 

element  ==  element ui

vue-cli实现（脚手架）

内置了 i18 国际化解决方案 提供了中文

安装：

```bash
# 克隆项目
git clone https://github.com/PanJiaChen/vue-element-admin.git

# 进入项目目录
cd vue-element-admin

# 安装依赖
npm install  # 翻墙安装  npm  ==  linux 中  yum  包管理工具 npm时候node的包管理工具
# node_modules

# 建议不要用 cnpm 安装 会有各种诡异的bug 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 需要启动mock服务
# 运行 mock
# python mock.py

# 本地开发 启动项目
npm run dev # 项目的根目录
```

框架中重点关注

```bash
├── src                        # 源代码
│   ├── api                    # 所有请求   *
│   ├── assets                 # 主题 字体等静态资源
│   ├── components             # 全局公用组件  *
│   ├── directive              # 全局指令
│   ├── filters                # 全局 filter
│   ├── icons                  # 项目所有 svg icons
│   ├── lang                   # 国际化 language
│   ├── layout                 # 全局 layout
│   ├── router                 # 路由   *
│   ├── store                  # 全局 store管理
│   ├── styles                 # 全局样式
│   ├── utils                  # 全局公用方法
│   ├── vendor                 # 公用vendor
│   ├── views                  # views 所有页面  *
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
│   └── permission.js          # 权限管理
```

#### 目标：

parameter.html  迁移到 vue-admin框架

parameter.html 就是基于elementui实现的  vue-element-admin 框架也是基于  vue和element-ui实现的

前端 3 大块

js、css、html

#### 导入模块、导出模块

~~~
第一种
export { parameterRouter } // 导出模块
import { parameterRouter } from './modules/parameter'

第二种
  export default {     // 只能导出一个
    name: 'parameter'
  }
  import chartsRouter from './modules/charts'   //  不带大括号的都是 和 export default结合使用的
~~~

#### 代码爆红

代码爆红是由于框架增加了代码格式验证

需要修改根目录下 .eslintrc.js 中 rules:{}

#### 新建页面

##### 1、配置路由

例：src/router/parameter.js

##### 2、在Views目录创建 对应业务的文件夹，文件夹下有默认的一个index.vue  parameter.html 

例：src/views/parameter目下有个index.vue

##### 3、创建API，和后台请求的接口，每开发一个模块，需要在src/api目录下创建一个针对这个模块的API接口

例：src/api/parameter.js

##### 4、src/utils/requests.js 实际是对axios进行封装，最后暴露一个模块

#### 组佳化业务功能

例:将参数管理中，搜索功能变成组件化

##### 1、在src/views/parameter/下创建components目录，创建组件Bar

##### 2、在index.vue中导入创建的组件

~~~
import Bar from './components/Bar'
~~~

##### 3、注册组件

~~~
components: {
	Bar   // es6的简写
	Bar:Bar // 全写，如果 k v 相同，则可以只写Bar
},
~~~

##### 4、子组件和父组件交互

页面点击查询按钮—>触发子组件search方法—>子组件向父组件通过$emit("innerSearch","value")向父组件触发事件—>父组件触发outerSerch方法—>outerSearch方法中接收子组件传递的参数

谁调用了组件，那么引入的组件就是它的子组件

~~~
子组件search方法
search(){
        // 子组件内，像父组件触发事件
  this.$emit('innerSearch',this.filter_query.search ? this.filter_query.search : undefined)
  // 三元运算 
      },
父组件中子组件的配置
<Bar @innerSearch="outerSearch" ></Bar>

调用父组件outerSearch
outerSearch: function (val) {
  console.log(val,'val')
  this.filter_query.page = 1;
  this.get_page_data()
},
~~~

#### 5、如何找组件

通过页面，选定要找组件的菜单，看一下href的路由

拷贝路由最后一级，全局搜索找path

~~~
path: 'mix-chart',
component: () => import('@/views/charts/mix-chart')
~~~

按照路由配置的组件，看它是怎么写的。

~~~
import Chart from '@/components/Charts/MixChart'
~~~

#### 6、如何用组件

在views中创建我们自己的模块，将找到组件copy到自己模块下的components目录中（目的是不影响其他组件）

index.vue内容copy菜单对应因纳入的组件

例：如下

~~~
path: 'mix-chart',
component: () => import('@/views/charts/mix-chart')
~~~

#### 7、图表

vue-element-admin基于echarts 实现图标，可以访问https://echarts.apache.org/zh/option.html#xAxis看下每个参数的意义。



index.vue 实际是vue的组件格式 跟html不相同。

vue-cli脚手架的搭配的组合。（前端开发工具，能够帮助前端进行快速开发，编译，生成上线文件。）

页面访问必须是parameter.html （浏览器只认html文件）

把写好的index.vue组件，根据编译规则生成 html文件。

~~~bash
npm run build:prod # 编译 线上环境 会在项目根目录生成 dist目录文件下就是要上线的html文件。
~~~

为什么有vue-cli（vue-element-admin框架）格式。

开发快，组件库多。可以提取共用组件。



### 七、Django 基础知识

（前后端不分离）

#### 1、django的filter 和 tag == vue实例里 filter属性

##### django自带过滤器（filter）

可以对服务端返回的数据进一步加工。

使用方式

1.0、在html当中通过{{ navs|length }}进行使用

length是django自带的过滤器，在使用{{ navs|length }}  实际是将navs传递给length 由length返回当前变量长度。

1.1、{{ navs|truncatechars:6 }} 

truncatechars是个方法，navs当做第一个参数传递给truncatechars，6 当做第二个参数传递给truncatechars

注：filter 最多 接收2个参数。

##### 自定义filter

##### 自定Tag（可以接收多个参数）

#### 2、django的分页

2.0、提高用户体验、提高应用的速度

#### 3、django models 多对多、django orm的基础操作（django提供的数据库操作）

#### 4、django froms 验证（前端传递过来的数据）

#### 5、前后单分离接口开发

##### 5.1用django进行接口开发

全局参数（get，post，put，delete）

接口：/api/parameter

GET：获取全局参数的所有数据

POST：创建全局参数

PUT：更新全局参数

DELETE：删除全局参数

##### 5.2FBV的 接口开发方式  CBV 

##### 5.3 Model 学习BaseModel

创建时间、更新时间、是否删除

##### 5.4、框架的优化 restfromwork 

6、注册、登录、权限

6.0、User表的创建

6.1、针对parampter的接口进行登录过滤

思路：要判断是否有token   在那判断？

6.2、对接前端。



#### 7、ReView 所有表结构的设计，体验通过NbView 实现 post put delete get请求。

#### 8、开发其它接口

获取依赖用例接口：

http://127.0.0.1:8000/api/get_rely_case?project_id=2  添加场景 ---》 获取这个项目下的其他用例 

http://127.0.0.1:8000/api/get_rely_case?project_id=2&case_id=3 编辑场景--》 会把当前用例也返回--》通过case_id过滤当前用例



集合下 获取选择用例数据：

http://127.0.0.1:8000/api/join_case?project_id=2&id=2

http://127.0.0.1:8000/api/interface?project=4  根据项目获取当前项目下的接口



#### 9、celery task 异步任务

业务端后台：通过python manage 运行

运行用例：通过python manage 运行 多并发 会卡 影响效率

​					celery task  本身自己也是个服务。异步处理case 

（异步：永波给我买包烟，我去写代码码，永波买完之后给我。 同步：永波给我买包烟，我等着永波回来。）

celery 结构

-- celery_tasks

​	--- run

​		---- tasks # 异步任务的核心代码

​	--- config.py  # 存celery的配置

​	config 存放的是redis 地址

​	--- main.py # celery运行目录



运行用例 --- 调用异步任务 --- 任务写入reids --- 异步任务服务取任务 --- 干活

区分每次调用异步任务的结果： 





#### Day24作业：

用mysql 创建一下服务的数据库  测试下所有功能





#### 1、注册页面 需要开发

#### 2、权限配置页面 需要开发

