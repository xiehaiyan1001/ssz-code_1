from celery_tasks.main import app
# 在celery中使用logger
from celery.utils.log import get_task_logger
from example import models
from example.tools import str_for_dict
import django_redis
import json
from example.response import ResponseCheck
from example.myHttp import MyRequest
from example import const
import re

logger = get_task_logger('sksystem')


class Run:
    def __init__(self):
        self.premise_case_id = []  # 所有依赖的用例
        self.redis = django_redis.get_redis_connection('redis2')

    # 将全局参数的值进行替换
    def data_for_replace(self, data):
        for k, v in data.items():
            keys = re.findall(r'\$\{(.*?)\}', str(v))
            if keys:
                parameter = models.Parameter.objects.filter(name=keys[0], is_delete=1).first()
                if parameter:
                    logger.debug('全局参数: %s=%s' % (keys[0], parameter.value))
                    data[k] = parameter.value
                else:
                    # 如果有要定义的全局变量未找到，则去缓存里取
                    parameter = self.redis.get('cache:%s' % keys[0])
                    if parameter:
                        logger.debug('缓存取参: %s=%s' % (keys[0], parameter))
                        data[k] = parameter
            else:
                logger.debug('未在参数中找到含有{}的参数，无需替换。参数为[ %s ]' % v)
        return data

    def set_cache_field(self, cache_field, response, response_obj):
        '''
        :param cache_field:
        :param response:
        :param response_obj:
        :return:
        '''
        for item in cache_field.split(','):
            item = item.replace(' ', '')
            value = response_obj.get_response_value(item, response)
            logger.debug('缓存: ' + item + '=' + str(value))
            self.redis.set('cache:' + item, value, const.EXIT_TIME)

    def run(self, case_id, user_id, task_id, collection_id=None):
        # 获取到用例信息 组装用例信息
        case_obj = models.Case.objects.filter(id=case_id).first()
        # 获取项目相关信息
        host = case_obj.project.host

        # 获取接口相关信息，如默认参数、默认header
        uri = case_obj.interface.uri
        # 默认参数
        default_params_obj = str_for_dict(case_obj.interface.params) if case_obj.interface.params else {}
        # 默认header
        default_headers_obj = str_for_dict(case_obj.interface.headers) if case_obj.interface.headers else {}
        # 用例的参数
        params_obj = str_for_dict(case_obj.params) if case_obj.params else {}
        # 用例的header
        headers_obj = str_for_dict(case_obj.headers) if case_obj.headers else {}

        # 获取用例相关信息
        # get_method_display 获取choice的枚举
        method = case_obj.get_method_display()
        is_json = case_obj.is_json
        if is_json:
            json_obj = str_for_dict(case_obj.json)
            # 默认参数被用例参数覆盖
            default_params_obj.update(json_obj)
            data = self.data_for_replace(default_params_obj)
        else:
            # 默认参数被用例参数覆盖
            default_params_obj.update(params_obj)
            data = self.data_for_replace(default_params_obj)
        # 最终的headers
        headers_obj.update(default_headers_obj)
        # 替换header
        headers_obj = self.data_for_replace(headers_obj)

        # 实例化http，并调用对应方法
        logger.debug('url: %s' % host + uri)
        logger.debug('method: %s' % method)
        logger.debug('data: %s' % data)
        logger.debug('headers: %s' % headers_obj)
        response = MyRequest(url=host + uri, method=method, data=data, is_json=is_json, headers=headers_obj).results
        logger.debug('response: %s' % response)
        # 获取校验和缓存的相关数据
        check = case_obj.check
        check_obj = ResponseCheck(check, response)
        status = check_obj.status  # 校验状态
        reason = check_obj.reason  # 原因
        logger.debug('验证结果: %s' % reason)
        # 如果用例的运行成功，才会去取要缓存的字段
        if status == 1:
            if case_obj.cache_field:
                cache_field = case_obj.cache_field
                self.set_cache_field(cache_field, response, check_obj)

        create_data = {
            'response': str(response),
            'case': case_obj,
            'batch': task_id,
            'status': status,
            'duration': 100,
            'user': models.User.objects.get(id=user_id),
            'case_collection': models.CaseCollection.objects.get(id=collection_id) if collection_id else None,
            'reason': reason,
            'params': json.dumps(data),
            'url': host + uri,
            'title': case_obj.title,
            'project': case_obj.project,
        }
        # 记录到report表中
        models.Report.objects.create(**create_data)
        if not collection_id:
            models.Case.objects.filter(id=case_id).update(status=status)

    def get_premise_case_id(self, case_id):
        premise_cases = models.Case.objects.filter(id=case_id).first().case.all()
        return premise_cases

    def loop_premise_case(self, case_id):
        premise_cases = self.get_premise_case_id(case_id)
        for case in premise_cases:
            self.premise_case_id.insert(0, case.premise_case.id)  # 很关键的一点 ，取依赖时在这里控制依赖用例的执行顺序 越后查出来的越往前放。
            if self.get_premise_case_id(case.premise_case.id):
                self.loop_premise_case(case.premise_case.id)
        return self.premise_case_id

    # 去掉集合用例中可能存在和依赖用例重复的用例。
    def set_case_get_premise(self, case_ids):
        # 集合中所有要运行的用例
        for case_id in case_ids:
            logger.debug('当前用例: %s' % case_id)
            to_be_run = self.loop_premise_case(case_id)  # 递归获取依赖用例
            logger.debug('依赖用例: %s' % to_be_run)
            to_be_run.append(case_id)  # 追加当前用例
            logger.debug('追加当前用例: %s' % to_be_run)
        all_run_case_list = list(set(to_be_run))  # 去重后的列表
        all_run_case_list.sort(key=to_be_run.index)  # 按照原有排序在排回来
        logger.debug('集合中用例去重后，所有要运行的用例: %s' % all_run_case_list)
        return all_run_case_list


# 通过task修饰 这个函数是异步任务
@app.task(name='run_case')
def run_case_task(case_id, user_id):
    run_obj = Run()  # 每次运行实例化运行类，重新初始化依赖用例
    # 获取本次任务的task_id
    task_id = run_case_task.request.id
    # 排除重复用例，并获取依赖用例
    all_case = run_obj.set_case_get_premise([case_id])
    for run_id in all_case:
        run_obj.run(run_id, user_id, task_id)


@app.task(name='run_collection')
def run_collection_task(collection_id, user_id):
    run_obj = Run()  # 每次运行实例化运行类，重新初始化依赖用例
    case_id_obj = models.CaseCollection.objects.filter(id=collection_id).first().case.all()
    task_id = run_collection_task.request.id
    if case_id_obj:
        collection_case_id = []
        # 集合中的用例
        for case in case_id_obj:
            collection_case_id.append(case.id)
        all_case = run_obj.set_case_get_premise(collection_case_id)
        for case_id in all_case:
            run_obj.run(case_id, user_id=user_id, task_id=task_id, collection_id=collection_id)
            logger.info('集合 [%s] 运行用例 [%s]' % (collection_id, case_id))
        models.CaseCollection.objects.filter(id=collection_id).update(status=4)


class RunDemo:
    def __init__(self):
        self.redis = django_redis.get_redis_connection('redis2')
        self.premise_case_id = []

    def set_cache(self, cache_field, response, response_obj):
        '''

        :param cache_field: token,code
        :return:
        # 设置缓存 需要链接reids
        '''
        fields = cache_field.split(',')
        for field in fields:
            # 基于返回值 获取某个字段的value
            value = response_obj.get_response_value(field, response)
            # 设计reids数据
            logger.info('缓存: ' + field + '=' + str(value))
            self.redis.set('cache:' + field, value, const.EXIT_TIME)

    def data_replace(self, data):
        '''

        :param data: 参数或header
        :return:
        1、判断出参数中是否包含  ${}   re  正则表达式 keys = re.findall(r'\$\{(.*?)\}', str(v))
        2、存在则则替换
        3、顺序  先全局参数 在缓存 没有则不替换
        '''
        for k, v in data.items():
            keys = re.findall(r'\$\{(.*?)\}', str(v))
            logger.info('需要替换的字段：%s' % keys)
            if keys:
                parameter = models.Parameter.objects.filter(name=keys[0], is_delete=0).first()
                # 获取到参数后，复制
                if parameter:
                    data[k] = parameter.value
                else:
                    # 如果有要定义的全局变量未找到，则去缓存里取
                    parameter = self.redis.get('cache:%s' % keys[0])
                    if parameter:
                        logger.debug('缓存取参: %s=%s' % (keys[0], parameter))
                        data[k] = parameter

        logger.info('替换后的参数: %s' % data)
        return data

    def run(self, case_id, user_id, task_id, collection_id=None):
        '''

        :param case_id: 单个用例id
        :param user_id: 用例执行用户
        :param task_id: 批次号
        :param collection_id: 集合id 默认可以不传递
        :return: 无

        1、获取用例的相关信息
        2、拼出请求接口
        3、得到最新的请求参数、请求header
        4、发起请求
        5、获取请求结果，如果结果成功，是否配置缓存
        6、生成报告

        '''
        case_obj = models.Case.objects.get(id=case_id)
        # host
        host = case_obj.project.host
        # uri
        uri = case_obj.interface.uri

        # 接口参数
        interface_params = str_for_dict(case_obj.interface.params) if case_obj.interface.params else {}
        interface_headers = str_for_dict(case_obj.interface.headers) if case_obj.interface.headers else {}

        # 用例的
        case_params = str_for_dict(case_obj.params) if case_obj.params else {}
        case_headers = str_for_dict(case_obj.headers) if case_obj.headers else {}

        # 请求方式
        method = case_obj.get_method_display()

        # 更新header 以用例上的为例
        interface_headers.update(case_headers)
        headers = self.data_replace(interface_headers)

        # 更新参数 需要判断是否是json类型
        is_json = case_obj.is_json
        if is_json:
            # 如果是json 那么参数的覆盖就用jsondata
            json_data = str_for_dict(case_obj.json) if case_obj.json else {}
            interface_params.update(json_data)
            data = self.data_replace(interface_params)
        else:
            # params
            logger.info('更新前 case_params：%s' % case_params)
            interface_params.update(case_params)
            # 进行参数替换
            data = self.data_replace(interface_params)

        logger.info('更新后 interface_params:%s' % interface_params)
        logger.info('更新后 interface_headers :%s' % interface_headers)
        logger.info('method :%s' % method)
        logger.info('host :%s' % host)
        logger.info('uri :%s' % uri)

        # 发起请求
        response = MyRequest(url=host + uri, method=method, data=data, is_json=is_json, headers=headers).results
        logger.info('返回值结果：%s' % response)

        # 验证结果check
        check = case_obj.check
        check_obj = ResponseCheck(check, response)
        status = check_obj.status  # 校验状态
        reason = check_obj.reason  # 原因
        logger.info("校验的状态：%s" % status)
        logger.info("原因：%s" % reason)
        if status == 1:  # 为1 代表成功了
            # 成功了要做缓存字段
            cache_field = case_obj.cache_field
            # 判断是否有需要缓存的字段
            if cache_field:
                self.set_cache(cache_field, response, check_obj)
        create_data = {
            'response': str(response),
            'case': case_obj,
            'batch': task_id,
            'status': status,
            'duration': 100,  # 耗时
            'user': models.User.objects.get(id=user_id),
            'case_collection': models.CaseCollection.objects.get(id=collection_id) if collection_id else None,
            'reason': reason,
            'params': json.dumps(data),
            'url': host + uri,
            'title': case_obj.title,
            'project': case_obj.project,
        }
        # 记录到report表中
        models.Report.objects.create(**create_data)
        logger.info('报告创建成功')
        # 用例运行的情况下 需要变更case 状态
        if not collection_id:
            models.Case.objects.filter(id=case_id).update(status=status)

    def get_premise_case_id(self, case_id):
        premise_cases = models.Case.objects.filter(id=case_id).first().case.all()
        return premise_cases

    def loop_premise_case(self, case_id):
        premise_cases = self.get_premise_case_id(case_id)
        for case in premise_cases:
            self.premise_case_id.insert(0, case.premise_case.id)  # 很关键的一点 ，取依赖时在这里控制依赖用例的执行顺序 越后查出来的越往前放。
            if self.get_premise_case_id(case.premise_case.id):
                self.loop_premise_case(case.premise_case.id)
        return self.premise_case_id

    # 去掉集合用例中可能存在和依赖用例重复的用例。
    def set_case_get_premise(self, case_ids):
        # 集合中所有要运行的用例
        for case_id in case_ids:
            logger.debug('当前用例: %s' % case_id)
            to_be_run = self.loop_premise_case(case_id)  # 递归获取依赖用例
            logger.debug('依赖用例: %s' % to_be_run)
            to_be_run.append(case_id)  # 尾部追加当前用例
            logger.debug('追加当前用例: %s' % to_be_run)
        all_run_case_list = list(set(to_be_run))  # 去重后的列表
        all_run_case_list.sort(key=to_be_run.index)  # 按照原有排序在排回来
        logger.debug('集合中用例去重后，所有要运行的用例: %s' % all_run_case_list)
        return all_run_case_list


# 通过装饰器 app.task 实现将普通函数变为celery的执行函数
# name 给方法命名
@app.task(name='demo')
def demo(case_id, user_id):
    logger.info('case_id: %s' % case_id)
    logger.info('user_id: %s' % user_id)
    task_id = demo.request.id
    logger.info('task_id: %s' % task_id)
    import time
    time.sleep(2)
    # 实现运行的功能
    run_obj = RunDemo()
    # 调用run之前  将所有要运行的用例 准备好
    all_case = run_obj.set_case_get_premise([case_id])
    # 一条用例运行
    for case in all_case:
        run_obj.run(case, user_id, task_id)
    models.Case.objects.filter(id=case_id).update(status=1)


@app.task(name='demo_run_collection_task')
def demo_run_collection_task(collect_id, user_id):
    '''

    :param collect_id: 集合id 例： 1
    :param user_id: 用户
    :return:
    1、获取当前集合下所有的用例
    2、获取当前用例下所有的依赖用例
    3、调用实现好的获取依赖的方法 去重，最终得到 待执行的用例列表
    '''
    logger.info("collect_id %s" % collect_id)
    logger.info("user_id %s" % user_id)
    run_obj = RunDemo()  # 每次运行实例化运行类，重新初始化依赖用例
    # 多对多获取 集合下的用例
    case_id_obj = models.CaseCollection.objects.filter(id=collect_id).first().case.all()
    # 当前执行任务的id
    task_id = demo_run_collection_task.request.id
    if case_id_obj:
        #  获取到了 集合下的所有用例
        collection_case_id = []
        # 集合中的用例
        for case in case_id_obj:
            collection_case_id.append(case.id)
        # 获取每个用例的依赖用例
        all_case = run_obj.set_case_get_premise(collection_case_id)
        for case_id in all_case:
            run_obj.run(case_id, user_id=user_id, task_id=task_id, collection_id=collect_id)
            logger.info('集合 [%s] 运行用例 [%s]' % (collect_id, case_id))
        models.CaseCollection.objects.filter(id=collect_id).update(status=4)
