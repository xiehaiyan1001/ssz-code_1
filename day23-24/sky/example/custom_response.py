import datetime
import json

from django.http import JsonResponse


class NbJSONEncoder(json.JSONEncoder):
    '''解析json里面日期格式'''

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, datetime.date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)


class NbResponse(JsonResponse):
    def __init__(self, code=0, msg='操作成功', **kwargs):
        data = {"code": code, "msg": msg}
        data.update(kwargs)
        super().__init__(data=data, json_dumps_params={"ensure_ascii": False}, encoder=NbJSONEncoder)
