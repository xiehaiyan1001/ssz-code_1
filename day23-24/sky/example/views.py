import datetime
import json
import time
from itertools import chain
import django_redis
from django.core.paginator import Paginator
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views import View
import pickle
from .forms import ParameterForm
from . import forms
from . import models
from . import tools
from example.custom_view import NbView, PostView, BaseView, GetView
from example.custom_response import NbResponse
from . import const
from . import case_utils
from celery_tasks.run.tasks import run_case_task, run_collection_task
from celery_tasks.run.tasks import demo,demo_run_collection_task


# from django.forms.models import model_to_dict

def model_to_dict(instance, fields=None, exclude=None):
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if isinstance(value, datetime.datetime):
            value = value.strftime('%Y-%m-%d %H:%M:%S')
        if isinstance(value, datetime.date):
            value = value.strftime('%Y-%m-%d')
        data[f.name] = value
    return data


# FBV func base view
# 用函数实现的 view 叫做FBV
def f_parameter(requests):
    if requests.method == 'GET':
        # get请求时 获取传递过来的第几页数据
        page = requests.GET.get('page')
        qs = models.Parameter.objects.filter(is_delete=0)
        page_obj = Paginator(qs, 5)  # page_obj Paginator实例对象
        # 获取第几页的数据
        page_data = page_obj.get_page(page)
        # 用于存储返回的dict类型数据
        data_list = []
        for data in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(data, exclude=['is_delete'])
            data_list.append(tmp_data)
        return JsonResponse({"code": 0, "data": data_list})
    elif requests.method == 'POST':
        # 通过from进行数据验证
        form_obj = ParameterForm(requests.POST)
        # 数据验证是否通过
        if form_obj.is_valid():
            # 创建数据
            # form_obj.cleaned_data 是所有验证通过的数据
            models.Parameter.objects.create(**form_obj.cleaned_data)
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            # 返回错误信息
            return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
    elif requests.method == 'PUT':
        # django 并没有处理PUT的数据 requests.PUT
        # 实际put传过来的数据在requests.body
        # 需要导入 from django.http import QueryDict  来处处理数据 QueryDict(requests.body)
        # print(requests.PUT)
        # print('requests.body',QueryDict(requests.body))
        # PUT 传过来的数据
        # 更新数据，需要告知，要更新那条数据
        put_data = QueryDict(requests.body)
        print('requests.body', QueryDict(requests.body))
        # 获取要更新那条数据的主键id
        p_id = put_data.get('id')
        # 通过id 从数据库中取这条数据  obj
        data_obj = models.Parameter.objects.get(id=p_id)
        # 参数1 是前端传过来的数据， 参数2 是数据库中获取的数据
        form_obj = ParameterForm(put_data, instance=data_obj)
        if form_obj.is_valid():
            # 通过form的save方法进行数据更新
            form_obj.save()
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
    elif requests.method == 'DELETE':
        #  删除时，要知道 删除的是那条数据 获取主键id
        p_id = requests.GET.get('id')
        # 删除分为2种，逻辑删除、物理删除
        # 逻辑删除： 只是通过改变 某个字段的状态，来控制是否删除
        # 比如：is_delete 数据很重要未来有通过状态在改变回来的需要
        # 物理删除： 直接将数据删掉 delete掉 数据不是很重要。
        # 逻辑删除例子
        # models.Parameter.objects.filter(id=p_id).update(is_delete=1) # 这种更新方式不会触发 updatetime
        # obj = models.Parameter.objects.filter(id=p_id).first()
        # obj.is_delete = 1
        # obj.save()

        # 物理删除
        models.Parameter.objects.filter(id=p_id).delete()
        return JsonResponse({"msg": "delete"})
    else:
        return HttpResponse("errors")


# CBV class Base view
# 面向对象的语言  通过class  可以 用到继承 多继承 面向对像
# cbv 的视图
# class Parameter(View):
#     def get(self, requests):
#         # get请求时 获取传递过来的第几页数据
#         page = requests.GET.get('page')
#         qs = models.Parameter.objects.filter(is_delete=0)
#         page_obj = Paginator(qs, 5)  # page_obj Paginator实例对象
#         # 获取第几页的数据
#         page_data = page_obj.get_page(page)
#         # 用于存储返回的dict类型数据
#         data_list = []
#         for data in page_data:
#             # 通过model_to_dict 转换成dict
#             # fields  指定返回那些字段
#             # exclude 过滤那些字段
#             # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
#             tmp_data = model_to_dict(data, exclude=['is_delete'])
#             data_list.append(tmp_data)
#         return JsonResponse({"code": 0, "data": data_list})
#
#     def post(self, requests):
#         # 通过from进行数据验证
#         form_obj = ParameterForm(requests.POST)
#         # 数据验证是否通过
#         if form_obj.is_valid():
#             # 创建数据
#             # form_obj.cleaned_data 是所有验证通过的数据
#             models.Parameter.objects.create(**form_obj.cleaned_data)
#             return JsonResponse({"code": 200, "msg": "成功"})
#         else:
#             # 返回错误信息
#             return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
#
#     def put(self, requests):
#         # django 并没有处理PUT的数据 requests.PUT
#         # 实际put传过来的数据在requests.body
#         # 需要导入 from django.http import QueryDict  来处处理数据 QueryDict(requests.body)
#         # print(requests.PUT)
#         # print('requests.body',QueryDict(requests.body))
#         # PUT 传过来的数据
#         # 更新数据，需要告知，要更新那条数据
#         put_data = QueryDict(requests.body)
#         print('requests.body', QueryDict(requests.body))
#         # 获取要更新那条数据的主键id
#         p_id = put_data.get('id')
#         # 通过id 从数据库中取这条数据  obj
#         data_obj = models.Parameter.objects.get(id=p_id)
#         # 参数1 是前端传过来的数据， 参数2 是数据库中获取的数据
#         form_obj = ParameterForm(put_data, instance=data_obj)
#         if form_obj.is_valid():
#             # 通过form的save方法进行数据更新
#             form_obj.save()
#             return JsonResponse({"code": 200, "msg": "成功"})
#         else:
#             return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
#
#     def delete(self, requests):
#         #  删除时，要知道 删除的是那条数据 获取主键id
#         p_id = requests.GET.get('id')
#         # 删除分为2种，逻辑删除、物理删除
#         # 逻辑删除： 只是通过改变 某个字段的状态，来控制是否删除
#         # 比如：is_delete 数据很重要未来有通过状态在改变回来的需要
#         # 物理删除： 直接将数据删掉 delete掉 数据不是很重要。
#         # 逻辑删除例子
#         # models.Parameter.objects.filter(id=p_id).update(is_delete=1) # 这种更新方式不会触发 updatetime
#         # obj = models.Parameter.objects.filter(id=p_id).first()
#         # obj.is_delete = 1
#         # obj.save()
#
#         # 物理删除
#         models.Parameter.objects.filter(id=p_id).delete()
#         return JsonResponse({"msg": "delete"})


# 简化代码
# 大多数接口 都可以继承getview 来实现get的方法
# 继承 NbView 既有get post put  delete
class SParameter(NbView):
    # 重写了父类的变量
    # 抽象出不同的地方  让相同的代码进行复用
    model_class = models.Parameter
    form_class = ParameterForm
    # fields = ['name','desc','value']
    # exclude_fields = ['name']
    filter_fields = ['value']
    search_fields = ['name']

    '''
        例： 前端传递过来的参数
        phone ： 12311112222
        password：123456
        name：大师兄
    '''


class Register(PostView):
    model_class = models.User
    form_class = forms.RegisterForm

    # 重写 post 方法进行定制化开发
    # 将前端传递的 password 进行md5加密
    def post(self, requests):
        # 通过from进行数据验证
        form_obj = self.form(requests.POST)
        # 数据验证是否通过
        if form_obj.is_valid():
            # 创建数据
            # form_obj.cleaned_data 是所有验证通过的数据
            data = form_obj.cleaned_data
            data['password'] = self.model_class.mack_password(data['password'])
            print('data', data)
            self.model.objects.create(**data)
            return NbResponse()
        else:
            # 返回错误信息
            # return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
            return NbResponse(code=500, msg=form_obj.error_format)


class Login(View):
    def post(self, request):
        '''
        1、验证前端传递的数据是否符合规则，forms
        2、登录成功需要有标示，标示（token）存到  redis
        :param request:
        :return:
        '''
        form_obj = forms.LoginForm(request.POST)
        if form_obj.is_valid():
            # 验证通过的数据 代表登录成功
            # 登录成功需要有标示，标示（token）存到  redis
            form_data = form_obj.cleaned_data
            # token 生成方式  用户名 + 时间戳 + MD5
            username = form_data.get('username')
            tmp_token = '%s%s' % (username, time.time())
            # md5 用 tools提供工具
            token = tools.md5(tmp_token)
            # 存 token的时候  token 应该key
            user = form_obj.cleaned_data['user']
            # 安装 pip install django-redis
            # 获取redis 链接 默认取配置文件中 default
            redis = django_redis.get_redis_connection()
            # 参数1  是key  参数2 是value  参数3 是过期时间 单位秒
            # {token：userobj}
            #  pickle.dumps(user) 序列化数据，转成 二进制
            redis.set(const.SESSION + token, pickle.dumps(user), const.EXIT_TIME)
            return NbResponse(token=token, user=user.name, user_id=user.id)
        else:
            return NbResponse(-1, form_obj.error_format)


class Logout(View):
    def post(self, request):
        redis = django_redis.get_redis_connection()
        redis.delete(const.SESSION + request.token)
        return NbResponse()


class Info(View):
    def get(self, request):
        qs_data = models.Permission.objects.filter(user=request.user)
        roles = []
        for p in qs_data:
            roles.append(p.roles.name)
        data = {
            "roles": roles,
            "avatar": 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            "name": request.user.name,
            "introduction": request.user.phone,
            "user": request.user.id
        }
        return NbResponse(data=data)


class Project(NbView):
    model_class = models.Project  # model
    form_class = forms.ProjectForm
    search_fields = ["name", "desc"]

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        data_list = []
        for instance in page_data:
            tmp_data = model_to_dict(instance, fields=self.fields, exclude=self.exclude_fields)
            tmp_data['user'] = instance.user.name
            data_list.append(tmp_data)
        return NbResponse(data=data_list, count=page_obj.count)


class Interface(NbView):
    search_fields = ['name']
    filter_fields = ['project']
    model_class = models.Interface  # model
    form_class = forms.InterfaceForm

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        # 用于存储返回的dict类型数据
        data_list = []
        for instance in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(instance, fields=self.fields, exclude=self.exclude_fields)
            tmp_data['user'] = instance.user.name
            tmp_data['project_name'] = instance.project.name
            data_list.append(tmp_data)
        # return JsonResponse({"code": 0, "data": data_list})
        return NbResponse(data=data_list, count=page_obj.count)


class Case(NbView):
    model_class = models.Case  # model
    form_class = forms.CaseForm
    search_fields = ['title']
    filter_fields = ['project']

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        # 用于存储返回的dict类型数据
        data_list = []
        for instance in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(instance, fields=self.fields, exclude=self.exclude_fields)
            tmp_data['user'] = instance.user.name
            tmp_data['interface_name'] = instance.interface.uri
            tmp_data['project_name'] = instance.project.name
            tmp_data['rely_case'] = case_utils.get_premise_case(instance)
            data_list.append(tmp_data)
        # return JsonResponse({"code": 0, "data": data_list})
        return NbResponse(data=data_list, count=page_obj.count)

    # 添加
    def post(self, request):
        # 通过from进行数据验证
        form_obj = self.form(request.POST)
        # 获取前端传递过来 当前用例依赖的 id
        rely_case = request.POST.get('rely_case')
        rely_case_obj = json.loads(rely_case)
        # print('rely_case_obj', rely_case_obj)
        # 数据验证是否通过
        if form_obj.is_valid():
            # 创建数据
            # form_obj.cleaned_data 是所有验证通过的数据
            obj = self.model.objects.create(**form_obj.cleaned_data)  # 默认create方法返回的就是当前数据的对象
            # print("obj.id", obj.id)
            # 创建依赖关系 1、先创建当前用例，才会有ID  2、用当前用例id和 传过来的id进行绑定
            for premise_case in rely_case_obj:
                models.CasePremise.objects.create(case_id=obj.id, premise_case_id=premise_case)
            return NbResponse()
        else:
            # 返回错误信息
            # return JsonResponse({"code": "500", "msg": form_obj.errors.get_json_data()})
            return NbResponse(code=500, msg=form_obj.errors.get_json_data())

    def put(self, request):
        p_id = request.PUT.get('id')
        rely_case = request.PUT.get('rely_case')
        # 1、先删除原有依赖关系，在创建
        models.CasePremise.objects.filter(case_id=p_id).delete()
        rely_case_list = json.loads(rely_case)
        # 创建依赖关系
        for rely_case_id in rely_case_list:
            # 2、A 依赖 B  B 依赖 A
            if case_utils.check_premise_case(p_id, rely_case_id):
                models.CasePremise.objects.create(case_id=p_id, premise_case_id=rely_case_id)
            else:
                return NbResponse(code=-1, msg='依赖用例存在问题。请查证')
        data_obj = self.model.objects.get(id=p_id)
        form_obj = self.form(request.PUT, instance=data_obj)
        if form_obj.is_valid():
            form_obj.save()
            return NbResponse()
        else:
            return NbResponse(code=500, msg=form_obj.errors.get_json_data())


class GetRelyCase(View):
    def get(self, request):
        project_id = request.GET.get('project_id')
        case_id = request.GET.get('case_id')
        if case_id:
            # 代表编辑操作
            # 编辑操作 排除 当前用例 在根据项目获取
            data = models.Case.objects.exclude(id=case_id).filter(project_id=project_id, is_delete=0).values('id',
                                                                                                             'title')
        else:
            # 添加操作
            # 获取当前项目下的所有用例
            data = models.Case.objects.filter(project_id=project_id, is_delete=0).values('id', 'title')
        return NbResponse(data=list(data))


class JoinCase(View):
    def get(self, request):
        # joinCase 返回的数据的格式  allcase代表所有用例，joincase代表已经选择的用例
        # data = {
        #     "all_case": [{"id": 1, "title": "test"}, {"id": 2, "title": "test1"}],
        #     "join_case": [1,2]
        # }
        data = {
            "all_case": [],
            "join_case": []
        }
        project_id = request.GET.get('project_id')
        collection_id = request.GET.get('id')
        # all_case 返回当前项目下所有的用例
        qs = models.Case.objects.filter(project_id=project_id, is_delete=0).values('id', 'title')
        data['all_case'] = list(qs)

        collection_qs = models.CaseCollection.objects.get(id=collection_id).case.all().values('id')
        for case in collection_qs:
            data["join_case"].append(case.get('id'))

        return NbResponse(data=data)

    def post(self, request):
        join_case = request.POST.get('join_case_list')
        join_case_list = json.loads(join_case)
        print("join_case_list", join_case_list)
        collection_id = request.POST.get('id')
        collection_obj = models.CaseCollection.objects.get(id=collection_id)
        print("collection_obj", collection_obj)
        collection_obj.case.set(join_case_list)
        return NbResponse()
    # def post(self, requests):
    #     join_case_list = requests.POST.get('join_case_list')
    #     case_ids = json.loads(join_case_list)
    #     collection_id = requests.POST.get('id')
    #     collection_obj = models.CaseCollection.objects.filter(id=collection_id).first()
    #     # 每次绑定前需要先清除之前的绑定关系 否则无法做减少的操作
    #     collection_obj.case.clear()
    #     # 绑定case和集合的关系
    #     for case_id in case_ids:
    #         collection_obj.case.add(models.Case.objects.filter(id=case_id).first())
    #     return NbResponse()


# 集合和用例的关系是多对多的关系
class CaseCollection(NbView):
    model_class = models.CaseCollection  # model
    form_class = forms.CaseCollectionForm
    exclude_fields = ['case']

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        # 用于存储返回的dict类型数据
        data_list = []
        for instance in page_data:
            # 通过model_to_dict 转换成dict
            # fields  指定返回那些字段
            # exclude 过滤那些字段
            # django自带的model_to_dict 是不处理日期 自定义model_to_dict增加了日期的处理
            tmp_data = model_to_dict(instance, fields=self.fields, exclude=self.exclude_fields)
            tmp_data['user'] = instance.user.name
            tmp_data['project_name'] = instance.project.name
            tmp_data['case_count'] = instance.case.all().count()
            data_list.append(tmp_data)
        # return JsonResponse({"code": 0, "data": data_list})
        return NbResponse(data=data_list, count=page_obj.count)


class CaseRun(View):
    # def run(self):
    #     pass
    def post(self, request):
        caseIds = request.POST.get("case_id")
        user_id = request.POST.get("user_id")
        print("case_id", caseIds)
        print("user_id", user_id)
        # 调用异步任务
        for caseId in json.loads(caseIds):
            task_id = demo.delay(caseId, user_id)
            # self.run()
            # 对用例进行状态变更，变成运行中
            models.Case.objects.filter(id=caseId).update(report_batch=task_id, status=3)
        return NbResponse()


class CollectionRun(View):
    def post(self, request):
        collect_id = request.POST.get("collect_id") # [1,2]
        user_id = request.POST.get("user_id")
        for item in tools.str_for_dict(collect_id):
            task_id = demo_run_collection_task.delay(item,user_id)
            models.CaseCollection.objects.filter(id=item).update(report_batch=task_id, status=3)
        return NbResponse()


class CaseReportView(View):
    def get(self,request):
        case_id = request.GET.get('id')
        report_batch = request.GET.get('report_batch')
        case = models.Case.objects.get(id=case_id)
        report = models.Report.objects.filter(batch=report_batch, case_id=case.id).first()
        if report:
            data = {
                'title': case.title,
                'run_time': report.create_time,
                'project_name': case.project.name,
                'status': report.status,
                'case_collection': report.case_collection if report.case_collection else '单用例运行',
                'duration': report.duration,
                'run_user': report.user.name,
                'url': report.url,
                'method': case.get_method_display(),
                'check': case.check,
                'reason': report.reason,
                'params': report.params,
                'response': report.response
            }
            return NbResponse(data=data)
        else:
            return NbResponse(-1, msg='用例运行中，请耐心等待..')

class CollectionReportView(View):
    def get(self,request):
        case_collection_id = request.GET.get('id')
        report_batch = request.GET.get('report_batch')
        print("case_collection_id",case_collection_id)
        print("report_batch",report_batch)
        case_collection = models.CaseCollection.objects.get(id=case_collection_id)
        report = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id).first()
        fail_count = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id,
                                                  status=999).count()
        pass_count = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id,
                                                  status=1).count()
        if report:
            data = {
                'case_collection': case_collection.name,
                'run_time': report.create_time,
                'case_count': case_collection.case.all().count(),
                'pass_count': pass_count,
                'run_user': report.user.name,
                'fail_count': fail_count,
                'duration': 100,
                'report_batch': case_collection.report_batch,
            }
            return NbResponse(data=data)
        else:
            return NbResponse(-1, msg='用例集合运行中，请耐心等待..')


class ReportView(GetView):
    search_fields = ['title']
    filter_fields = ['project', 'case_collection', 'batch']
    model_class = models.Report
    exclude_fields = ['is_delete']

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        # 用于存储返回的dict类型数据
        data_list = []
        for instance in page_data:
            model_dict = model_to_dict(instance, self.fields, self.exclude_fields)  # 转成字典
            model_dict['title'] = instance.case.title
            model_dict['project_name'] = instance.case.project.name
            model_dict['case_collection'] = instance.case_collection.name if instance.case_collection else '单用例运行'
            model_dict['status'] = instance.case.status
            model_dict['run_user'] = instance.user.name
            model_dict['run_time'] = instance.create_time
            model_dict['check'] = instance.case.check
            model_dict['method'] = instance.case.get_method_display()
            data_list.append(model_dict)
        return NbResponse(data=data_list, count=page_obj.count)



class HomeDateView(View):
    def get_source_data(self, day):
        pass_count = models.Report.objects.filter(create_time__contains=day, status=1).count()
        fail_count = models.Report.objects.filter(create_time__contains=day, status=999).count()
        all_count = models.Report.objects.filter(create_time__contains=day).count()
        data = {
            "pass_count": pass_count,
            "fail_count": fail_count,
            "all_count": all_count
        }
        return data

    def get(self, request):
        import datetime
        today = datetime.date.today()
        # 统计数据 当天数据
        data = self.get_source_data(today)
        if models.HomeData.objects.filter(date=today).first():
            models.HomeData.objects.filter(date=today).update(**data)
        else:
            data.update({"date": today})
            models.HomeData.objects.create(**data)
        # 昨天数据

        yesterday = today - datetime.timedelta(days=1)
        data = self.get_source_data(yesterday)
        if models.HomeData.objects.filter(date=yesterday).first():
            models.HomeData.objects.filter(date=yesterday).update(**data)
        else:
            data.update({"date": yesterday})
            models.HomeData.objects.create(**data)

        # 最大执行次数和最大成功次数，用于确认页面的y轴数据
        max_all_count = models.Report.objects.all().count()
        max_pass_count = models.Report.objects.filter(status=1).count()

        project_count = models.Project.objects.filter(is_delete=1).count()
        interface_count = models.Interface.objects.filter(is_delete=1).count()
        collection_count = models.CaseCollection.objects.filter(is_delete=1).count()
        case_count = models.Case.objects.filter(is_delete=1).count()
        home_data = models.HomeData.objects.all().values()

        pass_count = tools.qs_to_list(home_data, 'pass_count')
        date = tools.qs_to_list(home_data, 'date')
        all_count = tools.qs_to_list(home_data, 'all_count')
        fail_count = tools.qs_to_list(home_data, 'fail_count')
        data = {
            'project_count': project_count,
            'interface_count': interface_count,
            'collection_count': collection_count,
            'case_count': case_count,
            'pass_count': pass_count,
            'date': date,
            'all_count': all_count,
            'fail_count': fail_count,
            'max_all_count': max_all_count,
            'max_pass_count': max_pass_count
        }
        return NbResponse(data=data)