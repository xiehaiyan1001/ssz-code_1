class CaseRun(View):
    def post(self, request):
        case_id = request.POST.get('case_id')
        user_id = request.POST.get('user_id')
        for item in tools.str_for_dict(case_id):
            # 测试方法，后续需要调整到异步task中
            # task_run_test(item, user_id, 1)
            # 调用task任务 参数可以在delay中传递，正常调用一样
            # task_id = run_case_task.delay(item, user_id)
            task_id = run_case_task.delay(item, request.user.id)
            # 设置最后一次运行时的批次号
            models.Case.objects.filter(id=item).update(report_batch=task_id, status=3)
        return NbResponse()


class CollectionRun(View):

    def post(self, request):
        collection_id = request.POST.get('collect_id')
        for item in tools.str_for_dict(collection_id):
            # 调用task任务 参数可以在delay中传递，正常调用一样
            task_id = run_collection_task.delay(item, request.user.id)
            # 设置最后一次运行时的批次号
            models.CaseCollection.objects.filter(id=item).update(report_batch=task_id, status=3)
        return NbResponse()


class CaseReportView(View):
    def get(self, request):
        case_id = request.GET.get('id')
        report_batch = request.GET.get('report_batch')
        case = models.Case.objects.get(id=case_id)
        report = models.Report.objects.filter(batch=report_batch, case_id=case.id).first()
        if report:
            data = {
                'title': case.title,
                'run_time': report.create_time,
                'project_name': case.project.name,
                'status': report.status,
                'case_collection': report.case_collection if report.case_collection else '单用例运行',
                'duration': report.duration,
                'run_user': report.user.name,
                'url': report.url,
                'method': case.get_method_display(),
                'check': case.check,
                'reason': report.reason,
                'params': report.params,
                'response': report.response
            }
            return NbResponse(data=data)
        else:
            return NbResponse(-1, msg='用例运行中，请耐心等待..')


class CollectionReportView(View):

    def get(self, request):
        case_collection_id = request.GET.get('id')
        report_batch = request.GET.get('report_batch')
        case_collection = models.CaseCollection.objects.get(id=case_collection_id)
        report = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id).first()
        fail_count = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id,
                                                  status=999).count()
        pass_count = models.Report.objects.filter(batch=report_batch, case_collection=case_collection.id,
                                                  status=1).count()
        if report:
            data = {
                'case_collection': case_collection.name,
                'run_time': report.create_time,
                'case_count': case_collection.case.all().count(),
                'pass_count': pass_count,
                'run_user': report.user.name,
                'fail_count': fail_count,
                'duration': 100,
                'report_batch': case_collection.report_batch,
            }
            return NbResponse(data=data)
        else:
            return NbResponse(-1, msg='用例集合运行中，请耐心等待..')


class ReportView(GetView):
    search_fields = ['title']
    filter_fields = ['project', 'case_collection', 'batch']
    model_class = models.Report
    exclude_fields = ['is_delete']

    def get(self, request):
        page_data, page_obj = self.get_page_data(request)
        # 用于存储返回的dict类型数据
        data_list = []
        for instance in page_data:
            model_dict = model_to_dict(instance, self.fields, self.exclude_fields)  # 转成字典
            model_dict['title'] = instance.case.title
            model_dict['project_name'] = instance.case.project.name
            model_dict['case_collection'] = instance.case_collection.name if instance.case_collection else '单用例运行'
            model_dict['status'] = instance.case.status
            model_dict['run_user'] = instance.user.name
            model_dict['run_time'] = instance.create_time
            model_dict['check'] = instance.case.check
            model_dict['method'] = instance.case.get_method_display()
            data_list.append(model_dict)
        return NbResponse(data=data_list, count=page_obj.count)


class HomeDateView(View):
    def get_source_data(self, day):
        pass_count = models.Report.objects.filter(create_time__contains=day, status=1).count()
        fail_count = models.Report.objects.filter(create_time__contains=day, status=999).count()
        all_count = models.Report.objects.filter(create_time__contains=day).count()
        data = {
            "pass_count": pass_count,
            "fail_count": fail_count,
            "all_count": all_count
        }
        return data

    def get(self, request):
        import datetime
        today = datetime.date.today()
        # 统计数据 当天数据
        data = self.get_source_data(today)
        if models.HomeData.objects.filter(date=today).first():
            models.HomeData.objects.filter(date=today).update(**data)
        else:
            data.update({"date": today})
            models.HomeData.objects.create(**data)
        # 昨天数据

        yesterday = today - datetime.timedelta(days=1)
        data = self.get_source_data(yesterday)
        if models.HomeData.objects.filter(date=yesterday).first():
            models.HomeData.objects.filter(date=yesterday).update(**data)
        else:
            data.update({"date": yesterday})
            models.HomeData.objects.create(**data)

        # 最大执行次数和最大成功次数，用于确认页面的y轴数据
        max_all_count = models.Report.objects.all().count()
        max_pass_count = models.Report.objects.filter(status=1).count()

        project_count = models.Project.objects.filter(is_delete=1).count()
        interface_count = models.Interface.objects.filter(is_delete=1).count()
        collection_count = models.CaseCollection.objects.filter(is_delete=1).count()
        case_count = models.Case.objects.filter(is_delete=1).count()
        home_data = models.HomeData.objects.all().values()

        pass_count = tools.qs_to_list(home_data, 'pass_count')
        date = tools.qs_to_list(home_data, 'date')
        all_count = tools.qs_to_list(home_data, 'all_count')
        fail_count = tools.qs_to_list(home_data, 'fail_count')
        data = {
            'project_count': project_count,
            'interface_count': interface_count,
            'collection_count': collection_count,
            'case_count': case_count,
            'pass_count': pass_count,
            'date': date,
            'all_count': all_count,
            'fail_count': fail_count,
            'max_all_count': max_all_count,
            'max_pass_count': max_pass_count
        }
        return NbResponse(data=data)