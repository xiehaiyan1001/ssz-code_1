import pickle

from django.middleware.common import MiddlewareMixin
from django.http import QueryDict

from example import const
from sky.settings import DEBUG, NO_LOGIN_LIST
from example.custom_response import NbResponse
import django_redis


class PutMethodMiddleware(MiddlewareMixin):
    @staticmethod
    def process_request(request):
        # 所有请求先走到这，然后再去请求视图
        if request.method == 'PUT':
            if 'boundary' in request.content_params.keys():
                put_data, files = request.parse_file_upload(request.META, request)
                request.PUT = put_data
                request._files = files  # 是因为request.FILES 里面取值的时候，就是_files
            else:
                request.PUT = QueryDict(request.body)


class ExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        # 拦截异常的
        if not DEBUG:
            return NbResponse(code=500, msg='系统开小差了，请联系管理员 %s' % exception)


class SessionMiddleware(MiddlewareMixin):
    # 增加 一个过滤的场景
    # 预制一个 列表  列表用于存储，那些接口可以不登录就访问
    def check_url(self, path_info):
        for url in NO_LOGIN_LIST:
            # path_info 前端过来的路径   /api/login
            # url NO_LOGIN_LIST  login
            if url in path_info:
                # 当前访问的接口路径 在  NO_LOGIN_LIST 返回false 不做token验证
                return False
        return True

    def process_request(self, request):
        '''请求过来之后先走到的这里'''
        # print("dir(request)",dir(request))
        # 前端传递过来的token 会自动变成大写，并且在前面增加HTTP
        # print("request.META.get('HTTP_TOKEN')",request.META.get('HTTP_TOKEN'))
        print('request.path_info', request.path_info)
        if self.check_url(request.path_info):
            token = request.META.get('HTTP_TOKEN')
            if token:
                redis = django_redis.get_redis_connection()
                b_data = redis.get(const.SESSION + token)
                if b_data:
                    data = pickle.loads(b_data)
                    # 将当前的登录信息 存储到request当中
                    request.user = data
                    request.user_id = data.id
                    request.token = token
                else:
                    return NbResponse(-1, '请登录')
            else:
                return NbResponse(-1, '请登录')
