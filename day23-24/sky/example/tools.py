import datetime
import hashlib
import json


def md5(s):
    '''md5'''
    m = hashlib.md5(s.encode())
    return m.hexdigest()


# 优化 errors返回的信息
class ErrorFormat:
    @property
    def error_format(self):
        errors_data = self.errors.get_json_data()
        # {'username': [{'message': '用户不存在', 'code': ''}]}
        print("errors_data", errors_data)
        error_list = []
        # 错误列表 用于存储所有的错误数据
        for k, v in errors_data.items():
            error_list.append(k + ':' + v[0].get("message"))
        return ','.join(error_list)


def str_for_dict(data):
    if data:
        return json.loads(data)
    else:
        return data


def qs_to_list(d, f):
    '''
    根据传入的queryset 将qs转成列表
    :param d: queryset的字段
    :param f: 要转成列表的key
    :return:
    '''
    l = []
    for item in d:
        if isinstance(item[f], datetime.date):
            value = item[f].strftime('%Y-%m-%d')
            l.append(value)
        else:
            l.append(item[f])
    return l