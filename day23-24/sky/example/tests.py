import django, os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')  # 设置django的配置文件
django.setup()
from celery_tasks.run.tasks import demo

# 通过 这个方法 调用 异步服务的函数
demo.delay()
#
#
# from example import models
# models.Roles.objects.create(name='qa',desc='测试同学')
# models.Roles.objects.create(name='rd',desc='开发同学')
#
#
# models.Permission.objects.create(user_id=1,roles_id=1)
# models.Permission.objects.create(user_id=1,roles_id=2)


