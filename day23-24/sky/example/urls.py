"""sky URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from .views import f_parameter, SParameter
from . import views
# from .views import Parameter
urlpatterns = [
    # fbv
    path('f_parameter', f_parameter),
    # cbv
    # path('c_parameter', Parameter.as_view()),
    # 优化代码路由
    # path('s_parameter', SParameter.as_view()),
    path('parameter', SParameter.as_view()),
    path('register', views.Register.as_view()),
    path('login', views.Login.as_view()),
    path('logout', views.Logout.as_view()),
    path('info', views.Info.as_view()),
    path('project', views.Project.as_view()),
    path('interface', views.Interface.as_view()),
    path('case', views.Case.as_view()),
    path('get_rely_case', views.GetRelyCase.as_view()),
    path('case_collection', views.CaseCollection.as_view()),
    path('join_case', views.JoinCase.as_view()),
    path('run', views.CaseRun.as_view()),
    path('run_collection', views.CollectionRun.as_view()),
    path('case_report', views.CaseReportView.as_view()),
    path('collection_report', views.CollectionReportView.as_view()),
    path('report', views.ReportView.as_view()),
    path('home_page', views.HomeDateView.as_view()),

]
