from django.db import models
from sky.settings import SECRET_KEY
from example.tools import md5


class BaseModel(models.Model):
    '''公共字段'''
    is_delete_choice = (
        (1, '删除'),
        (0, '正常')
    )
    is_delete = models.SmallIntegerField(choices=is_delete_choice, default=0, verbose_name='是否被删除')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)  # auto_now_add的意思，插入数据的时候，自动取当前时间
    update_time = models.DateTimeField(verbose_name='修改时间', auto_now=True)  # 修改数据的时候，时间会自动变

    class Meta:
        abstract = True  # 只是用来继承的,不会创建这个表


class Parameter(BaseModel):
    name = models.CharField(max_length=200, verbose_name='参数名', unique=True)
    desc = models.CharField(max_length=200, verbose_name='描述')
    value = models.CharField(max_length=200, verbose_name='参数值')

    def __str__(self):
        return self.name

    class Meta:
        # django admin
        verbose_name = '全局参数表'
        verbose_name_plural = verbose_name
        db_table = 'parameter'
        # 最后创建的在最上面
        ordering = ['-id']


class User(BaseModel):
    name = models.CharField(max_length=200, verbose_name='名字')
    phone = models.CharField(max_length=11, verbose_name='手机号-username')
    password = models.CharField(max_length=200, verbose_name='密码-password')

    # 密码我们需要进行加密 hashlib 将字符串 进行md5
    # 加 盐 的过程 ，
    # 设置密码、加密密码、验证密码
    @staticmethod
    def mack_password(row_password):
        '''

        :param row_password: 原始密码
        :return:
        '''
        tmp_password = '%s%s' % (row_password, SECRET_KEY)
        md5_password = md5(tmp_password)
        return md5_password

    def check_password(self, row_password):
        '''
            验证加密的密码 和 传过来的密码是否相同
            前端页面传递过来的  123456
            mack_password(123456)  ==  self.password
        :param row_password:
        :return:
        '''
        return self.mack_password(row_password) == self.password

    def set_password(self, row_password):
        '''
            设置密码
        :param row_password: 123456 这种原始密码
        :return:
        '''
        self.password = self.mack_password(row_password)

    def __str__(self):
        return self.name

    class Meta:
        # django admin
        verbose_name = '用户表'
        verbose_name_plural = verbose_name
        db_table = 'user'


class Roles(BaseModel):
    desc = models.CharField(verbose_name='角色描述', max_length=20, unique=True)
    name = models.CharField(verbose_name='角色标识', max_length=20, unique=True)

    class Meta:
        verbose_name = '角色表'
        verbose_name_plural = verbose_name
        db_table = 'roles'


class Permission(BaseModel):
    roles = models.ForeignKey(Roles, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='角色')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='用户')

    class Meta:
        verbose_name = '权限表'
        verbose_name_plural = verbose_name
        db_table = 'permission'


class Project(BaseModel):
    '''项目表'''
    name = models.CharField(verbose_name='项目名称', max_length=100, unique=True)
    desc = models.CharField(verbose_name='项目描述', max_length=200, null=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='创建用户')
    host = models.CharField(verbose_name='测试环境', max_length=1024)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '项目表'
        verbose_name_plural = verbose_name
        db_table = 'project'
        ordering = ['-id']


class Interface(BaseModel):
    name = models.CharField(verbose_name='接口名称', max_length=100, unique=True)
    uri = models.CharField(verbose_name='接口路径', max_length=100)
    params = models.CharField(verbose_name='请求参数', max_length=2048, null=True, blank=True)
    headers = models.CharField(verbose_name='请求头信息', max_length=2048, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='归属项目')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='创建用户')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '接口表'
        verbose_name_plural = verbose_name
        db_table = 'interface'
        ordering = ['-id']


class Case(BaseModel):
    '''用例表'''
    title = models.CharField(verbose_name='用例标题', max_length=100)
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='归属项目')
    interface = models.ForeignKey(Interface, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='接口')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='创建用户')
    method_choice = (
        (1, 'POST'),
        (2, 'GET'),
        (3, 'DELETE'),
        (4, 'PUT'),
    )
    method = models.SmallIntegerField(choices=method_choice, verbose_name='请求方式')
    cache_field = models.CharField(verbose_name='缓存字段', max_length=128, null=True, blank=True)
    check = models.CharField(verbose_name='校验点', max_length=512)
    params = models.CharField(verbose_name='请求参数', max_length=2048, null=True, blank=True)
    headers = models.CharField(verbose_name='请求头信息', max_length=2048, null=True, blank=True)
    is_json = models.BooleanField(verbose_name='参数是否是json', default=False)
    json = models.CharField(verbose_name='json类型参数', max_length=2048, null=True, blank=True)
    status_choice = (
        (1, '通过'),
        (2, '未运行'),
        (3, '运行中'),
        (999, '失败')
    )
    status = models.SmallIntegerField(choices=status_choice, verbose_name='用例状态',
                                      default=2)  # 记录上一次的状态 每次执行后需要更新下这个表的这个字段
    report_batch = models.CharField(verbose_name='最后一次执行的批次号', null=True, max_length=512, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '用例表'
        verbose_name_plural = verbose_name
        db_table = 'case'
        ordering = ['-id']


class CaseCollection(BaseModel):
    name = models.CharField(verbose_name='用例集合名称', max_length=100, unique=True)
    desc = models.CharField(verbose_name='用例集合描述', max_length=200, null=True)
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='归属项目')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='创建用户')
    case = models.ManyToManyField(Case, verbose_name='集合中的用例')
    report_batch = models.CharField(verbose_name='最后一次执行的批次号', null=True, max_length=512, blank=True)
    status_choice = (
        (2, '未运行'),
        (3, '运行中'),
        (4, '执行完毕')
    )
    status = models.SmallIntegerField(choices=status_choice, verbose_name='集合状态',
                                      default=2)  # 记录上一次的状态 每次执行后需要更新下这个表的这个字段

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '用例集合表'
        verbose_name_plural = verbose_name
        db_table = 'case_collection'
        ordering = ['-id']


class Report(BaseModel):
    '''用例报告表'''
    url = models.CharField(verbose_name='请求URL', max_length=1024)
    project = models.ForeignKey(Project, verbose_name='项目', on_delete=models.DO_NOTHING, db_constraint=False)
    title = models.CharField(verbose_name='用例名称', max_length=100)
    params = models.CharField(verbose_name='请求参数', max_length=2048)
    response = models.CharField(verbose_name='接口返回值结果', max_length=2048)
    case = models.ForeignKey(Case, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='结果所属用例')
    case_collection = models.ForeignKey(CaseCollection, on_delete=models.DO_NOTHING, db_constraint=False,
                                        verbose_name='结果所属集合', null=True)
    batch = models.CharField(verbose_name='批次', null=True, max_length=128)  # 用于区分运行的第几批集合的用例
    reason = models.CharField(verbose_name='失败原因', null=True, max_length=128, blank=True)
    status_choice = (
        (1, '通过'),
        (999, '失败')
    )
    duration = models.IntegerField(verbose_name='用例耗时')
    status = models.SmallIntegerField(choices=status_choice, verbose_name='用例结果状态')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_constraint=False, verbose_name='运行用户')

    class Meta:
        verbose_name = '用例报告表'
        verbose_name_plural = verbose_name
        db_table = 'report'
        ordering = ['-id']


class HomeData(BaseModel):
    date = models.DateField(verbose_name='统计时间', unique=True)
    pass_count = models.IntegerField(verbose_name='成功次数', default=0)
    all_count = models.IntegerField(verbose_name='执行次数', default=0)
    fail_count = models.IntegerField(verbose_name='失败次数', default=0)

    class Meta:
        verbose_name = '首页数据统计'
        verbose_name_plural = verbose_name
        db_table = 'home_data'
        ordering = ['-id']


class CasePremise(models.Model):
    case = models.ForeignKey(Case, models.DO_NOTHING, db_constraint=False, unique=False, verbose_name='用例',
                             related_name='case')
    premise_case = models.ForeignKey(Case, models.DO_NOTHING, db_constraint=False, unique=False, verbose_name='依赖用例',
                                     related_name='premise_case')

    class Meta:
        unique_together = ('case', 'premise_case')  # 联合主键，一个case对另外一个case只能依赖一次
        db_table = 'case_premise'
        verbose_name = '依赖用例表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.case