from django import forms
from example import models
from . import tools


class ParameterForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.Parameter  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time']


class ProjectForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.Project  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time']


class InterfaceForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.Interface  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time']


class CaseForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.Case  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time', 'status']


class CaseCollectionForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.CaseCollection  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time', 'status','case']


class RegisterForm(forms.ModelForm, tools.ErrorFormat):
    class Meta:
        model = models.User  # 创建类变量和model建立映射关系
        exclude = ['id', 'is_delete', 'update_time', 'create_time']


# 不能用ModelForm
# 登录要验证什么？  username   password
class LoginForm(forms.Form, tools.ErrorFormat):
    username = forms.CharField(min_length=5, max_length=20, required=True)
    password = forms.CharField(min_length=6, max_length=20, required=True)

    def clean(self):
        print('self.errors', self.errors)
        print('self.cleaned_data', self.cleaned_data)
        # 没有错误代表 username password 最基本验证是通过的
        if not self.errors:
            # 继续验证 账号密码是否正确
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            # 验证用户是否存在
            qs = models.User.objects.filter(phone=username)
            # 如果过滤后有值代表有用户
            if qs:
                user_obj = qs.first()
                if user_obj.check_password(password):
                    self.cleaned_data['user'] = user_obj
                    return self.cleaned_data
                else:
                    self.add_error('password', '密码错误')
            else:
                self.add_error('username', '用户不存在')

        return self.cleaned_data
