from example import models


def get_premise_case(instance):
    '''
    获取用例的依赖用例
    :param instance: 用例的obj对象
    :return: [{'id':1,'title':'test'}]
    1、根据用例的obj对象 获取当前用例所有的 依赖用例信息（自关联的查询）
    2、按照固定的格式进行返回
    '''
    # 获取自关联数据
    case_premise_qs = instance.case.all()
    print("当前用例id", instance.id)
    # case_premise_qs  case_premise表的qs
    case_premises = []
    for case_premise_obj in case_premise_qs:
        # print("case_premise_title", case_premise_obj.premise_case.title)
        # print("case_premise_id", case_premise_obj.premise_case.id)
        case_premises.append({"id": case_premise_obj.premise_case.id, "title": case_premise_obj.premise_case.title})
    return case_premises


class Premise:
    def __init__(self):
        self.premise_list = []  # 用于存所有被递归发现的依赖用例id

    def get_premise_all(self, case_id):
        # 假设 case_id = 8   依赖用例就是 3，4
        premise_qs = models.Case.objects.get(id=case_id).case.all()
        return premise_qs

    def loop_premise(self, case_id):
        # qs 是当前依赖用例id 所依赖的所有id
        qs = self.get_premise_all(case_id)
        for obj in qs:
            self.premise_list.append(obj.premise_case.id)
            # 自己掉自己的条件
            if self.get_premise_all(obj.premise_case.id):
                self.loop_premise(obj.premise_case.id)
        return self.premise_list


def check_premise_case(case_id, premise_id):
    '''
    验证 premise_id 依赖的用例id 是否依赖case_id
    :param case_id: 当前用例id
    :param premise_id: 依赖id
    :return: true 或 false
    A B  C
    case_id   A
    premise_id B -- C -- D -- E
    递归场景  本质是自己掉自己  递归也是有次数限制的
    每次递归 量级上要有所减少，而且有条件能够结束递归

    1、获取premise_id 的依赖用例id---》
    2、判断 case_id 是否 依赖id 列表中
    '''
    p = Premise()
    premise_list = p.loop_premise(premise_id)
    if int(case_id) in premise_list:
        return False
    else:
        return True
