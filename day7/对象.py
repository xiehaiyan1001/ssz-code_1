class DictToObject(dict):
    '''字典转对象'''
    def __getattr__(self, item):
        #__getattr__的作用是通过x.xx的时候它会自动调用__getattr__这个方法，把你要获取的属性的key传过来
        #比如说 user.name ,它就是调用了__getattr__，把name传给__getattr__函数，然后返回这个name的值
        value = self[item]
        if isinstance(value,dict):
            value = DictToObject(value)#如果是字典类型，直接返回DictToObject这个类的对象

        elif isinstance(value,list) or isinstance(value,tuple):
            #如果是list，循环list判断里面的元素，如果里面的元素是字典，那么就把字典转成DictToObject的对象
            value = list(value)
            for index,obj in enumerate(value):
                if isinstance(obj,dict):
                    value[index] = DictToObject(obj)

        return value

if __name__ == '__main__':
    person = {'name':'xiaohei',
              'sex':'male',
              'addr':'beijing',
              'car':[{'country':'china','brand':'byd'},{'country':'jp','brand':'nissan'}],
              'money':{'financial':5000,'salary':6000,'stock':7000}
              }

    p_obj = DictToObject(person)
    print(p_obj.name)
    print(p_obj.money)
    print(p_obj.car[0].country)
    print(p_obj.get('money'))



    # print(p_obj.name)
    # print(p_obj.sex)
    # print(p_obj.money.salary)
    # print(p_obj.car[0].brand)
    # print(p_obj.car[0].country)
    # print(p_obj.car[1].country)



