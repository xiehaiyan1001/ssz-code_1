import time

import flask
import json
import pymysql
import hashlib
import redis
#flask轻量级的web开发框架

server = flask.Flask(__name__)

def op_mysql(sql,one_tag=False):
    connect=pymysql.connect(host='118.24.3.40',
                    user='jxz',
                    password='123456',
                    db='jxz',
                    port=3306,
                    charset='utf8',
                    autocommit=True
                    )
    cursor = connect.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)
    if one_tag:
        result = cursor.fetchone()
    else:
        result = cursor.fetchall()
    cursor.close()
    connect.close()
    return result

#123456nhy_iodsgs233_12
def md5(s,salt='nhy_iodsgs233_12'):
    s = str(s)+salt
    m = hashlib.md5(s.encode())
    return m.hexdigest()

def op_redis(key,value=None,expire=60*60*2):
    r =  redis.Redis(host='118.24.3.40',password='HK139bc&*',decode_responses=True)
    if value:
        r.set(key,value,expire)
    else:
        return r.get(key)




@server.route('/api/payment',methods=['get'])
def payment():
    status = flask.request.values.get('status')
    if status == 'success':
        data = {"code":0,"msg":"支付成功","amount":30000}
    elif status == 'process':
        data = {"code":305,"msg":"支付处理中","amount":30000}
    elif status == 'fail':
        data = {"code":-1,"msg":"失败","amount":30000}
    else:
        data = {"code":400,"msg":'支付状态错误'}
    return json.dumps(data,ensure_ascii=False)

@server.route('/api/login_old')
def login_old():
    data = {"code":0,"msg":"登录成功"}
    return json.dumps(data,ensure_ascii=False)

@server.route('/account_data',methods=['post'])
def account_data():
    account_name = flask.request.values.get('account_name')
    # flask.request.json.get()
    result = op_mysql('select * from gtm_account where account="%s";'%account_name)
    data = {"code":0,"msg":"success","data":result}
    return json.dumps(data,ensure_ascii=False)


@server.route('/register',methods=['post'])
def register(): #app_myuser
    username = flask.request.values.get('username')
    password = flask.request.values.get('password')
    cpwd = flask.request.values.get('cpwd')
    sql = 'select * from app_myuser where username ="%s";'%username
    if username and password and cpwd:
        if password !=cpwd:
            data = {'code': 400, 'msg': '两次输入密码不一致'}
        elif op_mysql(sql):
            data = {'code':401,'msg':'用户已经存在'}
        else:
            password = md5(password)
            insert_sql = 'insert into app_myuser (username,passwd) values ("%s","%s");'%(username,password)
            op_mysql(insert_sql)
            data = {'code':0,'msg':'注册成功'}
    else:
        data = {'code':400,'msg':'必填参数不能为空'}

    return json.dumps(data,ensure_ascii=False)



@server.route('/login',methods=['post'])
def login(): #app_myuser
    username = flask.request.values.get('username')
    password = flask.request.values.get('password') #123456
    if username and password:
        sql = 'select * from app_myuser where username ="%s";' % username
        result = op_mysql(sql,True)
        if result:
            if md5(password) == result.get('passwd'):
                token = md5(username + str(time.time()))
                info = {'username':username,'id':result.get('id')}
                op_redis(token,json.dumps(info))
                # 6881ad02521e0bd6eefae0072acb4e1e = >  {"username":"songxueyuan","id":1}
                data = {'code':0,'msg':'登录成功','token':token}
            else:
                data = {'code':403,'msg':'账号/密码错误'}
        else:
            data = {'code': 401, 'msg': '用户不存在'}
    else:
        data = {'code': 400, 'msg': '必填参数不能为空'}

    return json.dumps(data,ensure_ascii=False)


@server.route('/api/v2/payment',methods=['get'])
def payment_v2():
    token = flask.request.values.get('token')
    amount = flask.request.values.get('amount')
    #todo:check_amount
    if token:
        result = op_redis(token)
        if result:
            amount = float(amount)
            result = json.loads(result)
            userid = result.get('id')
            sql = 'update app_myuser set balance=balance-%s where id = %s;'%(amount,userid)
            op_mysql(sql)
            data = {'code':0,'msg':'支付成功'}
        else:
            data = {'code': 401, 'msg': '未登录'}
    else:
        data = {'code':401,'msg':'未登录'}
    return json.dumps(data,ensure_ascii=False)
server.run(port=9999,debug=True)
# server.run(host='0.0.0.0',port=9999,debug=True)

#http://192.168.1.3:9999/api/payment
#server.run(port=9999,debug=True)#这种方式启动，别人访问不了


#host='0.0.0.0'


#http://ip:8000/api/payment