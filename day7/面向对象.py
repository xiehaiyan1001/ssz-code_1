class Car:#类 经典类

    def run(self):
        print('run。。。')
    def fly(self):
        print('fly...')

# bmw = Car()#实例化
# bmw.run()
# bmw.fly()
# bmw.fly()
#
# benz = Car()

class Person(object): #新式类
    def __init__(self,name,sex,color):
        self.name = name #属性
        self.sex = sex
        self.color = color
        print('self的内存地址',id(self))

    def cry(self):
        print('%s cry....'%self.name)

    def fly(self):
        print('%s fly....'%self.name)

    def eat(self):
        print('color',self.color)
        print('eat..')

ccx = Person('常彩霞','女','黑色')
print('实例的内存地址',id(ccx))
ccx.eat()  #eat(ccx)

# ccx.eat()
# ccx.fly()

# lj = Person('刘婕','女','白色')
# lj.fly()

class Tool:
    def md5(self):
        pass
    def clean_log(self):
        pass
    def hanyu_to_pinyin(self):
        pass
