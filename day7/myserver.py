from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel

class User(BaseModel):
    name:str
    addr:str

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}

@app.post("/test/")
def read_item(user: User):
    print(user)
    return dict(user)

if __name__ == '__main__':
    uvicorn.run('myserver:app',port=9000,host='0.0.0.0',debug=True)