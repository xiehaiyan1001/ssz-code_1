#1、连数据库
#2、执行sql
#3、关闭数据库
import pymysql

#面向过程
def connect():
    connect = pymysql.connect(host='118.24.3.40',
                              user='jxz',
                              password='123456',
                              db='jxz',
                              port=3306,
                              charset='utf8',
                              autocommit=True
                              )
    cursor = connect.cursor()
    return connect,cursor

def execute(cursor,sql):
    cursor.execute(sql)
    result = cursor.fetchall()
    return result

def close(connect,curosr):
    connect.close()
    curosr.close()

# conn,curosr = connect()
# sql = ['a','b','c']
# for s in sql:
#     result = execute(curosr,s)
# close(conn,curosr)

class MysqlDB:
    def __init__(self,mysql_info):
        self.mysql_info = mysql_info
        self.__connect()

    def __del__(self):
        self.__close()

    def __connect(self):
        self.conn = pymysql.connect(**self.mysql_info)
        self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)

    def execute(self,sql):
        self.cursor.execute(sql)

    def fetchone(self): #select * from user where username = 'xx';
        return self.cursor.fetchone()

    def fetchall(self):#select * from user;
        return self.cursor.fetchall()

    def fetchmany(self,limit):
        return self.cursor.fetchmany(limit)

    def __close(self):
        self.cursor.close()
        self.conn.close()

mysql_info = {'host':'118.24.3.40','user':'jxz','password':'123456',
              'port':3306,'autocommit':True,'charset':'utf8','db':'jxz'}
m = MysqlDB(mysql_info)

m.execute('select * from app_myuser where username = "liangcailian";')

result = m.fetchone()
