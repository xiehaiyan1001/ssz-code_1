
#1、传入qq号，下载群里所有人的头像，图片的文件名取群备注/昵称
#2、调用接口，获取到所有人的qq号、nick
    #1、获取到群里面有多少人，一次能获取到40个
    #2、100 / 40 = 2.xxx
#3、下载头像
import requests
url = "https://qun.qq.com/cgi-bin/qun_mgr/search_group_members"#根据qq群号获取群消息
img_url = "https://q4.qlogo.cn/g?b=qq&nk=%s&s=140"


data = {'gc':806298801,'st':0,'end':1,'sort':0,'bkn':138851395}
header = {'cookie':'pgv_pvi=6636933120; RK=gRZhhBpNbS; ptcz=14bab564718e3e1048a09cc0e18a23f7c51f20d5b93da610cc1427f51f63a2f8; pgv_pvid=4990195883; ts_uid=5190463916; pgv_si=s9611632640; uin=o0511402865; skey=@xXicJXShe; p_uin=o0511402865; pt4_token=PMLMawLBCWZWCg0lNqX0RMyveUbQhhIQ7LghBfltOiQ_; p_skey=N9BjavmXFbrWGT0Dn9ZuCgcSq8ya9FBQKFMGgA--ygk_; traceid=ecfbe04cee'}
req = requests.post(url,data,headers=header)
result = req.json()
#1获取到多少人
count = result.get('count') #100


#1、算两次的方式
# number = count // 40 # 2
# if count % 40 !=0:
#     number+=1
#2、结果转字符串的方式
number = count / 40 #1.3
if int(str(number).split('.')[-1]) >0:
    number+=1
number = int(number)



st = 0
end = st+40
for i in range(number):
    data = {'gc': 806298801, 'st': st, 'end': end, 'sort': 0, 'bkn': 138851395}
    req = requests.post(url, data, headers=header)
    members = req.json().get('mems')
    print('st',members)
    for member in members:
        qq = member.get('uin')
        card = member.get('card') if member.get('card') else member.get('nick')
        img_req = requests.get(img_url % qq)
        with open(r'pics/%s.jpg' % card,'wb') as fw:
            fw.write(img_req.content)
    st+=41
    end=st+40

