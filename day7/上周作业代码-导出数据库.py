import pymysql,xlwt
#传入一个表名，吧表里的内容导出到excel里面
#1、连接数据库，执行sql，查询到表里所有的数据
#2、循环数据，存到excel里面

def op_mysql(sql,type=None):
    connect=pymysql.connect(host='118.24.3.40',
                    user='jxz',
                    password='123456',
                    db='jxz',
                    port=3306,
                    charset='utf8',
                    autocommit=True
                    )
    cursor_type = pymysql.cursors.DictCursor if type == 'dict' else None
    cursor = connect.cursor(cursor_type)
    try:
        cursor.execute(sql)
    except Exception as e: #Exception, e
        quit('sql错误')
    else:
        fields = [i[0] for i in cursor.description]
        result = list(cursor.fetchall())
        result.insert(0,fields)
        return result
    finally:
        cursor.close()
        connect.close()


def export_table(table_name):
    sql = f'select * from {table_name};' #这种方式3.6以上的python可以用
    result = op_mysql(sql)
    book = xlwt.Workbook()
    sheet = book.add_sheet('sheet1')
    for row_number,row in enumerate(result): #[a,b,c ]
        for col_number,col in enumerate(row):
            sheet.write(row_number,col_number,col)
    book.save(f'{table_name}.xls')

export_table('gtm_account')