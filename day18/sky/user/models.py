from django.db import models

# Create your models here.


class Category(models.Model):
    name = models.CharField(verbose_name='分类名称',max_length=20,unique=True)
    create_time = models.DateTimeField(verbose_name='创建时间',auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='修改时间',auto_now=True)

    class Meta:
        db_table = 'category' #创建表的时候表名
        verbose_name = '分类'
        verbose_name_plural = verbose_name
        ordering = ['-create_time','name']

def category_fun():
    return 883

class Article(models.Model):
    title = models.CharField(verbose_name='标题', max_length=25)  # varchar
    content = models.TextField(verbose_name='文章内容')
    img = models.ImageField(upload_to='article', blank=True)
    category = models.ForeignKey(Category,
                                 on_delete=models.SET(category_fun), #cascade
                                 db_constraint=False,verbose_name='分类')

    #CASCADE，关联的数据也会被删除
    #models.PROTECT #受保护，不能删，只要这个分类下面有文章，那么这个分类就不能被删
    # models.DO_NOTHING #什么也不再
    #models.SET_NULL 设置成空，需要这个字段可以为空
    # models.SET_DEFAULT，设置成这个字段的默认值，需要这个字段有默认值
    # models.SET #传入一个方法名，用方法返回值
    #https://www.cnblogs.com/zhangqunshi/p/6953915.html 外键on_delete选项
    #db_constraint=False，不会真的在表里面创建外键关联
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='修改时间', auto_now=True)

    class Meta:
        db_table = 'article'  # 创建表的时候表名
        verbose_name = '文章'
        verbose_name_plural = verbose_name
        ordering = ['-create_time']
