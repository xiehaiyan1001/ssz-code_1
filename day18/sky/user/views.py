from django.http import HttpResponse
from django.shortcuts import render
from . import models
# Create your views here.

def user_info(request):
    s = '你好'
    return HttpResponse(s) #返回的字符串的话用它

def welcome(request):
    username = 'changcaixia' #request
    username_zh = '常彩霞-使用的reaplace' #sql查

    return render(request,'welcome.html',{'username_zh':username_zh})

def index(request):
    categories = models.Category.objects.all()
    articles = models.Article.objects.all()
    return render(request,'index.html',{'categories':categories,'articles':articles})