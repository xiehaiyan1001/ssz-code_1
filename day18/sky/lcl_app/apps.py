from django.apps import AppConfig


class LclAppConfig(AppConfig):
    name = 'lcl_app'
