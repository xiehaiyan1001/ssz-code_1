import fastapi
import hashlib
server = fastapi.FastAPI()
salt = "ssz-abc"

from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex


class Prpcrypt():
    def __init__(self, key):
        '''
        :param key:security_key,一个16位的随机字符串，不要让别人知道
        '''
        self.key = key.encode()

    # 加密函数，如果text不是16的倍数【加密文本text必须为16的倍数！】，那就补足为16的倍数
    def encrypt(self, text):
        text = text.encode()
        cryptor = AES.new(self.key, AES.MODE_CBC, self.key)  # 生成一个加密对象

        # 这里密钥key 长度必须为16（AES-128）、24（AES-192）、或32（AES-256）Bytes 长度.目前AES-128足够用
        length = 16
        count = len(text)
        if count % length != 0:  # 判断加密的字符串长度是不是16的倍数
            add = length - (count % length)  # 如果不是的话，就在前面补多少个/0， 这里的算法是
            # 举例：53/16 商3 余5; 16-5 = 11, 53+11 = 64 ;，64是53上面最小的一个16的倍数
            # 53是加密内容的长度，不是16的倍数，通过 16 - (53 % 16 ) 算出需要补多少个/0，加密内容的长度才是16的倍数
        else:
            add = 0
        text = text + (b'\0' * add)
        ciphertext = cryptor.encrypt(text)
        # 因为AES加密时候得到的字符串不一定是ascii字符集的，输出到终端或者保存时候可能存在问题
        # 所以这里统一把加密后的字符串转化为16进制字符串,就是b9925b80185497ccbcc3d501604845ee这样的密文
        # b2a_hex这个函数的作用就是把bytes类型的转成16进制
        result = b2a_hex(ciphertext).decode()
        return result

    def decrypt(self, text):
        cryptor = AES.new(self.key, AES.MODE_CBC, self.key)  # 生成一个加密对象
        plain_text = cryptor.decrypt(a2b_hex(text))  # a2b_hex就是把16进制的转成bytes类型的
        return plain_text.decode()


#name、age、address、email、xxx
#abc+18+beijng+niuhanyang@173.com+xxx #
@server.post('/api/test')
def test(name:str,addr:str,phone:str,auth:str):
    s = '%s%s%s%s'%(name,addr,phone,salt)
    s = hashlib.md5(s.encode()).hexdigest()
    if auth == s:
        return  {"code":0,"msg":"操作成功","name":name,'addr':addr,'phone':phone}
    else:
        return {"code":-1,"msg":"验签失败"}

@server.get('/api/test1')
def test1(q:str):
    p = Prpcrypt('1234567890123456')
    # miwen = p.encrypt('{"code":1}')
    # print(miwen)
    try:
        result = p.decrypt(q)

    except:
        return {"code":-1,'msg':"解密失败"}

    return {"code":0,"msg":result}


if __name__ == '__main__':
    # d={
    #     "name": "自动化测试优惠券",
    #     "discount": 500,
    #     "count": 50
    # }
    # p = Prpcrypt('1234567890123456')
    # import json
    # miwen = p.encrypt(json.dumps(d))
    # print(miwen)


    import uvicorn
    uvicorn.run('接口:server',port=9000,host='0.0.0.0',debug=True)