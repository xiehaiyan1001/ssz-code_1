import jsonpath
import redis
from config.setting import MYSQL_INFO,REDIS_INFO,log
from .op_db import MySQL

def get_value(d, key,expr=False,more=False):
    if expr:
        jp = jsonpath.jsonpath(d, key)  # [1,2]
    else:
        jp = jsonpath.jsonpath(d, '$..%s' % key)  # [1,2]
    if jp:
        if more:
            return jp
        else:
            return jp[0]


def get_db(config_name='default'):
    mysql_info = MYSQL_INFO.get(config_name)
    log.debug("mysql连接信息：%s"%mysql_info)

    return MySQL(mysql_info)

def get_redis(config_name='default'):
    redis_info = REDIS_INFO.get(config_name)
    return redis.Redis(**redis_info)

if __name__ == '__main__':
    mysql = get_db()
    mysql.execute('sql')
    mysql.fetchall()

