#1、在project目录下建一个项目的目录：ltemall
#2、在ltemall目录下建4个目录，分别是  cases report public data
#3、在ltemall目录下创建run.py,并填充内容
#4、在public目录下创建const文件，并填充const内容
#5、如果是cases目录，要在这个目录下创建一个__init__.py,如果不加的话，cases目录里面
#你再创建文件夹，从文件夹里面写用例的话，他找不到
import os
from config.setting import PROJECTS_PATH,BASE_PATH
class Project:

    project_dirs = ['cases','report','public','data']

    def __init__(self,project_name):
        self.project_name = project_name
        self.project_path = os.path.join(PROJECTS_PATH,project_name)
        self.check_project()

    def check_project(self):
        if os.path.exists(self.project_path):
            quit("项目已经存在")

    def create_project_dir(self):
        '''创建项目目录'''
        os.mkdir(self.project_path)

    def create_child_dir(self):
        for dir in self.project_dirs:
            child_dir = os.path.join(self.project_path,dir)
            os.mkdir(child_dir)

    def create_run_py(self):
        template_path = os.path.join(BASE_PATH,'config','run_template')
        py_file = os.path.join(self.project_path,'run.py')
        with open(py_file,'w',encoding='utf-8') as fw, open(template_path,encoding='utf-8') as fr:
            py_file_content = fr.read().format(project_name=self.project_name)
            fw.write(py_file_content)

    def create_const_py(self):
        template_path = os.path.join(BASE_PATH,'config','const_template')
        py_file = os.path.join(self.project_path,'public','const.py')
        with open(py_file,'w',encoding='utf-8') as fw, open(template_path,encoding='utf-8') as fr:
            py_file_content = fr.read()
            fw.write(py_file_content)

    def create_init_py(self):
        py_file = os.path.join(self.project_path,'cases','__init__.py')
        with open(py_file, 'w', encoding='utf-8') as fw:
            pass

    def main(self):
        self.create_project_dir()
        self.create_child_dir()
        self.create_run_py()
        self.create_const_py()
        self.create_init_py()




