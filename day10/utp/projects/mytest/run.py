import unittest
import nnreport
import time
import os
from config.setting import dingding_template,mail_template
from utils.send_message import Send
from public.const import CASE_PATH,REPORT_PATH
suite = unittest.defaultTestLoader.discover(CASE_PATH,'test*.py')


def run_case():
    title = "mytest-接口测试报告-%s"%time.strftime("%Y-%m-%d %H-%M-%S")
    run = nnreport.BeautifulReport(suite)

    run.report(
        description=title,
        filename=title,
        log_path=REPORT_PATH
    )

    report_file_path = os.path.join(REPORT_PATH,title+'.html')

    all_count = run.success_count + run.failure_count

    mail_content = mail_template % (all_count,run.success_count,run.failure_count)
    dingding_content = dingding_template % (all_count,run.success_count,run.failure_count)
    Send.send_mail(title,mail_content,report_file_path)
    Send.send_dingding(dingding_content)
if __name__ == '__main__':
    run_case()




