import os
project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

REPORT_PATH = os.path.join(project_path,'report')#报告
DATA_PATH = os.path.join(project_path,'data')#数据
CASE_PATH = os.path.join(project_path,'cases')#用例

server_host = "http://api.nnzhp.cn" #这个是服务器的地址