import unittest
import parameterized
import os
from public.const import server_host,DATA_PATH
from urllib.parse import urljoin
from utils.http_request import HttpRequest
from utils.tools import get_value,get_db
from utils.get_data import GetData


data_file = os.path.join(DATA_PATH,'login_data')
class LoginTest(unittest.TestCase):

    url = urljoin(server_host,'/api/user/login')

    @classmethod
    def setUpClass(cls):
        cls.mysql = get_db()

    @parameterized.parameterized.expand(GetData.to_txt(data_file))
    def test_login(self,username,password):
        data = {'username':username,'passwd':password}
        r = HttpRequest(self.url,data)
        result = r.post()
        token = get_value(result,'sign')
        userid = get_value(result,'userId')
        sql = 'select id from app_myuser where username="%s";'%username
        self.mysql.execute(sql)
        sql_userid = self.mysql.fetchone().get('id')
        self.assertIsNotNone(token,msg="登录失败，获取不到token")
        self.assertEqual(userid,sql_userid,msg='接口返回的userid和数据库中的不一致')



