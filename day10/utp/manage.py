#提供项目管理的功能
from utils.project import Project
import os
def create_project():
    project_name = input('请输入项目名称:').strip()
    p = Project(project_name)
    p.main()

def clean_report():
    pass

def bak_db():
    pass


func_map = {
    "1":create_project,
    "2":clean_report,
    "3":bak_db
}
if __name__ == '__main__':
    choice = input(""
          "1、创建项目\n"
          "2、清理测试报告\n"
          "3、备份数据库\n"
                   ":")
    if choice in func_map:
        func_map.get(choice)()
    else:
        print('输入错误！')


