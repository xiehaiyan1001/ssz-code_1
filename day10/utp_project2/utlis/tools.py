import jsonpath


def get_value(d, key,expr=False,more=False):
    if expr:
        jp = jsonpath.jsonpath(d, key)  # [1,2]
    else:
        jp = jsonpath.jsonpath(d, '$..%s' % key)  # [1,2]
    if jp:
        if more:
            return jp
        else:
            return jp[0]