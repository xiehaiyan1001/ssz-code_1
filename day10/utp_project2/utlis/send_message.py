import yamail
import traceback
from config.setting import dingding_url,log
from config.setting import USER_MAIL_CC,USER_MAIL_LIST,MAIL_INFO
from .http_request import HttpRequest

class Send:
    @staticmethod
    def send_mail(title,content,file=None):
        log.debug("开始发邮件")
        try:
            smtp = yamail.SMTP(**MAIL_INFO)
            smtp.send(to=USER_MAIL_LIST,cc=USER_MAIL_CC,
                      subject=title,contents=content,
                      attachments=file)
        except Exception:
            log.error("发送邮件出错，错误原因%s"%traceback.format_exc())

    @staticmethod
    def send_dingding(text):
        d = {"msgtype": "text", "text": {"content": "besttest %s" %text }}
        r = HttpRequest(dingding_url,d,is_json=True)
        r.post() #发请求

