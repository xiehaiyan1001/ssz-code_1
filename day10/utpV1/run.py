import unittest
import nnreport
import time
import os
from config.setting import CASE_PATH,REPORT_PATH,dingding_template,mail_template
from utlis.send_message import Send

suite = unittest.defaultTestLoader.discover(CASE_PATH,'test*.py')

title = "接口测试报告-%s"%time.strftime("%Y-%m-%d %H-%M-%S")
run = nnreport.BeautifulReport(suite)

run.report(
    description=title,
    filename=title,
    log_path=REPORT_PATH
)

report_file_path = os.path.join(REPORT_PATH,title+'.html')

all_count = run.success_count + run.failure_count

mail_content = mail_template % (all_count,run.success_count,run.failure_count)
dingding_content = dingding_template % (all_count,run.success_count,run.failure_count)
Send.send_mail(title,mail_content,report_file_path)
Send.send_dingding(dingding_content)




#邮件内容模板
#各位好：
    #本次测试结果如下：
        #共运行%s条用例，通过%s条，失败%s条。具体报告看附件。
#dingding
# 本次测试结果如下:共运行%s条用例，通过%s条，失败%s条。