import requests
import traceback
from config.setting import log
from .sign import sign

class HttpRequest:
    def __init__(self, url,
                 data=None,
                 is_json=False,
                 params=None,
                 files=None,
                 headers=None,
                 timeout=3
                 ):
        self.url = url
        self.params = params  # 数据放在url里面的时候传它
        self.data = data
        self.is_json = is_json
        self.files = files
        self.headers = headers
        self.timeout = timeout  # 默认超时时间
        log.debug("开始准备调用接口，请求相关参数 %s"% locals())
        # self.add_sign()
        #self.param_jiami()

    # def add_sign(self):
    #     self.headers['sign'] = sign(self.data)

    # def param_jiami(self):
    #     return  'xx'



    def post(self):
        log.debug("post请求")
        try:
            if self.is_json:
                response = requests.post(self.url, json=self.data,
                                         params=self.params,
                                         headers=self.headers, timeout=self.timeout
                                         )
            else:
                response = requests.post(self.url, data=self.data,
                                         headers=self.headers,
                                         params=self.params,
                                         files=self.files, timeout=self.timeout
                                         )
        except Exception:
            log.error("请求出错\n%s"%traceback.format_exc())
            result = {"error_code": -1, "error_msg": traceback.format_exc()}
        else:
            result = self.__response_to_dict(response)
        return result

    def get(self):
        try:
            response = requests.get(self.url, params=self.params,
                                    headers=self.headers,
                                    timeout=self.timeout
                                    )
        except Exception:
            result = {"error_code": -1, "error_msg": traceback.format_exc()}
        else:
            result = self.__response_to_dict(response)
        return result

    def put(self):
        try:
            response = requests.put(self.url, data=self.data,
                                    params=self.params,
                                    headers=self.headers,
                                    timeout=self.timeout
                                    )
        except Exception:
            result = {"error_code": -1, "error_msg": traceback.format_exc()}
        else:
            result = self.__response_to_dict(response)
        return result

    def delete(self):
        try:
            response = requests.delete(self.url, data=self.data,
                                       params=self.params,
                                       headers=self.headers,
                                       timeout=self.timeout
                                       )
        except Exception:
            result = {"error_code": -1, "error_msg": traceback.format_exc()}
        else:
            result = self.__response_to_dict(response)
        return result

    def __response_to_dict(self, response):
        try:
            result = response.json()
        except Exception:
            result = response.text

        return result


if __name__ == '__main__':
    url = "http://api.nnzhp.cn/api/user/login"
    data = {'username': 'niuhanyang', 'passwd': 'aA123456'}
    params = {'a':1}
    r = HttpRequest(url, data,params=params)
    result = r.post()
    print(result)
