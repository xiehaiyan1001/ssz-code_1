import os
import xlrd
from config.setting import DATA_PATH

class GetData:
    '''获取参数化的类'''
    @staticmethod
    def to_txt(filename,sep=','):
        '''
        :param filename: 文件名
        :param sep: 分隔符，默认使用逗号分隔
        :return: 返回的是一个二维数组，每个小数组是文件里面的每一行数据
        '''
        data = []
        filename = os.path.join(DATA_PATH,filename)#和datapath把文件的路径拼成绝对路径
        if os.path.exists(filename):
            with open(filename, 'r', encoding='utf-8') as fr:
                for line in fr:
                    d1 = line.strip().split(sep)  # 每行分隔完之后变成一个数组
                    data.append(d1)
        else:
            raise FileNotFoundError('参数化文件找不到')
        return data

    @staticmethod
    def to_excel(filename,row_number=0):
        '''
        :param filename: excel文件名
        :param row_number: 从第几行开始读，默认从第0行
        :return: 返回的是一个二维数组，每个小数组是文件里面的每一行数据
        '''
        filename = os.path.join(DATA_PATH,filename)

        data = []
        if os.path.exists(filename):
            book = xlrd.open_workbook(filename)
            sheet = book.sheet_by_index(0)
            for i in range(row_number,sheet.nrows):
                data.append(sheet.row_values(i)) #取一整行的数据
        else:
            raise FileNotFoundError('参数化文件找不到')
        return data

    def to_mysql(self):
        pass
    def to_redis(self):
        pass
    def to_other(self):
        pass



