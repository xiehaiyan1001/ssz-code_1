import pymysql
import traceback
import redis

from config.setting import MYSQL_INFO,log,REDIS_INFO

class MySQL:
    def __init__(self):
        self.__connect()

    def __connect(self):
        log.debug("开始连接mysql，myql的连接信息是%s"%MYSQL_INFO)
        try:
            self.conn = pymysql.connect(**MYSQL_INFO)
            self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        except Exception as e:
            log.error("mysql无法连接")
            log.error(traceback.format_exc())
            raise Exception('mysql无法连接')

    def execute(self,sql):
        log.info("执行的sql语句是 [%s]"%sql)
        try:
            self.cursor.execute(sql)
        except Exception:
            log.error("sql执行出错，sql是【%s】"%sql)

    def fetchone(self): #select * from user where username = 'xx';
        return self.cursor.fetchone()

    def fetchall(self):#select * from user;
        return self.cursor.fetchall()

    def fetchmany(self,limit):
        return self.cursor.fetchmany(limit)

    def __close(self):
        self.cursor.close()
        self.conn.close()

    def __del__(self):
        self.__close()


def get_redis():
    return redis.Redis(**REDIS_INFO)

