import os
import nnlog

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath( __file__ )))

CASE_PATH = os.path.join(BASE_PATH,'case') #存放用例的目录
REPORT_PATH = os.path.join(BASE_PATH,'report') #存放报告的目录
LOG_PATH = os.path.join(BASE_PATH,'logs','utp.log') #存放日志的目录
DATA_PATH = os.path.join(BASE_PATH,'data') #存放数据的目录
salt = "szz-abc"


log = nnlog.Logger(LOG_PATH) #日志对象

MYSQL_INFO  = {
    'host':'118.24.3.40',
    'user':'besttest',
    'password':'123456',
    'port':3306,
    'db':'main',
    'charset':'utf8',
    'autocommit':True,
} #mysql连接信息

REDIS_INFO = {
    'host':'',
    'password':'',
    'port':6379,
    'db':0
}

USER_MAIL_LIST = ['602647962@qq.com','1345741814@qq.com','173547362@qq.com']
USER_MAIL_CC = ['769855584@qq.com','1113092340@qq.com','511402865@qq.com']

#host=host,user=user,password=password
MAIL_INFO = {
    'host':'smtp.163.com',
    'user':'uitestp4p@163.com',
    'password':'houyafan123',
    #'port':465
} #邮箱信息

dingding_url = 'https://oapi.dingtalk.com/robot/send?access_token=44402c9408df8cf3f429c02a20399fc34604f98cf572fcaeaa3f9592426176a7'

test_env = 'test2' #默认使用哪个环境

server_host_info = {
    'test':'http://api.nnzhp.cn/',
    'test2':'http://api.nnzhp.cn/',
}

server_host = server_host_info.get(test_env)


#邮件内容模板
mail_template='''
各位好：
    本次测试结果如下：
        共运行%s条用例，通过%s条，失败%s条。具体报告看附件。
'''
#dingding
dingding_template="本次测试结果如下:共运行%s条用例，通过%s条，失败%s条。"