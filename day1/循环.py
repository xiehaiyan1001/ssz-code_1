#循环、迭代、遍历
#for while

#while ,需要一个计数器

# count = 0
# while count<10:
#     count = count+1
#     if count == 4:
#         continue
#     print('创建了文件夹',count)
    # break
# else:
#     print('while对应的else')


# for i in range(10):
#     if i==4:
#         continue
#     print('创建了文件夹', i)
# else:#只要循环不是被break提前结束的，就会执行else里面的代码
#     print('for对应的else')
usernames = ['abc','1234']
pas = '123456'
login_tag = 'fail'
for i in range(3):
    name = input('username:')
    if name == '' or name not in usernames:
        continue
    for j in range(3):
        password = input('password:')
        if password == pas:
            print('登录成功')
            login_tag = 'success'
            break
        else:
            print('密码错误')
    if login_tag == 'success':
        break