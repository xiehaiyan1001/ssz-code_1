# if else
#if xxx: #
# else:
#xxx

age = 35

# if age<=6:
#     print('儿童')
# elif age<18 and age>6:
#     print('未成年人')
# elif age>=18 and age<=35:
#     print('年轻人')
# elif age>35 and age<=55:
#     print('中年人')
# else:
#     print('老年人')

if age>35:
    print('xxx')

sex = '男'

if sex=='男' or sex=='女':
    print('性别正常')
else:
    print('无法识别性别')


#  > < >= <=  !=
#<18 未成年人 >6 ,小于等于6岁 儿童，>18<35，年轻人 >35<50,中年人
