# name
# phone
# addr
names = ['name1','name2']
phone = ['1111','1111']
addr = ['xxxx','xxxxx']

infos = [ ['name1','1111','beijing'],['name2','2222','shanghai'],['liangcailian','33333','guangzhou'] ]

#字典
stu_info = {
    'name':'lcl',
    'phone':'11111111',
    'addr':'guangzhou'
}

#增加
stu_info['age'] = 18
stu_info.setdefault('qq','602647962')
# stu_info['name'] = 'zyb'
# stu_info.setdefault('name','zyb')
#修改
stu_info['name'] = 'zyb' #修改
#删除
# stu_info.pop('age')
# del stu_info['name']

#取值
# print(stu_info.get('phone'))
# print(stu_info['qq'])
# print(stu_info.get('sex','女'))
# print(stu_info)

# new_stu_info = stu_info.copy()
# stu_info.clear()
# print(stu_info.keys())#所有的key
# print(stu_info.values()) # {s1:100,s2:200,s3:300} 所有value
# print(stu_info.items())

stu_info.update(name='xh',money=5000)

print(stu_info)
