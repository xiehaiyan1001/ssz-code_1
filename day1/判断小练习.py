# score = input('请输入你的分数:') #input接收到的，全部都是str类型
# print('score的类型',type(score) )
# # score = int(score)#转成int类型
# score = float(score)#转成浮点型
# if score>=90 and score<=100:
#     print('优秀')
# elif score<90 and score>=80:
#     print('良好')
# elif score<80 and score>=60:
#     print('及格')
# elif score<60 and score>=0:
#     print('不及格')
# else:
#     print('分数不合法')




#分数，0-100
#>= 90 优秀
#<=90 >= 80 #良好
#>=60 <= 80 #及格
#小于60 不及格
#分数如果不在1-100之间，提示分数输入不合法


title = input('请输入标题:')

# sb,SB,傻逼
# if 'sb' in title or 'SB' in title or '傻逼' in title:
#     print('标题不合法')
# else:
#     print('标题合法')
if 'sb' not in title and 'SB' not in title and '傻逼' not in title:
    print('合法')
else:
    print('标题不合法')
