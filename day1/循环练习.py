#猜数字的游戏，最多7次机会，1-100之间，如果猜对了提示，恭喜你猜对了，游戏结束
#如果猜错了，告诉玩家猜大了 还是猜小了

#需求分析：
#1、随机产生一个数字
#2、循环7次
#3、input输入猜的数字
#4、判断猜测结果，大于、等于、还是小于
#5、提示，游戏次数超限，游戏结束
import random
number = random.randint(1,100)
count = 0
while count < 7:
    count = count+1
    # count+=1
    # count*=1 #count = count*1
    # count/=1 # count = count/1
    # count-=1 #count = count-1

    guess = input('请输入数字：')
    guess = int(guess)
    if guess == number:
        print('恭喜你，猜对了，结果是',guess)
        break
    elif guess < number:
        print('猜小了')
    else:
        print('猜大了')
else:
    print('次数超限，游戏结束')


for i in range(7):
    guess = input('请输入数字：')
    guess = int(guess)
    if guess == number:
        print('恭喜你，猜对了，结果是',guess)
        break
    elif guess < number:
        print('猜小了')
    else:
        print('猜大了')
else:
    print('次数超限，游戏结束')

