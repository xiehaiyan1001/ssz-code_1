# 欢迎xx登录，今天的时间是 xxxxx
name = '王月'
time = '2020/04/18'
s = '欢迎'+name+'登录，今天的时间是'+time
s2 = '欢迎 %s 登录，今天的时间是 %s'%(name,time) #占位符
s3 = '欢迎 %s 登录'%name
#%d %s %f
age = 18
score = 98.5555
s4 = '年龄是 %d,分数是 %.2f' %(age,score)
s5 = '年龄是 %s,分数是 %s' %(age,score)


# s6 = '年龄是{age},分数是{score}'
# s6.format(age=18,score=36)
# print(s6)
# #insert into user (id,name,addr,phone,qq,email,gold,class) values (%s,%s,%s,%s,%s,%s,%s,%s)
# sql = 'insert into user (id,name,addr,phone,qq,email,gold,class) values ({id},{name},{addr},{phone},{qq})'
# sql.format(id=1,addr='beijing',name='小暗黑',phone='1111',qq='xxxx')
s6 =  '年龄是{age},分数是{score}'.format(age=age,score=score)

s7 =  '年龄是{age},分数是{score}'
s7 = s7.format(age=age,score=score)
print(s7)