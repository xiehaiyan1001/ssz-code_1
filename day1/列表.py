#int float str list list

s = '王月、张娜、宋雪艳、刘姐、王何、程月香'
     #1   2     3     4    5   6
#王小月
#王大河
stus = ['王月','张娜','宋雪艳','刘姐','王何','程月香'] #下标、角标、索引
#        0      1     2       3     4     5
stu2 = []

#新增
stus.append('唐玉亮')
stus.insert(1,'徐静')
#改
stus[0] = '王小月'
#删除
# stus.pop(0)
# del stus[3]
# stus.clear()#清空列表
print(stus)
stus.remove('王何')
#取值
# print(stus)
# print(stus[0])
# print(stus[3])
# print(stus)

#其他的方法

l2 = [1,54,77,2,3,15,1]

l2.reverse()
print(l2)
# print(l2.count(1))
# print(l2.index(77))#找下标
# l2.sort(reverse=True)
# # print(l2)
# l3 = l2.copy()#复制一个list
# l2.extend(stus)
# print(l2)
# stus.sort()
# stus.copy()
# stus.extend() #合并两个list
# stus.reverse()