import add

#1、正常的用例
result = add.add(1,2) #int float
if result == 3:
    print('测试通过')
else:
    print('测试不通过')

#2、边界值测试
result = add.add(2,3)
assert result==5 #断言 True

#3、test
result = add.add(-1,5)
assert result==-3 #断言就不通过
