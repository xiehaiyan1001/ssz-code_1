import unittest

def add(a,b):
    return a+b

class AddTest(unittest.TestCase):

    def setUp(self):
        #每条用例运行之前都会执行它，
        print("我是set up")
    def tearDown(self):
        #每条用例运行完之后都会执行它
        print('我是tear down')

    @classmethod
    def tearDownClass(cls):
        print("tearDownClass")
        #最后执行tearDownClass里面的
        #db()

    @classmethod
    def setUpClass(cls):
        #最先开始执行的,大多数用例需要统一执行的操作
        #bak_db()
        print("setUpClass")

    def testb(self):
        print("testb")

    def get_token(self):
        return "token"

    def test_add_normal(self):
        print('add_normal')
        result = add(1,1)
        # self.assertEqual(3,result)

    def test_add_error(self):
        print("add_error")
        yq = 1
        result = add(1,1)
        # self.assertEqual(yq,result,"add测试结果不通过，要预期结果%s，实际结果%s"
        #                  %(yq,result))
    def test_a(self):
        print("testa")


#unittest.main() #运行当前python文件里面的所有用例


