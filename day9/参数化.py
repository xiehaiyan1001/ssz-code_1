import unittest
import requests
import parameterized
import os

class GetData:

    @staticmethod
    def read_data_to_file(filename):
        data = []
        if os.path.exists(filename):
            with open(filename,'r',encoding='utf-8') as fr:
                for line in fr:
                    d1 = line.strip().split(',')  #每行分隔完之后变成一个数组
                    data.append(d1)
        else:
            raise FileNotFoundError('参数化文件找不到')
        return data

    @staticmethod
    def read_data_to_excel(file_name):
        pass

    @staticmethod
    def read_data_to_redis(key):
        pass

    @staticmethod
    def read_data_to_mysql(sql):
        pass



class StuInfoTest(unittest.TestCase):
    url = 'http://api.nnzhp.cn/api/user/stu_info'

    @parameterized.parameterized.expand(GetData.read_data_to_file('stu_info.txt'))
    def test_single(self,name,age):
        print(name,age)
        data = {'stu_name':name,'age':age}
        r = requests.get(self.url,data)
        print(r.json())

    @parameterized.parameterized.expand(
        [
            ['xiaohei','sdsdf'],
            ['xiaohei','sdsdf'],
            ['xiaohei','sdsdf'],
            ['xiaohei','sdsdf'],
            ['xiaohei','sdsdf'],
        ]
    )
    def test_more(self,name,age):
        data = {'stu_name':name,'age':age}
        r = requests.get(self.url,data)
        print(r.json())




unittest.main()

