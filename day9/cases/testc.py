import unittest

class TestLogin(unittest.TestCase):
    def test_login_normal(self):
        '''正常登录'''
        self.assertEqual(1,1)

    def test_login_black_list(self):
        '''黑名单登录'''
        self.assertTrue(False,'黑名单登录运行失败')

    def test_login_exit(self):
        '''注销用户登录'''
        self.assertNotIn(1,[1,2,3],'这个是notin的')

    def test_login_max_count(self):
        '''达到最大错误次数登录'''
        self.assertNotEqual(1,2,'这个是不相等的')

