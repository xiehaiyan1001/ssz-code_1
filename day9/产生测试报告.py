import unittest
import HTMLTestRunner
import nnreport

class TestLogin(unittest.TestCase):
    def test_login_normal(self):
        '''正常登录'''
        self.assertEqual(1,1)

    def test_login_black_list(self):
        '''黑名单登录'''
        self.assertTrue(False,'黑名单登录运行失败')

    def test_login_exit(self):
        '''注销用户登录'''
        self.assertNotIn(1,[1,2,3],'这个是notin的')

    def test_login_max_count(self):
        '''达到最大错误次数登录'''
        self.assertNotEqual(1,2,'这个是不相等的')


#unittest.main()

# suite = unittest.TestSuite() #测试集合,手动一个个的把用例加入到测试集合中
# suite.addTest(TestLogin('test_login_normal'))
# suite.addTest(TestLogin('test_login_black_list'))

suite = unittest.makeSuite(TestLogin)
#把一个类里面的所有测试用例，放到一个测试集合里面

# f = open('测试报告2.html','wb')
# runner = HTMLTestRunner.HTMLTestRunner(f,
#                                        title='登录接口测试报告',
#                                        description='这个是登录接口测试报告')
# runner.run(suite)
# f.close()

runner = nnreport.BeautifulReport(suite)
runner.report(description='登录接口测试报告',
              filename='login_report2',log_path='/Users/nhy/Desktop')