import time
import unittest
import requests
import jsonpath
import nnreport

def get_value(d, key,expr=False,more=False):
    if expr:
        jp = jsonpath.jsonpath(d, key)  # [1,2]
    else:
        jp = jsonpath.jsonpath(d, '$..%s' % key)  # [1,2]
    if jp:
        if more:
            return jp
        else:
            return jp[0]

class LiteMallBase(unittest.TestCase):
    username = "admin123"
    password = username
    host = "http://proxy.nnzhp.cn"

    def login(self,username,password):
        '''登录'''
        url = self.host+'/admin/auth/login'
        data = {"username":self.username,"password":self.password}
        r = requests.post(url,json=data)
        token = get_value(r.json(),'token')
        self.assertIsNotNone(token,msg="获取不到token")
        return token

    def create_coupon(self,name,token,money):
        '''建券'''
        url = self.host+"/admin/coupon/create"
        data = {"name":name,"desc":"王月优惠券",
                "total":"999","discount":money,"min":999,
                "limit":1,"type":0,"status":0,"goodsType":0,
                "goodsValue":[],"timeType":0,"days":"10",
                "startTime":None,"endTime":None}

        headers = {'X-Litemall-Admin-Token':token}
        r = requests.post(url,json=data,headers=headers)
        cid = get_value(r.json(),'id')
        cname = get_value(r.json(),'name')
        errno = get_value(r.json(),'errno')
        self.assertEqual(errno,0,msg="创建券失败，%s"% r.json() )
        self.assertEqual(name,cname,msg="创建的券名称和返回的不一致")

        #少写了从数据库里面查询的！！

        return cid

    def index_data(self):
        '''查询首页数据'''
        url = self.host + '/wx/home/index'
        r = requests.get(url)
        errno = get_value(r.json(),'errno')
        self.assertEqual(errno,0,msg="首页接口调用失败")
        return r.json()

    def create_brand(self):
        pass
    def create_good(self):
        pass

    

class CouponCreate(LiteMallBase):

    def test_create_coupon(self):
        '''建券测试'''
        name = "自动化测试%s-优惠券"%int(time.time())
        token = self.login()
        cid = self.create_coupon(name,token,99999)
        index_data = self.index_data()
        coupon_ids = get_value(index_data,'$.data.couponList..id',True,True)
        self.assertIn(cid,coupon_ids,msg="创建的券id不存在，不存在的券id是%s"%cid)



class TestLogin(LiteMallBase):
    def test_login(self):
        self.login()

    #1、登录 拿到token
    #2、token传给 创建券的接口，接口返回券id
    #3、调用首页接口，传券id给他，判断券是否存在

suite = unittest.makeSuite(CouponCreate)

report = nnreport.BeautifulReport(suite)
report.report(description="优惠券测试",filename="coupon_report")



