import datetime
from itertools import chain

from django.db.models import Q, Model
from django.forms import BaseForm
from django.views import View
from django.http import HttpResponse, JsonResponse
from example import models
from django.core.paginator import Paginator
from django.http import QueryDict
from .tools import model_to_dict


class BaseView(View):
    model_class = None
    form_class = None
    fields = []
    exclude_fields = []
    search_field = ''
    filter_field = []

    def get_filter_dict(self):
        filter_dict = {}
        for field in self.filter_field:
            value = self.request.GET.get(field)
            if value:
                filter_dict[field] = value
        return filter_dict

    def get_search_obj(self):
        search = self.request.GET.get('search')
        if search:
            search_dict = {'%s__contains' % self.search_field[0]: search}
            return search_dict
        else:
            return {}

    @property
    def form(self):
        if self.form_class and issubclass(self.form_class, BaseForm):
            return self.form_class
        raise Exception('请指定form类')

    @property
    def model(self):
        if self.model_class and issubclass(self.model_class, Model):
            return self.model_class
        raise Exception('请指定model类')


class GetView(BaseView):
    def get(self, requests):
        page = requests.GET.get('page')
        filter_dict = self.get_filter_dict()
        search_dict = self.get_search_obj()
        qs = self.model.objects.filter(is_delete=0).filter(**filter_dict).filter(**search_dict)
        page_obj = Paginator(qs, 5)
        page_data = page_obj.get_page(page)
        data_list = []
        for instance in page_data:
            # 将page_data 转成字典
            # 可以传递fields 指定返回那些字段
            # 可以传递exclude 指定排除那些字段返回
            data = model_to_dict(instance, fields=self.fields, exclude=self.exclude_fields)
            data_list.append(data)
        return JsonResponse({"msg": data_list})


class PostView(BaseView):
    def post(self, requests):
        form_obj = self.form_class(requests.POST)
        f = form_obj.is_valid()
        if f:
            self.model_class.objects.create(**form_obj.cleaned_data)
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            return JsonResponse({"code": 500, "msg": form_obj.errors})


class PutView(BaseView):
    def put(self, requests):
        obj = self.model.objects.get(id=requests.PUT.get('id'))
        form_obj = self.form(requests.PUT, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return JsonResponse({"msg": "成功"})
        else:
            return JsonResponse({"msg": form_obj.errors})


class DeleteView(BaseView):
    def delete(self, requests):
        p_id = requests.GET.get('id')
        data = self.model.objects.filter(id=p_id).first()
        data.is_delete = 1
        data.save()
        return JsonResponse({"msg": "成功"})


class NbView(GetView, PostView, PutView, DeleteView):
    pass
