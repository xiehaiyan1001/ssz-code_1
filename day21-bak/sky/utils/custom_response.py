from django.http.response import JsonResponse
import json, datetime


class NbJSONEncoder(json.JSONEncoder):
    '''解析json里面日期格式'''

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, datetime.date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)


class NbResponse(JsonResponse):
    '''改为类，就支持
        response = NbResponse()
        response.data = {}
        return response
        这样的用法
    '''
    def __init__(self, code=0, msg="操作成功", **kwargs):
        data = {'code': code, 'msg': msg}
        data.update(kwargs)
        data.update(self.__dict__)
        super().__init__(data, json_dumps_params={"ensure_ascii": False}, encoder=NbJSONEncoder)