from django.db import models


# Create your models here.
class BaseModel(models.Model):
    '''公共字段'''
    is_delete_choice = (
        (1, '删除'),
        (0, '正常')
    )
    is_delete = models.SmallIntegerField(choices=is_delete_choice, default=0, verbose_name='是否被删除')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)  # auto_now_add的意思，插入数据的时候，自动取当前时间
    update_time = models.DateTimeField(verbose_name='修改时间', auto_now=True)  # 修改数据的时候，时间会自动变

    class Meta:
        abstract = True  # 只是用来继承的,不会创建这个表


class Parameter(BaseModel):
    '''全局参数'''
    name = models.CharField(verbose_name='参数名称', max_length=100, unique=True)
    desc = models.CharField(verbose_name='参数描述', max_length=200, null=True)
    value = models.CharField(verbose_name='参数值', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '全局参数表'
        verbose_name_plural = verbose_name
        db_table = 'parameter'
        ordering = ['-id']
