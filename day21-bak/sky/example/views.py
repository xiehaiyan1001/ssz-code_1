import datetime
from itertools import chain

from django.views import View
from django.http import HttpResponse, JsonResponse
from example import models
from example import forms
from django.core.paginator import Paginator
from django.http import QueryDict
from utils.custom_view import GetView, PostView, NbView


# 默认导入地址
# from django.forms.models import model_to_dict


# Create your views here.

def model_to_dict(instance, fields=None, exclude=None):
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if isinstance(value, datetime.datetime):
            value = value.strftime('%Y-%m-%d %H:%M:%S')
        if isinstance(value, datetime.date):
            value = value.strftime('%Y-%m-%d')
        data[f.name] = value
    return data


# cbv fbv
# 面向对象编程，提高代码复用性。
# 继承 多继承
class Parameter(NbView):
    model_class = models.Parameter
    form_class = forms.ParameterForm
    exclude_fields = ['is_delete']
    filter_field = ['desc', 'name']
    search_field = ['name']


# class Parameter(View):
#     def get(self, requests):
#         page = requests.GET.get('page')
#         qs = models.Parameter.objects.filter(is_delete=0)
#         page_obj = Paginator(qs, 5)
#         page_data = page_obj.get_page(page)
#         data_list = []
#         for instance in page_data:
#             # 将page_data 转成字典
#             # 可以传递fields 指定返回那些字段
#             # 可以传递exclude 指定排除那些字段返回
#             data = model_to_dict(instance, fields=None, exclude=None)
#             data_list.append(data)
#         return JsonResponse({"msg": data_list})
#
#     def post(self, requests):
#         form_obj = forms.ParameterForm(requests.POST)
#         f = form_obj.is_valid()
#         if f:
#             models.Parameter.objects.create(**form_obj.cleaned_data)
#             return JsonResponse({"code": 200, "msg": "成功"})
#         else:
#             return JsonResponse({"code": 500, "msg": form_obj.errors})
#
#     def put(self, requests):
#         putData = QueryDict(requests.body)
#         obj = models.Parameter.objects.get(id=putData.get('id'))
#         form_obj = forms.ParameterForm(putData, instance=obj)
#         if form_obj.is_valid():
#             form_obj.save()
#             return JsonResponse({"msg": "成功"})
#         else:
#             return JsonResponse({"msg": form_obj.errors})
#
#     def delete(self, requests):
#         p_id = requests.GET.get('id')
#         data = models.Parameter.objects.filter(id=p_id).first()
#         data.is_delete = 1
#         data.save()
#         return JsonResponse({"msg": "成功"})


def f_parameter(requests):
    if requests.method == 'GET':
        page = requests.GET.get('page')
        qs = models.Parameter.objects.filter(is_delete=1)
        page_obj = Paginator(qs, 5)
        page_data = page_obj.get_page(page)
        data_list = []
        for instance in page_data:
            # 将page_data 转成字典
            # 可以传递fields 指定返回那些字段
            # 可以传递exclude 指定排除那些字段返回
            data = model_to_dict(instance, fields=None, exclude=None)
            data_list.append(data)
        return JsonResponse({"msg": data_list})
    elif requests.method == 'POST':
        form_obj = forms.ParameterForm(requests.POST)
        f = form_obj.is_valid()
        if f:
            models.Parameter.objects.create(**form_obj.cleaned_data)
            return JsonResponse({"code": 200, "msg": "成功"})
        else:
            return JsonResponse({"code": 500, "msg": form_obj.errors})
    elif requests.method == 'PUT':
        putData = QueryDict(requests.body)
        obj = models.Parameter.objects.get(id=putData.get('id'))
        form_obj = forms.ParameterForm(putData, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return JsonResponse({"msg": "成功"})
        else:
            return JsonResponse({"msg": form_obj.errors})
    elif requests.method == 'DELETE':
        p_id = requests.GET.get('id')
        data = models.Parameter.objects.filter(id=p_id).first()
        data.is_delete = 1
        data.save()
        return JsonResponse({"msg": "成功"})
    else:
        return HttpResponse('error')
