# Generated by Django 2.1 on 2020-09-01 20:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('example', '0004_auto_20200901_2038'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='parameter',
            unique_together={('is_delete', 'name')},
        ),
    ]
