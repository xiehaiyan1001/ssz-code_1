from django import forms
from django.core.exceptions import ValidationError  # 验证有问题，抛异常用的
from example import models


class ParameterForm(forms.ModelForm):
    class Meta:
        model = models.Parameter
        exclude = ['id', 'is_delete', 'create_time', 'update_time']
