from django.utils.deprecation import MiddlewareMixin
from .settings import DEBUG
from utils.custom_response import NbResponse
from django.http import QueryDict


class ExceptionMiddleware(MiddlewareMixin):
    '''处理异常，不让接口接收到异常，有异常就返回系统异常'''

    def process_exception(self, request, exception):
        if not DEBUG:
            return NbResponse(500, '系统开小差了，请联系管理员 %s' % (exception))


class PutMethodMiddleware(MiddlewareMixin):
    '''处理put请求，给request对象里面添加request.PUT'''

    @staticmethod
    def process_request(requests):
        # 所有请求先走到这，然后再去请求视图
        if requests.method == 'PUT':
            if 'boundary' in requests.content_params.keys():
                put_data, files = requests.parse_file_upload(requests.META, requests)
                requests.PUT = put_data
                requests._files = files  # 是因为request.FILES 里面取值的时候，就是_files
            else:
                requests.PUT = QueryDict(requests.body)
                # request.PUT = dict(parse_qsl(unquote_plus(request.body.decode())))
