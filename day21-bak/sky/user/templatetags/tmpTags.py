from django import template

register = template.Library()


# 自定义 过滤器
@register.filter
def status_filter(x):
    if x == 1:
        return '成功'
    else:
        return '失败'


@register.filter
def test_filter(x, y):
    return x + y


@register.simple_tag
def test_simple_tag(x, y, z):
    return x+y+z
