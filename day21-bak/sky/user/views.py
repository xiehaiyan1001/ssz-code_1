from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from . import models
# Create your views here.
from django.core.paginator import Paginator
from user.forms import ArticleFrom,StudentForm


def user_info(request):
    s = '你好'
    return HttpResponse(s)  # 返回的字符串的话用它


def welcome(request):
    username = 'changcaixia'  # request
    username_zh = '常彩霞-使用的reaplace'  # sql查

    return render(request, 'welcome.html', {'username_zh': username_zh})


def index(request):
    page = request.GET.get('page')
    print('页面传递过来的参数',page)
    articles = models.Article.objects.all()
    # 创建 分页实例 articles是文章所有数据，第二个参数代表每页展示多少条数据 5
    page_obj = Paginator(articles, 5)  # page_obj Paginator实例对象
    print(" 文章一共有多少数据", page_obj.count)  # 文章一共有多少数据
    # 一共分了6页，每页5条数数据，有30条数据  我们只有27条，最后一页会有2条数据
    print("返回总页数 一共分了多少页", page_obj.num_pages)  # 返回总页数 一共分了多少页
    # range 可迭代对象。 顾头不顾尾
    print(" 页码列表 range对象", page_obj.page_range)  # 页码列表 range对象
    # for i in page_obj.page_range:
    #     print(i)

    # page_data数据是某一页的数据集合对象
    page_data = page_obj.get_page(page)  # number 代表获取第几页的数据
    # print("是否有上一页或下一页",page_data.has_other_pages())  # 是否有上一页或下一页
    # print("是否有下一页",page_data.has_next())  # 是否有下一页
    # print("是否有上一页",page_data.has_previous())  # 是否有上一页
    # print("下一页页码值",page_data.next_page_number())  # 下一页页码值
    # print("上一页页码值",page_data.previous_page_number())  # 上一页页码值
    # print("当前页",page_data.number)  # 当前页

    return render(request, 'index.html', {'articles': page_data,"page_obj":page_obj})


def category(request, id):
    category_obj = models.Category.objects.get(id=id)
    articles = models.Article.objects.filter(category=category_obj)
    return render(request, 'category.html', {'articles': articles, 'category_obj': category_obj})


def article(request):
    if request.method == 'GET':
        return render(request, 'post.html')
    else:
        # 实例化form验证器，接收post传过来的数据
        articleObj = ArticleFrom(request.POST)
        # 判断当前 写好的验证器是否验证通过  返回 True 或 false
        f = articleObj.is_valid()
        if f:
            # 验证通过的数据 存在了一个叫做cleaned_data
            # 验证通过的字段会都在cleaned_data中，是个dict
            print("articleObj.cleaned_data",articleObj.cleaned_data)
            msg = '成功'
            # 存数据
        else:
            print("is_valid",f)
            # 拿到错误信息
            print("articleObj.errors",articleObj.errors)
            # 通过 dict方式取 信息
            print("articleObj.errors",articleObj.errors.get_json_data())
            # msg = articleObj.errors.get_json_data().get('title')[0].get('message')
        return HttpResponse(articleObj.errors)


def login(request):
    username = request.POST.get('userbane')

    return HttpResponse('{"code":0,"msg":"ok"}')


def archive(request):
    categories = models.Category.objects.all()


def student(requests):
    if requests.method == 'GET':
        return render(requests, 'student.html')
    else:
        studentObj = StudentForm(requests.POST)
        f = studentObj.is_valid()
        if f:
            print('cleaned Data',studentObj.cleaned_data)
        else:
            print('error',studentObj.errors.get_json_data())
        return HttpResponse('ok')