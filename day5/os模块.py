import os
#   ../day4/a.py


# print(os.path.exists('/Users/nhy/PycharmProjects/ssz/day51')) #判断目录/文件是否存在
# print(os.path.dirname('/Users/nhy/PycharmProjects/ssz/day5/goods.json') )##获取父目录
# print(os.path.abspath(r'../nay4/a.py'))#根据相对路径获取绝对路径
# print(os.path.split('/usr/local/a.py'))
# print(os.path.getsize(r'/Users/nhy/PycharmProjects/ssz/day5/os模块.py'))
# print(os.path.isfile('/Users/nhy/PycharmProjects/ssz/day5/'))
# print(os.path.isdir('/Users/nhy/PycharmProjects/ssz/day5/'))
# print(os.path.getmtime(r'/Users/nhy/PycharmProjects/ssz/day5/os模块.py')) #修改时间
# print(os.path.getatime(r'/Users/nhy/PycharmProjects/ssz/day5/os模块.py')) #最后一次访问的时间
print(os.path.getctime(r'/Users/nhy/PycharmProjects/ssz/day5/os模块.py')) #创建时间
#

# os.mkdir('test')
# os.makedirs('test1')
# os.mkdir('python2/python')
# os.mkdir('python2/python3')
# os.makedirs('python/python2') #父目录不存在的时候，会帮你创建一个父目录
# os.rmdir('python/python3')#删除空文件夹的
# os.remove('goods.json') #只能删除文件，不能删文件夹
# os.rename('python','python3')
# print(os.listdir('..'))
#os.chdir('/Users/nhy/Desktop') #进入到某个目录下
#print(os.getcwd())#获取当前目录

#os.system('rm -rf  python3') #执行操作系统命令，获取不到结果


# result = os.popen('ifconfig').read()    #执行操作系统命令，可以获取到命令执行的结果
#
# print(result)
# os.walk() #获取某个目录下的内容 # 当前目录，当前目录下的文件夹，当前目录下的文件
#它帮你循环这个目录下面所有的子目录
# for cur_dir,dirs,files in os.walk('/Users/nhy/PycharmProjects/ssz'):
#     print(cur_dir)
#     print(dirs)
#     print(files)
#     print('='*10)


