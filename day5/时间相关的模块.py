#logs
import time,datetime
#格式化好的时间 2020-05-16 18：32：53
#时间戳 1589616752.423 从unix元年（计算机发明的时间）到现在过了多少秒

#时间元组
print(time.time())#当前时间戳
print(time.strftime('%Y-%m-%d %H:%M:%S'))
print(time.strftime('%Y-%m-%d'))
print(time.strftime('%H:%M:%S'))
print(time.strftime('%Y%m'))

# result =time.localtime(1589617130)#把时间戳转成时间元素
# print(time.strftime('%Y-%m-%d %H:%M:%S',result))

result = time.strptime('2020-05-16 16:18:50','%Y-%m-%d %H:%M:%S')
print(time.mktime(result))


def str_to_timezone(str=None,format="%Y-%m-%d %H:%M:%S"):
    #这个函数是格式化好的时间转时间戳的，如果不传参数默认返回当前时间戳
    if str:
        time_tuple = time.strptime(str,format)
        result = time.mktime(time_tuple)
    else:
        result = time.time()
    return int(result)

def timezone_to_str(timezone=None,format="%Y-%m-%d %H:%M:%S"):
    '''这个函数是时间戳转格式化好的时间，如果不传参数，默认返回当前时间'''
    if timezone:
        time_tuple = time.localtime(timezone)
        result = time.strftime(format,time_tuple)
    else:
        result = time.strftime(format)
    return result

time.sleep(10)

print('end')