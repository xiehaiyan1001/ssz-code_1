#规则：
    #前区 01-35 5
    #后区：01 -12 2
    #1. 1 - 35 5  1-12 2
import random
def lotte():
    front_list = random.sample(range(1,36),5)
    front_list.sort()
    back_list = random.sample(range(1,13),2)
    back_list.sort()
    result = [str(i).zfill(2) for i in front_list+back_list]
    return ' '.join(result)

def run():
    number = input("请输入产生的条数:").strip() #1000
    password_set = set()
    if number.isdigit():
        while len(password_set)!=int(number):#0,1000
            # result = create_password()
            result = lotte()
            password_set.add(result+'\n')
    with open('大乐透号码.txt','w') as fw:
        fw.writelines(password_set)
run()