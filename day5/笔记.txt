上周回顾：
    1、函数、内置函数、模块、json
    函数：
            with open(a) as fr:
                result = fr.read()
    addr = "shanghai"
    def test(name):
        print(name)
        global addr
        addr = "beijing"
        print(addr)
        return "hello %s"name
    test("1234")

    全局变量
    递归 最大递归999
    def test1():
        number = input("输入数字：")
        if number % 2 ==0:
            print('输入的是偶数')
            return
        test1()

    lambda a:a+1
    lambda: a:a[1]
    lambda: x,y:y+x
    内置函数：
        input、len、print、max、min、sorted、exit()、
        quit()
        d = {'a':1,'b':2}
        d = [(a,1),(b,2)]
        sorted(d.items(),key=lambda x:x[1])
    模块：
        导入模块的原理
            把导入的这个模块代码运行一遍
        导入模块的顺序：
            1、当前目录找
            2、从python的环境变量里面找 （sys.path）
    第三方模块：
        pip install xxx
            redis.tar.gz
            cd redis
            python setup.py install
        pip install redis.whl

常用模块讲完：
    os
    sys
    mysql
    redis
    shangzhou作业：
        1、生成密码、包含大写字母、小写字母、数字
        2、生成大乐透号码
        3、商品管理的程序

    本周：
        1、写一个删除日志的程序，删除5天前或为空的日志，不包括当天的
        2、改造商品管理的程序，把商品信息存到数据库里面，自己建一个表


