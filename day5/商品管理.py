import json
FILE_NAME = 'goods.json'
def read_product():
    with open(FILE_NAME,'a+',encoding='utf-8') as  fr:
        fr.seek(0)
        result = fr.read()
        if result:
            return json.loads(result)
        return {}

def write_product(product):
    with open(FILE_NAME,'w',encoding='utf-8') as  fr:
        json.dump(product,fr,ensure_ascii=False,indent=4)

def get_product_name():
    for i in range(3):
        name = input("请输入商品名称：").strip()
        if name:
            return name
        else:
            print('商品名称不能为空')
    else:
        quit("错误次数过多")

def show():
    name = get_product_name()
    product = read_product()
    if name == 'all':
        print(product)
    elif product.get(name):
        print("商品信息是%s"%product.get(name))
    else:
        print('商品不存在！')

def delete():
    name = get_product_name()
    product = read_product()
    if product.get(name):
        product.pop(name)
        print("商品已经被删除")
        write_product(product)
    else:
        print('商品不存在！')

def check_count(count:str):
    if count.isdigit():
        if int(count)>0:
            return int(count) #1
    #None

def check_price(price:str):
    count = check_count(price)
    if count:
        return count
    else:
        if price.count('.')==1 and price.replace('.','').isdigit():
            return float(price) if float(price)>0 else None

def add():
    name = get_product_name()
    price = input("price:").strip()
    count = input("count:").strip()
    color = input("color:").strip()
    price = check_price(price)
    count = check_count(count)
    if  price and count and color :
        product = read_product()
        if product.get(name):
            print('无法添加')
        else:
            d = {"price":price,'count':count,'color':color}
            product[name] = d
            print("添加成功！")
            write_product(product)
    else:
        print("价格/数量/颜色不合法")

def modify():
    name = get_product_name()
    price = input("price:").strip()
    count = input("count:").strip()
    color = input("color:").strip()
    price = check_price(price)
    count = check_count(count)
    if  price and count and color :
        product = read_product()
        if product.get(name):#商品存在可以修改
            d = {"price": price, 'count': count, 'color': color}
            product[name] = d
            print("修改成功！")
            write_product(product)
        else:
            print('商品不存在！')
    else:
        print("价格/数量/颜色不合法")

# choice = input("请输入：1、添加2、修改、3、查看4、删除、other、退出").strip()
# if choice == "1":
#     add()
# elif choice == "2":
#     modify()
# elif choice=="3":
#     show()
# elif show()=="4":
#     delete()
# else:
#     quit()

choice = input("请输入：1、添加2、修改、3、查看4、删除、other、退出：").strip()
func_map = {'1':add,'2':modify,'3':show,'4':delete}
if choice in func_map:
    func_map.get(choice)()
else:
    quit("退出程序！")
