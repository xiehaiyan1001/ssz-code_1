import pymysql
connect = pymysql.connect(host='118.24.3.40',
                          user='jxz',
                          password='123456',
                          db='jxz',
                          port=3306,
                          charset="utf8",
                          autocommit=True
                          )
cur = connect.cursor(pymysql.cursors.DictCursor)#建立游标
# sql = 'select * from students;'
# sql="insert into students values (99,'蔡明昌','男',38,'射手座','北京');"
sql='select * from students limit 3;'
cur.execute(sql)
# connect.commit() #提交
# connect.rollback() #回滚,sql执行失败的时候，用在事务里
# print(cur.fetchall()) #获取sql执行的结果
# print(cur.fetchone()) #获取sql执行的结果,只获取一条结果
# result2 = cur.fetchmany(10) #获取sql执行的结果#获取指定的条数
print(cur.description)

# print(result)
# print(result1)
# print(result2)

cur.close()
connect.close()

#commit操作，修改数据的情况下，必须得commit
#事务：多条sql一起执行，如果一条失败，两条sql都算失败

#