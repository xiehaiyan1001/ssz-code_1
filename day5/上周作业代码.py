#1、包含数字、大写字母、小写字母，长度6-12随机的
#不用集合的
    #1、随机取一位数字、一位大写字母、一位小写字母 #3个字符串
    #2、生成一个6-12之间的随机数，number
    #3、把随机数减去3，剩下的随机取
#用集合的
    #1、生成3个集合，大写字母、小写、数字3个集合
    #2、随机取6-12位的密码，从所有的大写+小写+数字里面取
    #3、取出来密码分别和大写字母、小写、数字3个集合取交集
import string
import random
def create_password():
    start = random.choice(string.digits) + random.choice(string.ascii_lowercase) \
    +random.choice(string.ascii_uppercase)
    end = random.sample(string.digits+string.ascii_letters,random.randint(6,12)-3)
    end.append(start)
    random.shuffle(end)
    password = ''.join(end)
    return password

def create_password2():
    number_set = set(string.digits)# 0-9
    upper_set = set(string.ascii_uppercase)
    lower_set = set(string.ascii_lowercase)

    while True:
        length = random.randint(6, 12)#密码长度
        before_password = random.sample(string.digits+string.ascii_uppercase+string.ascii_lowercase,length)
        p = set(before_password)
        if number_set & p and upper_set & p and lower_set & p:
            return ''.join(p)

def run():
    number = input("请输入产生的条数").strip() #1000
    password_set = set()
    if number.isdigit():
        while len(password_set)!=int(number):#0,1000
            # result = create_password()
            result = create_password2()
            password_set.add(result+'\n')

    with open('passwords.txt','w') as fw:
        fw.writelines(password_set)


