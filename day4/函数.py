# def func():
#     a = 1
#     b = 2
#     c = 3
#     return  a,b,c
#
# q,w,e = func()
# print(q,w,e)
#
# #解包
# info = ['xiaohei','beijing','haidianqu']
#
# name,addr,e = info
#
# print(name,addr,e)

#局部变量：在函数里面定义的变量都是局部变量（没有加global修饰），
# 作用域是在函数里面，出了函数就失效了

#全局变量：定义在函数外面，一般都是写在代码的最上面,作用域是全部的，大家都可以用

name = 'caimingchang'
def rename():
    global name
    name = 'cai'
    print('name1',name)
def test():
    addr = 'beijing'
    print('name',name)
    print('addr',addr)
print('name2',name)
rename()
print('name2',name)


# money = 500
# def test(consume):
#     return money - consume
#
# def test1(money):
#     return test(money) + money
#
# money = test1(money)
# print(money)

def test():
    global a
    a = 5

def test1():
    c = a + 5
    return c
# test()
res = test1()
print(res)
#常量 -> 变量
#常量：一个不变的值
IP  = '192.168.1.3' #常量
