#匿名函数也是一个函数，是一个简单的函数，没有名字，只能实现一些简单的功能

f1 = lambda x,y:x+y

def f2(x):
    return x+1

print(f1(1,2))

print(f2(1))