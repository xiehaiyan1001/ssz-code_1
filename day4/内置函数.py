# int
# str
# list
# set
# dict
# tuple
# bool
# type
# len
# input
# print
# print(all([1, 2, 3, 4]))  # 判断可迭代的对象里面的值是否都为真

# print(all(['1','2','3',0]))
# print(any([0, '', False]))  # 判断可迭代的对象里面的值是否有一个为真
# print(bin(10))  # 十进制转二进制
# print(chr(33))  # 打印数字对应的ascii
# print(ord('b'))  # 打印字符串对应的ascii码
#
# f = open('a.txt','w')
# s=''
# print(dir(s))  # 打印传入对象的可调用方法
# print(hex(111))  # 数字转成16进制
# print(oct(111))  # 把数字转换成8进制
# def func():
#     name ='abc'
#     age = 18
#     # {'name':'abc','age':18}
#     print(locals())  # 返回局部变量
# name = '123'
# print(globals())  # 返回程序内所有的变量，返回的是一个字典

# print(max([1,5,9,111,324]))  # 取最大值
# print(sum([1,5,9,111,324]))
# print(divmod(10,3))#取余 # (3,1)
# print(round(11.1111111111, 2))  # 取几位小数
# zip()#把两个list和成一个二维数组 [12,3,4,2]  [4,4,2,2]  [[12,4],[3,4],[4,2],[2,2]]

# print(sorted([2, 31, 34, 6, 1, 23, 4]))  # 排序
# print(eval('[]'))  # 执行python代码，只能执行简单的，定义数据类型和运算
# print(exec('def a():pass'))  # 执行python代码
# print(filter(lambda x: x > 5, [12, 12, 35]))  # 把后面的迭代对象根据前面的方法筛选
# print(map(lambda x: x > 5, [1, 2, 3, 4, 5, 6])) [False,False,False,False,False,True]

#result = map(lambda x:str(x).zfill(2),range(1,36))

# l = ['u1','u2','u3','u4']
# l2 = ['p1','p2','p3']
# print(list(zip(l,l2)))
# print(dict(zip(l,l2)))
# l = [
#     ('小名',5,['xx',11]),
#     ['xiaohei',19,['xx',13]],
#     ['xiaobai',20,['xx',18]],
#     ['xiaolan',10,['xx',20]]
# ]
# def use_key(x):
#     return x[1]
#
# l4 = sorted(l,reverse=True,key=lambda x:x[-1][-1])
# print(l4)
# d={'a':1,'b':2}
# print(d)
# l2 = sorted(d.items(),reverse=True,key=lambda x:x[0])
# l3 = sorted(d.items(),reverse=True,key=lambda x:x[1])
#key的作用是传一个函数名

# print(filter(lambda x: x > 5, [12, 3, 12, 2, 1, 2, 35]))  # 把后面的迭代对象根据前面的方法筛选
# print(map(lambda x: x > 5, [1, 2, 3, 4, 5, 6]))
# def filter_test(x):
#     return x>5

# result = filter(lambda x: x > 5, [12, 3, 12, 2, 1, 2, 35])
# result = filter(filter_test, [12, 3, 12, 2, 1, 2, 35])
# l = [12, 3, 12, 2, 1, 2, 35]
# result = map(lambda x: str(x).strip().zfill(3), l) #map方式实现
# print(list(result))
#
# #最传统的方式
# l2 = []
# for i in l:
#     new_i = str(i).strip().zfill(3)
#     l2.append(new_i)
# print(l2)
#
# l3 = [str(i).strip().zfill(3) for i in l]
#
# print(l3)
# l = ['购物','注册','充值','积分查询']
#
# # index = 0
# # for i in l:
# #     print('%s -> %s'%(index,i))
# #     index+=1
#
# for index,i in enumerate(l,1):
#     print('%s -> %s' % (index, i))
# print('请输入你的选择：')

# print(eval('[]'))  # 执行python代码，只能执行简单的，定义数据类型和运算
# print(exec('def a():pass'))  # 执行python代码

# str_code = '''
# import os
# l = [12, 3, 12, 2, 1, 2, 35]
# print([str(i).strip().zfill(3) for i in l])
# print('哈哈哈哈哈')
# '''
# if 'import os' in str_code:
#     print('os模块不能用')
# exec(str_code)

#result = eval('{"code":1,"name":"xiaohei"}') #执行python代码，只能执行简单的，定义数据类型和运算
#print(result)


for i in range(20):
    print("i")
    exit("拜拜")